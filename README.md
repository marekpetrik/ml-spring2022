# CS750 / CS850: Machine Learning #

### When and Where 

*Lectures*: MW 11:10 am - 12:30 pm

*Recitations*: F 1:10pm - 2:00pm

*Where*: Kingsbury N113 

See [class overview](overview/overview.pdf) for the information on grading, rules, and office hours.

## Syllabus: Lectures

The slides will be updated before each lecture. The topics are preliminary and may change due to snow days.

Regarding *reading materials* see the section on textbooks below. *ISL* is the main textbook.

| Date   | Day | Slides                                                                               | Reading     | Notebooks
| ------ | --- | ------------------------------------------------------------------------------------ | ----------- | ---------
| Jan 26 | Wed | [Introduction to ML](slides/intro/intro.pdf)                                         | ISL 1,2     | [intro](notebooks/intro/introduction.Rmd), [plots](slides/figs/intro/plots.R)
| Jan 31 | Mon | [Basic probability and stats](slides/prob/prob.pdf)                                  | MML 6       | 
| Feb 02 | Wed | [Basic linear algebra](slides/linalg/linalg.pdf)                                     | ISL 1, MML 2| See LAR
| Feb 07 | Mon | [Linear regression I](slides/linreg/linreg1.pdf)                                     | ISL 3.1-2   | [plots](slides/figs/class2/plots.R)
| Feb 09 | Wed | [Linear regression II](slides/linreg/linreg2.pdf)                                    | ISL 3.3-6   | [LR](notebooks/linreg/linear_regression.Rmd), [bias-var](notebooks/linreg/bias_variance.Rmd),[tides](notebooks/linreg/tides.Rmd)
| Feb 14 | Mon | [Linear algebra: Implementing linear regression](slides/linreg/solve.pdf)            | LAR         | [Source](slides/linreg/solve.Rmd)
| Feb 16 | Wed | [Logistic regression](slides/logreg/logreg.pdf)                                      | ISL 4.1-3   | 
| Feb 21 | Mon | [Generative models](slides/generate/generate.pdf) 		                              | ISL 4.4-5   | 
| Feb 23 | Wed | [Generalized linear models](slides/glm/glm.pdf),                                     | ISL 4.6     | [QQ](notebooks/linreg/qqplots.Rmd), [Notes](slides/glms/notes.pdf)
| Feb 28 | Mon | Generalized models and log likelihood                                                |             |
| Mar 02 | Wed | [Cross-validation](slides/cv/cv.pdf)                                                 | ISL 5       |
| Mar 07 | Mon | [Model selection, Lasso](slides/lasso/models.pdf)                                    | ISL 6.1-2   |
| Mar 09 | Wed | [Lasso and Bayesian ML](slides/lasso/map.pdf)                                        | ISL 6.3-4   | 
| Mar 14 | Mon | *spring break*
| Mar 16 | Wed | *spring break*
| Mar 21 | Mon | [Regression splines](slides/splines/splines_monkie.pdf), [Also](slides/splines/splines.pdf) | ISL 7 | [(poly)](notebooks/class10/polynomials.Rmd) [(splines)](notebooks/class10/spline_simple.Rmd)
| Mar 23 | Wed | [Decision trees and boosting](slides/trees/trees.pdf)                                | ISL 8       |
| Mar 28 | Mon | [Midterm review](#)                                                                  |             | 
| Mar 30 | Wed | [SVM I](slides/svm/svm1.pdf)                                                         | ISL 9.1-2   | [(Implementation)](notebooks/class14/separating_hyperplane.Rmd)
| Apr 04 | Mon | [SVM II](slides/svm/svm2.pdf)                                                        | ISL 9.3-5   | 
| Apr 06 | Wed | [Neural nets](slides/nnet/basics.pdf)                                                | ISL 10.1-3  | [(keras)](notebooks/class22/keras_example.Rmd)  
| Apr 11 | Mon | [Convolutional and recurrent neural nets](slides/nnet/convolve.pdf)                  | ISL 10.4-5  | 
| Apr 13 | Wed | [Fitting neural nets and optimization](slides/logreg/solve.pdf)                      | ISL 10.6-9, CO 9.2 | [(Optimization)](notebooks/class12_5/optimization.Rmd)
| Apr 18 | Mon | [Dimensionality reduction: PCA](slides/pca/pca.pdf)                                  | ISL 12.1-2  | [(PCA)](notebooks/class16)
| Apr 20 | Wed | [Bayesian machine learning](bayesian/ml.pdf)                                         | --          |
| Apr 25 | Mon | [k-means](slides/clustering/clustering.pdf) [EM](slides/clustering/em.pdf)           | ISL 12.4    |
| Apr 27 | Wed | [EM](slides/clustering/em.pdf) [Recommenders](slides/clustering/matrix.pdf)          | ISL 12.3    |
| May 02 | Mon | [Final review](#)
| May 04 | Wed | [Project presentations and discussion](#)                                            |             |
| May 09 | Mon | [Project presentations and discussion](#)                                            |             |



## Office Hours

- *Piazza*: [piazza.com/unh/spring2022/cs750and850](https://piazza.com/unh/spring2022/cs750and850 )

- *Marek*: Mon 2:00pm - 3:00pm at Kingsbury N215b
- *Soheil* (TA): Tue 10:00am - 12:00pm at Kingsbury W240
- *Jiahao* (Grader): Fri, Time 10:00am - 11:00am at Kingsbury W240

## Assignments

| Assignment                                 | Source                                       |  Due Date           |
|------------------------------------------- | -------------------------------------------- | --------------------|
| [1](assignments/assignment1.pdf)           | [1](assignments/assignment1.Rmd)             | Tue 2/01 at 11:59PM |
| [2](assignments/assignment2.pdf)           | [2](assignments/assignment2.Rmd)             | Tue 2/08 at 11:59PM |
| [3](assignments/assignment3.pdf)           | [3](assignments/assignment3.Rmd)             | Tue 2/15 at 11:59PM |
| [4](assignments/assignment4.pdf)           | [4](assignments/assignment4.Rmd)             | Tue 2/22 at 11:59PM |
| [5](assignments/assignment5.pdf)           | [5](assignments/assignment5.Rmd)             | Tue 3/01 at 11:59PM |
| [6](assignments/assignment6.pdf)           | [6](assignments/assignment6.Rmd)             | Tue 3/08 at 11:59PM |
| [7](assignments/assignment7.pdf)           | [7](assignments/assignment7.Rmd)             | Tue 3/22 at 11:59PM |
| [8](assignments/assignment8.pdf)           | [8](assignments/assignment8.Rmd)             | Tue 3/29 at 11:59PM |
| [9](assignments/assignment9.pdf)           | [9](assignments/assignment9.Rmd)             | Tue 4/05 at 11:59PM |
| [10](assignments/assignment10.pdf)         | [10](assignments/assignment10.Rmd)           | Tue 4/12 at 11:59PM |
| [11](assignments/assignment11.pdf)         | [11](assignments/assignment11.Rmd)           | Tue 4/19 at 11:59PM |
| [12](assignments/assignment12.pdf)         | [12](assignments/assignment12.Rmd)           | Tue 5/03 at 11:59PM |

## Project

See [Project Description](project/project.pdf) for the requirements and suggestions for the class project. 





## Quizzes

The projected dates for the quizzes are:

| Quiz          |  Dates Available|  
| ------------- | --------------- |
| Quiz 1        |  2/24-3/04      |
| Quiz 2        |  3/24-3/31      |
| Quiz 3        |  4/19-4/26      |
| Quiz 4        |  5/02-5/09      |

The quizzes will be available online on mycourses. See [practice questions](questions/test_questions.pdf) for the type of questions you may expect to be on the quizzes.

The quizzes are multiple choice and there will be 3 attempts allowed for each quiz. The best result counts.

## Exams

The final exam will be a take-home over the finals week. The exam will be based on conceptual/theoretical questions similar to homework assignments.

The exam is scheduled for May 12, 10am until May 14, 10am. The exam is take home and should not take more that 3 hours to complete. There will be no coding questions. 

## Textbooks ##

### Main Reference:
- **ISL**: James, G., Witten, D., Hastie, T., & Tibshirani, R. (2021).[An Introduction to Statistical Learning, (2nd)](https://www.statlearning.com/)

### More Machine Learning:
- **ESL**: Hastie, T., Tibshirani, R., & Friedman, J. (2009). [The Elements of Statistical Learning. Springer Series in Statistics (2nd ed.)](http://statweb.stanford.edu/~tibs/ElemStatLearn)
- **MLP**: Murphy, K (2012). Machine Learning, A Probabilistic Perspective.
- **ML**: Mitchell, T. (1997). Machine Learning
- Scholkopf B., Smola A. (2001). Learning with Kernels.
- **DL**: Goodfellow, I., Bengio, Y., & Courville, A. (2016). [Deep Learning](http://www.deeplearningbook.org/) 
- **PRM**: Bishop, C. M. (2006), [Pattern Recognition and Machine Learning](https://www.microsoft.com/en-us/research/uploads/prod/2006/01/Bishop-Pattern-Recognition-and-Machine-Learning-2006.pdf) 


### Probability and Statistics:
- **IST**: Lavine, M. (2005). [Introduction to statistical thought](http://people.math.umass.edu/~lavine/Book/book.html).
- Casella G., & Berger R. L. (2002) Statistical Inference, 2nd edition.
- **IP**: Grinstead, C., & Snell, J. (1998). [Introduction to probability](https://www.dartmouth.edu/~chance/teaching_aids/books_articles/probability_book/amsbook.mac.pdf).

### Linear Algebra:
- **MML**: Deisenroth, Faisal, Soon, (2020) [Mathematics for Machine Learning](https://mml-book.github.io/), 
- **LAR**: [Introductory Linear Algebra with R](http://bendixcarstensen.com/APC/linalg-notes-BxC.pdf)
- **LAO**: Hefferon, J. (2017) [Linear Algebra](http://joshua.smcvt.edu/linearalgebra/#current_version) (2017)
- **LA**: Strang, G. (2016) [Introduction to Linear Algebra](http://math.mit.edu/~gs/linearalgebra/).  *Also see:* [online lectures](https://ocw.mit.edu/courses/mathematics/18-06-linear-algebra-spring-2010/video-lectures/)

### Mathematical Optimization:
- **CO** Boyd, S., & Vandenberghe, L. (2004). [Convex Optimization](http://web.stanford.edu/~boyd/cvxbook/)
- Berstsekas, D., (2016), Nonlinear programming, Athena Scientific.
- Nocedal, J., Wright, S. (2006). [Numerical Optimization](https://www.springer.com/us/book/9780387303031)


### Related Areas:
- **RL**: Sutton, R. S., & Barto, A. (2018). [Reinforcement learning](http://people.inf.elte.hu/lorincz/Files/RL_2006/SuttonBook.pdf). 2nd edition 
- **RLA**: Szepesvari, C. (2013), [Algorithms for Reinforcement Learning](https://sites.ualberta.ca/~szepesva/RLBook.html)
- **AIMA**: Russell, S., & Norvig, P. (2013). Artificial Intelligence A Modern Approach. (3rd ed.).


## Programming Language

The class will involve hand-on data analysis using machine learning methods. The recommended language for programming assignments is [R](https://www.r-project.org/) which is an excellent tool for statistical analysis and machine learning. *No prior knowledge of R is needed or expected*; the book and lectures will include a gentle introduction to the language. You can also use Python instead if you are confident that you can install appropriate packages and figure out how to solve assignments. About 15% of the class used Python in 2019.

### R Resources

- [R For Data Science](https://r4ds.had.co.nz/index.html)
- [Cheatsheets (Markdown and others)](https://www.rstudio.com/resources/cheatsheets/)

We recommend using the free [R Studio](https://www.rstudio.com) for completing programming assignments. R Notebooks are very convenient for producing reproducible reports and we encourage you to use them.

If you are more adventurous, you may want to give nvim-r a try. It is a package for vim/nvim that has excellent R support.

### Python Resources

Book code is available for Python, for example, [here](https://github.com/JWarmenhoven/ISLR-python). If you find other good sources, please let me know. [Jupyter](https://jupyter.org/) is a similar alternative for Python. 

## Pre-requisites ##

Basic programming skills (scripting languages like Python are OK) and some familiarity with statistics and calculus. If in doubt, please email the instructor. 
