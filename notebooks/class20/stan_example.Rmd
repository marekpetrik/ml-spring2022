---
title: "Simple Stan Examples"
output:
  pdf_document: default
  html_notebook: default
---

First, we just need to load the necessary libraries and enable multi-core computation.
```{r}
library(rstan)
library(ggplot2)
rstan_options(auto_write = TRUE)
options(mc.cores = parallel::detectCores(TRUE))
```

Lets define a simple linear regression model. The model assumes only one feature and an expression as:
$$ y = \alpha + \beta x + \epsilon $$
The header of the block specifies the output variable in which the Stan model is stored.
```{stan,output.var="linear_regression"}
data {
    int<lower=0> N;
    vector[N] x;
    vector[N] y;
}
parameters {
    real alpha;
    real beta;
    real<lower=0> sigma;
}
model {
    y ~ normal(alpha + beta * x, sigma);
}
```


Use the Auto data set.
```{r}
auto <- ISLR::Auto
ggplot(auto, aes(x=horsepower,y=weight)) + geom_point() 
```


Compute linear regression.
```{r}
auto.fit <- lm(weight~horsepower, data=auto)
summary(auto.fit)
```
Try the same thing with Stan.
```{r}
data = list(x=auto$horsepower,y=auto$weight, N=length(auto$horsepower))
samples <- sampling(linear_regression, data = data, chains=1, iter=5000)
samples.df <- as.data.frame(samples)
```

MAP chooses the parameter with the maximum probability (also known as the mode). Median is easier to compute though.
```{r}
print(median(samples.df$alpha))
print(median(samples.df$beta))
```

But we also get a distribution!
```{r}
ggplot(samples.df, aes(x=beta)) + geom_histogram(bins=50)
```

And the joint ditribution of $\alpha$ and $\beta$ is:
```{r}
ggplot(samples.df, aes(x=beta,y=alpha)) + geom_density2d()
```

What about ridge regression?
```{stan,output.var="ridge_regression"}
data {
    int<lower=0> N;
    vector[N] x;
    vector[N] y;
}
parameters {
    real alpha;
    real beta;
    real<lower=0> sigma;
}
model {
    beta ~ normal(0,1);
    y ~ normal(alpha + beta * x, sigma);
}
```

Sample.
```{r}
data = list(x=auto$horsepower,y=auto$weight, N=length(auto$horsepower))
samples.ridge <- sampling(ridge_regression, data = data, chains=8, iter=5000)
samples.ridge.df <- as.data.frame(samples.ridge)
```

```{r}
print(median(samples.ridge.df$alpha))
print(median(samples.ridge.df$beta))
```


```{r}
ggplot(samples.ridge.df, aes(x=beta,y=alpha)) + geom_density2d()
```

