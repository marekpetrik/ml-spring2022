\documentclass{beamer}

\usepackage{booktabs}
\usepackage{natbib}
\usepackage{algorithm2e}
\usepackage{siunitx}
\usepackage{framed}
\usepackage{longtable}
\usepackage{amsmath}
\usepackage{amsthm}
\usepackage{grffile}
\usepackage{amssymb}

\newcommand{\opt}{^{\star}}

\usetheme[progressbar=frametitle]{metropolis}
\usepackage{libertine}



% *** Styles ***
\setbeamertemplate{navigation symbols}{}
\usecolortheme{dolphin}
%\usecolortheme{rose}
%\setbeamercovered{transparent}
\usefonttheme{professionalfonts}
%\usefonttheme[onlymath]{serif}

% *** Colors ***
\newcommand{\tc}[2]{\textcolor{#1}{#2}}
\newcommand{\tcb}[1]{\tc{blue}{#1}}
\newcommand{\tcr}[1]{\tc{red}{#1}}
\newcommand{\tcg}[1]{\tc{green}{#1}}
\newcommand{\tr}{^\top}


\newcommand{\Ex}{\mathbb{E}}
\newcommand{\var}{\operatorname{Var}}
%\newcommand{\Pr}{\mathbb{P}}

\definecolor{varcolor}{RGB}{132,23,49}
\newcommand{\varname}[1]{\textcolor{varcolor}{\mathsf{#1}}}

\newenvironment{mprog}{\begin{equation}\begin{array}{>{\displaystyle}l>{\displaystyle}l>{\displaystyle}l}}{\end{array}\end{equation}}
\newenvironment{mprog*}{\begin{equation*}\begin{array}{>{\displaystyle}l>{\displaystyle}l>{\displaystyle}l}}{\end{array}\end{equation*}}
\newcommand{\stc}{\\[1ex] \subjectto}
\newcommand{\subjectto}{\mbox{s.t.} &}
\newcommand{\minimize}[1]{\min_{#1} &}
\newcommand{\maximize}[1]{\max_{#1} &}
\newcommand{\minsep}[2]{\min_{#1 \; \vline \; #2} &}
\newcommand{\maxsep}[2]{\max_{#1 \; \vline \; #2} &}
\newcommand{\cs}{\\[1ex] & }

\title{Support Vector Machines}
\subtitle{Maximum Margin Classifiers}
\author{Marek Petrik}
\date{March 30th, 2022}

\begin{document}

\begin{frame} \maketitle
\end{frame}


\begin{frame} \frametitle{Regression Trees}
\begin{itemize}
	\item Predict Baseball $\varname{Salary}$ based on $\varname{Years}$ played and $\varname{Hits}$
	\item Example:
	\begin{center}\includegraphics[width=0.5\linewidth]{{../islrfigs/Chapter8/8.1}.pdf}\end{center}
\end{itemize}
\end{frame}

\begin{frame} \frametitle{Tree Partition Space}
\begin{center}
	\includegraphics[width=0.4\linewidth]{{../islrfigs/Chapter8/8.1}.pdf} \includegraphics[width=0.5\linewidth]{{../islrfigs/Chapter8/8.2}.pdf}
\end{center}
\end{frame}

\begin{frame} \frametitle{Learning a Decision Tree}
\begin{itemize}
	\item NP Hard problem
	\vfill
	\item<2-> Approximate algorithms (heuristics):
	\begin{itemize}
		\item ID3, C4.5, C5.0 (classification)
		\item CART (classification and regression trees)
		\item MARS (regression trees)
		\item \ldots
	\end{itemize}
\end{itemize}
\end{frame}

\begin{frame} \frametitle{Classification Trees: Metrics}
	\begin{itemize}
		\item $\hat{p}_{mk}$ is proportion of observations in $R_m$ in class $k$
		\item Partition to \emph{minimize} a metric:
		\begin{enumerate}
			\item Classification error rate (of the best class)
			\[ 1 - \max_k \hat{p}_{mk} = \min_k (1 - \hat{p}_{mk} ) \]
			Often too pessimistic in practice
			\item Gini (impurity) index (CART):
			\[ \sum_{k=1}^K \hat{p}_{mk} (1-\hat{p}_{m_k}) \]
			\item Cross-entropy (information gain) (ID3, C4.5):
			\[ - \sum_{k=1}^K \hat{p}_{mk} \log \hat{p}_{m_k} \]
		\end{enumerate}
		\item ID3, C4.5 do not prune
	\end{itemize}
\end{frame}

\begin{frame} \frametitle{Bagging}
\begin{itemize}
	\item Stands for ``Bootstrap Aggregating''
	\item Construct multiple bootstrapped training sets:
	\[ T_1, T_2, \ldots, T_B \]
	\item Fit a tree to each one:
	\[  \hat{f}_1, \hat{f}_2, \ldots , \hat{f}_B \]
	\item Make predictions by averaging individual tree predictions
	\[  \hat{f}(x) = \frac{1}{B} \sum_{b=1}^B \hat{f}_b(x)\]
	\item Large values of $B$ are not likely to overfit, $B \approx 100$ is a good choice
\end{itemize}
\end{frame}

\begin{frame} \frametitle{Random Forests}
\begin{itemize}
\item Many trees in bagging will be similar
\item Algorithms choose the same features to split on
\item Random forests help to address similarity:
\begin{itemize}
	\item At each split, choose only from $m$  randomly sampled features
\end{itemize}
\item Good empirical choice is $m = \sqrt{p}$
\end{itemize}
\begin{center}
\vspace{-10mm}
\includegraphics[width=0.6\linewidth]{{../islrfigs/Chapter8/8.10}.pdf}\end{center}
\end{frame}

\begin{frame} \frametitle{Gradient Boosting (Regression)}
\begin{itemize}
	\item Boosting uses all of data, not a random subset (usually)
	\item Also builds trees $\hat{f}_1, \hat{f}_2, \ldots$
	\item \alert{and} weights $\lambda_1, \lambda_2, \ldots$
	\item Combined prediction:
	\[ \hat{f}(x)  = \sum_{i} \lambda_i \hat{f}_i(x) \]
	\item  Assume we have $1 \ldots m$ trees and weights, next best tree?
\end{itemize}
\end{frame}

\begin{frame} \frametitle{Gradient Boosting (Regression)}
\begin{itemize}
\item Just use \alert{gradient descent}
\item \textbf{Objective} is to minimize RSS (1/2):
\[ \frac{1}{2} \sum_{i=1}^n (y_i - f(x_i))^2 \]
\item \textbf{Objective} with the new tree $m+1$:
\[ \frac{1}{2} \sum_{i=1}^n \left(y_i - \sum_{j=1}^m \lambda_j \hat{f}_j(x_i) - \alert{\hat{f}_{m+1}(x_i)} \right)^2 \]
\item Greatest reduction in RSS: \alert{gradient}
\[ y_i - \sum_{j=1}^m \lambda_j \hat{f}_j(x_i) \approx \alert{\hat{f}_{m+1}(x_i)} \]
\end{itemize}
\end{frame}


\begin{frame} \frametitle{Machine Learning Choices ...}
\centering
\includegraphics[width=\linewidth]{../figs/class1/ml_map.png}\\
{\tiny Source: \url{http://scikit-learn.org/stable/tutorial/machine_learning_map/index.html}} \\[3mm]
\end{frame}

\begin{frame} \frametitle{Today: Support Vector Machines}
\begin{enumerate}
\item Maximum margin classifier
\item Slack variables for non-separable classese
\item Statistical learning theory: Why maximum margin
\item Next time: Kernels and Support Vector Machines
\end{enumerate}
\end{frame}

\begin{frame}
  \frametitle{Support Vector Machines}
  \begin{quotation}
     SVMs are currently a hot topic in the machine learning community, creating a similar enthusiasm at the moment as Artificial Neural Networks used to do before. Far from being a panacea, SVMs yet represent a powerful technique for general (nonlinear) classification, regression and outlier detection with an intuitive model representation.
   \end{quotation}
   \begin{flushright}
     ``Hype of Hallelujah?'', Bennett and Campbell, 2000.
   \end{flushright}
\end{frame}


\begin{frame} \frametitle{Separating Hyperplane}
	\begin{center}\includegraphics[width=0.6\linewidth]{{../islrfigs/Chapter9/9.1}.pdf}\end{center}
	\only<1>{Hyperplane: $\beta_0 + x\tr \beta = 0$}
	\only<2>{Blue: $\beta_0 + x\tr \beta > 0$}
	\only<3>{Red: $\beta_0 + x\tr \beta < 0$}
\end{frame}

\begin{frame} \frametitle{Question}
	\begin{itemize}
		\item Which other classification methods classify using a separating hyperplane?
	\end{itemize}
\end{frame}

\begin{frame} \frametitle{Best Separating Hyperplane}
	\begin{center}\includegraphics[width=\linewidth]{{../islrfigs/Chapter9/9.2}.pdf}\end{center}
	\begin{itemize}
		\item Data is separable
		\item Why would either one be better than others?
	\end{itemize}
\end{frame}

\begin{frame} \frametitle{Separating Hyperplane Methods}
	How is it computed?
	\begin{itemize}
		\item \textbf{Logistic regression}: \visible<2->{Maximum likelihood}
		\vfill
		\item<3-> \textbf{LDA}: \visible<4->{Maximum likelihood}
		\vfill
		\item<5-> \textbf{Support vector machines}: Maximum margin
	\end{itemize}
\end{frame}

\begin{frame} \frametitle{Maximum Margin Hyperplane}
	\begin{center}\includegraphics[width=0.6\linewidth]{{../islrfigs/Chapter9/9.3}.pdf}\end{center}
\end{frame}

\begin{frame} \frametitle{Computing Maximum Margin Hyperplane}
	\begin{center}\includegraphics[width=0.3\linewidth]{{../islrfigs/Chapter9/9.3}.pdf}\end{center}
	\begin{itemize}
		\item \textbf{Class labels}: $y_i \in \{-1,+1\}$ (\alert{not $\{0,1\}$})
		\item Solve a \textbf{quadratic program} (assume one of the features is a constant to get the equivalent of an intercept)
		\begin{mprog*}
		\maximize{\beta,M} \qquad M
		\stc y_i (\beta\tr x_i) \ge M
		\cs \| \beta \|_2 = 1
		\end{mprog*}
	\end{itemize}
\end{frame}

\begin{frame} \frametitle{Non-separable Case}
	\begin{itemize}
		\item Rarely lucky enough to get separable classes
	\end{itemize}
	\begin{center}\includegraphics[width=0.7\linewidth]{{../islrfigs/Chapter9/9.4}.pdf}\end{center}
\end{frame}

\begin{frame} \frametitle{Almost Unseparable Cases}
	\begin{itemize}
		\item Maximum margin can be brittle even when classes are separable
	\end{itemize}
	\begin{center}\includegraphics[width=\linewidth]{{../islrfigs/Chapter9/9.5}.pdf}\end{center}
\end{frame}

\begin{frame} \frametitle{Introducing Slack Variables}
	\begin{itemize}
		\item \textbf{Maximum margin classifier}
		\begin{mprog*}
			\maximize{\beta,M} \qquad M
			\stc y_i (\beta\tr x_i) \ge M
			\cs \| \beta \|_2 = 1
		\end{mprog*}
		\item \textbf{Support Vector Classifier} a.k.a Linear SVM
		\begin{mprog*}
			\maximize{\beta,M, \epsilon\ge 0} \qquad M
			\stc y_i (\beta\tr x_i) \ge (M - \alert{\epsilon_i})
			\cs \| \beta \|_2 = 1
			\cs \alert{\| \epsilon \|_1 \le C}
		\end{mprog*}
		\item Slack variables: $\epsilon$
		\item Parameter: $C$ \visible<2>{What if $C=0$?}
	\end{itemize}
\end{frame}

\begin{frame} \frametitle{Effect of Decreasing Parameter $C$}
	\begin{center}\includegraphics[width=0.7\linewidth]{{../islrfigs/Chapter9/9.7}.pdf}\end{center}
\end{frame}

\begin{frame} \frametitle{Why Maximum Margin: Statistical Learning Theory}
  \begin{itemize}
  \item Maximum margin classifier: Most likely to generalize among all linear classifiers
  \item Based on: \\
    \begin{small}
    Bousquet, O., Boucheron, S., and Lugosi, G. \emph{Introduction to Statistical Learning Theory}, Advanced Lectures on Machine Learning 3176 (2004): 169–207.
    \end{small}
    \vfill 
  \item \textbf{Classifier}: $g \colon \mathcal{X} \to  \{-1,1\}$
  \item \textbf{Classification error}: $R(g) = \Pr[g(X) \neq Y]$
    \item Bayes risk: $R\opt  = \inf_{g\in \mathcal{G}} R(g)$
  \end{itemize}
\end{frame}

\begin{frame} \frametitle{Empirical Risk Minimization}
  Training set
  \[ X_1, \dots , X_n \qquad \text{and} \quad Y_1, \dots , Y_n \]
  Empirical risk (training error)
  \[ R_n(g) = \frac{1}{n} \sum_{i=1}^{n} \mathds{1}_{g(X_i) \neq Y_i}   \]
  Empirical risk minimization (minimize error on training set)
 \[ g_n \in \arg\min_{g \in \mathcal{G}} R_n(g)  \] 
\end{frame}

\begin{frame}
  \frametitle{Empirical Risk Minimization}
 \begin{center}
 \includegraphics[width=0.8\linewidth]{../figs/statlearning/statlearning.png}
 \end{center}

 \textbf{Generalization error}:
 \begin{align*}
   R(g_n) - R(g\opt) &\le R(g_n) - R_n(g_n) + R_n(g_n) - R(g\opt)  \\
                     &\le \bigl( R(g_n) - R_n(g_n) \bigr) + \bigl( R_n(g\opt) + R(g\opt)\bigr)  \\
   &\le 2\cdot \sup_{g\in \mathcal{G}} |R(g) - R_n(g)|
\end{align*}
\end{frame}

\begin{frame} \frametitle{Generalization Error Bounds}
  \textbf{Test error} (for some $g$ independent of data) with probability $1-\delta$ is
  \[ |R(g) - R_n(g)| \le \sqrt{ \frac{\log \frac{2}{\delta }}{2n} }\]
  \vfill 
  \textbf{Training error} ($g_n$ computed from data) with probability $1-\delta$ is
  \[  |R(g_n) - R_n(g_n)| \le \sqrt{\frac{\log  |\mathcal{G}| + \log \frac{1}{\delta }}{2n}} \]
  The generalization depends on the size $|\mathcal{G}|$ of the set of possible classifiers\\

  How large is $|\mathcal{G}|$ ?
\end{frame}

\end{document}

%%% Local Variables:
%%% mode: latex
%%% TeX-master: t
%%% End:
