\documentclass{beamer}

\usepackage{booktabs}
\usepackage{natbib}
\usepackage{algorithm2e}
\usepackage{siunitx}
\usepackage{framed}
\usepackage{longtable}
\usepackage{amsmath}
\usepackage{amsthm}
\usepackage{grffile}
\usepackage{amssymb}

\newcommand{\opt}{^{\star}}

\usetheme[progressbar=frametitle]{metropolis}
\usepackage{libertine}

% *** Styles ***
\setbeamertemplate{navigation symbols}{}
\usecolortheme{dolphin}
%\usecolortheme{rose}
%\setbeamercovered{transparent}
\usefonttheme{professionalfonts}
%\usefonttheme[onlymath]{serif}

% *** Colors ***
\newcommand{\tc}[2]{\textcolor{#1}{#2}}
\newcommand{\tcb}[1]{\tc{blue}{#1}}
\newcommand{\tcr}[1]{\tc{red}{#1}}
\newcommand{\tcg}[1]{\tc{green}{#1}}
\newcommand{\tr}{^\top}

\newcommand{\Ex}{\mathbb{E}}
\newcommand{\var}{\operatorname{Var}}
%\newcommand{\Pr}{\mathbb{P}}

\definecolor{varcolor}{RGB}{132,23,49}
\newcommand{\varname}[1]{\textcolor{varcolor}{\mathsf{#1}}}

\newenvironment{mprog}{\begin{equation}\begin{array}{>{\displaystyle}l>{\displaystyle}l>{\displaystyle}l}}{\end{array}\end{equation}}
\newenvironment{mprog*}{\begin{equation*}\begin{array}{>{\displaystyle}l>{\displaystyle}l>{\displaystyle}l}}{\end{array}\end{equation*}}
\newcommand{\stc}{\\[1ex] \subjectto}
\newcommand{\subjectto}{\mbox{s.t.} &}
\newcommand{\minimize}[1]{\min_{#1} &}
\newcommand{\maximize}[1]{\max_{#1} &}
\newcommand{\minsep}[2]{\min_{#1 \; \vline \; #2} &}
\newcommand{\maxsep}[2]{\max_{#1 \; \vline \; #2} &}
\newcommand{\cs}{\\[1ex] & }

\title{Support Vector Machines}
\author{Marek Petrik}
\date{Apr 4th, 2022}

\begin{document}

\begin{frame} \maketitle
\end{frame}


\begin{frame} \frametitle{Today: Support Vector Machines}
\begin{enumerate}
\item Last time: Maximum margin classifier
\item Last time: Slack variables for non-separable classese
\item Statistical learning theory: Why maximum margin
\item Kernels and Support Vector Machines
\end{enumerate}
\end{frame}

\begin{frame} \frametitle{Separating Hyperplane}
	\begin{center}\includegraphics[width=0.6\linewidth]{{../islrfigs/Chapter9/9.1}.pdf}\end{center}
	\only<1>{Hyperplane: $\beta_0 + x\tr \beta = 0$}
	\only<2>{Blue: $\beta_0 + x\tr \beta > 0$}
	\only<3>{Red: $\beta_0 + x\tr \beta < 0$}
\end{frame}

\begin{frame} \frametitle{Best Separating Hyperplane}
	\begin{center}\includegraphics[width=\linewidth]{{../islrfigs/Chapter9/9.2}.pdf}\end{center}
	\begin{itemize}
		\item Data is separable
		\item Why would either one be better than others?
	\end{itemize}
\end{frame}

\begin{frame} \frametitle{Maximum Margin Hyperplane}
	\begin{center}
	\includegraphics[width=0.6\linewidth]{{../islrfigs/Chapter9/9.3}.pdf}
	\end{center}
\end{frame}

\begin{frame} \frametitle{Non-separable Case}
	\begin{itemize}
		\item Rarely lucky enough to get separable classes
	\end{itemize}
	\begin{center}\includegraphics[width=0.7\linewidth]{{../islrfigs/Chapter9/9.4}.pdf}\end{center}
\end{frame}

\begin{frame} \frametitle{Almost Unseparable Cases}
	\begin{itemize}
		\item Maximum margin can be brittle even when classes are separable
	\end{itemize}
	\begin{center}\includegraphics[width=\linewidth]{{../islrfigs/Chapter9/9.5}.pdf}\end{center}
\end{frame}

\begin{frame} \frametitle{Introducing Slack Variables}
	\begin{itemize}
		\item \textbf{Maximum margin classifier}
		\begin{mprog*}
			\maximize{\beta,M} \qquad M
			\stc y_i (\beta\tr x_i) \ge M, \quad i = 1,\ldots,m
			\cs \| \beta \|_2 = 1
		\end{mprog*}
		\item \textbf{Support Vector Classifier} a.k.a Linear SVM
		\begin{mprog*}
			\maximize{\beta,M, \epsilon\ge 0} \qquad M
			\stc y_i (\beta\tr x_i) \ge (M - \alert{\epsilon_i}), \quad i = 1,\ldots,m
			\cs \| \beta \|_2 = 1
			\cs \alert{\| \epsilon \|_1 \le C}
		\end{mprog*}
		\item Slack variables: $\epsilon$
		\item Parameter: $C$ \visible<2>{What if $C=0$?}
	\end{itemize}
\end{frame}

\begin{frame} \frametitle{Effect of Decreasing Parameter $C$}
	\begin{center}\includegraphics[width=0.7\linewidth]{{../islrfigs/Chapter9/9.7}.pdf}\end{center}
\end{frame}


\begin{frame} \frametitle{Why Maximum Margin: Statistical Learning Theory}
  \begin{itemize}
  \item Maximum margin classifier: Most likely to generalize among all linear classifiers
  \item Based on: \\
    \begin{small}
    Bousquet, O., Boucheron, S., and Lugosi, G. \emph{Introduction to Statistical Learning Theory}, Advanced Lectures on Machine Learning 3176 (2004): 169–207.
    \end{small}
    \vfill 
  \item \textbf{Classifier}: $g \colon \mathcal{X} \to  \{-1,1\}$
  \item \textbf{Classification error}: $R(g) = \Pr[g(X) \neq Y]$
    \item Bayes risk: $R\opt  = \inf_{g\in \mathcal{G}} R(g)$
  \end{itemize}
\end{frame}

\begin{frame} \frametitle{Empirical Risk Minimization}
  Training set
  \[ X_1, \dots , X_n \qquad \text{and} \quad Y_1, \dots , Y_n \]
  Empirical risk (training error)
  \[ R_n(g) = \frac{1}{n} \sum_{i=1}^{n} \bf{1}_{g(X_i) \neq Y_i}   \]
  Empirical risk minimization (minimize error on training set)
 \[ g_n \in \arg\min_{g \in \mathcal{G}} R_n(g)  \] 
\end{frame}

\begin{frame}
  \frametitle{Empirical Risk Minimization}
 \begin{center}
 \includegraphics[width=0.8\linewidth]{../figs/statlearning/statlearning.png}
 \end{center}

 \textbf{Generalization error}:
 \begin{align*}
   R(g_n) - R(g\opt) &\le R(g_n) - R_n(g_n) + R_n(g_n) - R(g\opt)  \\
                     &\le \bigl( R(g_n) - R_n(g_n) \bigr) + \bigl( R_n(g\opt) + R(g\opt)\bigr)  \\
   &\le 2\cdot \sup_{g\in \mathcal{G}} |R(g) - R_n(g)|
\end{align*}
\end{frame}

\begin{frame} \frametitle{Generalization Error Bounds}
  \textbf{Test error} (for some $g$ independent of data) with probability $1-\delta$ is
  \[ |R(g) - R_n(g)| \le \sqrt{ \frac{\log \frac{2}{\delta }}{2n} }\]
  \vfill 
  \textbf{Training error} ($g_n$ computed from data) with probability $1-\delta$ is
  \[  |R(g_n) - R_n(g_n)| \le \sqrt{\frac{\log  |\mathcal{G}| + \log \frac{1}{\delta }}{2n}} \]
  The generalization depends on the size $|\mathcal{G}|$ of the set of possible classifiers\\

  How large is $|\mathcal{G}|$ ?
\end{frame}

\begin{frame} \frametitle{What About Nonlinearity?}
	\begin{center}\includegraphics[width=\linewidth]{{../islrfigs/Chapter9/9.8}.pdf}\end{center}
\end{frame}

\begin{frame} \frametitle{Nonlinear Classification}
	\begin{itemize}
		\item Nonlinear boundary by introducing feature transformations
		\item One feature: $x_1$
		\item Generate additional features: $\phi(x_1) = [x_1, x_1^2, x_1^3, \ldots]$
		\item But what if there are many features already: $x_1, x_2, x_3$
		\item Then many many combinations:
		\[ \phi([x_1,x_2,x_3]) = [x_1, x_1 \cdot x_2, x_1 \cdot x_3, x_2 \cdot x_3, x_1^2, x_2^2, \ldots] \]
		\item Becomes intractable quickly
		\item SVM can handle feature transformations with possibly \emph{infinite number of terms}
	\end{itemize}
\end{frame}

\begin{frame}{Lifting Feature Space}
	\begin{center}
		\includegraphics[width=0.9\linewidth]{../figs/class15/2d-to-3d-projection.jpeg}
		{\tiny source: \url{https://prateekvjoshi.com/2012/08/24/support-vector-machines/}}
	\end{center}
\end{frame}

\begin{frame}{Scaling Up SVMs}
	Lifted maximum margin classifier
	\begin{mprog*}
		\maximize{\beta,M} \qquad M
		\stc y_i (\beta\tr \phi(x_i)) \ge M, \quad i = 1,\ldots,m
		\cs \| \beta \|_2 = 1
	\end{mprog*}
	\begin{itemize}
		\item Say lifted space is 50000 transformed features
		\item Then $\beta \in \mathbb{R}^{50000}$
		\item which is intractable
	\end{itemize}
\end{frame}

\begin{frame} \frametitle{Dealing with Nonlinearity}
	\begin{itemize}
		\item It is possible to do better with SVMs
		\item \textbf{\underline{Primal} Quadratic Program}
		\begin{mprog*}
		\maximize{\beta,M} \qquad M
		\stc y_i (\beta\tr x_i) \ge M, \quad i = 1,\ldots,m
		\cs \| \beta \|_2 = 1
		\end{mprog*}
		\item Equivalent \textbf{\underline{Dual} Quadratic Program}  (usually max-min, not here)
		\begin{mprog*}
			\maximize{\alpha\ge 0} \sum_{l=1}^m \alpha_l-\frac{1}{2} \sum_{j,k=1}^{m} \alpha_j \alpha_k y_j y_k \langle x_j, x_k \rangle
			\stc \sum_{l=1}^m \alpha_l y_l = 0
		\end{mprog*}
	\end{itemize}
\end{frame}

\begin{frame} \frametitle{SVM Dual Representation}
	\begin{itemize}
		\item \textbf{\underline{Dual} Quadratic Program}  (usually max-min, not here)
		\begin{mprog*}
		\maximize{\alpha\ge 0} \sum_{l=1}^m \alpha_l-\frac{1}{2} \sum_{j,k=1}^{m} \alpha_j \alpha_k y_j y_k \alert<2>{\langle x_j, x_k \rangle}
		\stc \sum_{l=1}^m \alpha_l y_l = 0
		\end{mprog*}
		\item \textbf{Representer theorem}: (classification test):
		\[f(z) = \sum_{l=1}^m \alpha_l y_l \alert<2>{\langle z, x_l\rangle} > 0\]
		\item<2-> Only need the inner product between data points
		\item<3-> Define a \textbf{kernel function} by projecting data to higher dimensions:
		\[ k(x_1,x_2) = \langle \phi(x_1), \phi(x_2) \rangle \]
	\end{itemize}
\end{frame}

\begin{frame}{Lifting Feature Space}
    \[ k(x_1,x_2) = \langle \phi(x_1), \phi(x_2) \rangle \]
	\begin{center}
		\includegraphics[width=0.8\linewidth]{../figs/class15/2d-to-3d-projection.jpeg}
		{\tiny source: \url{https://prateekvjoshi.com/2012/08/24/support-vector-machines/}}
	\end{center}
\end{frame}

\begin{frame} \frametitle{Kernelized SVM}
	\begin{itemize}
		\item \textbf{\underline{Dual} Quadratic Program} (usually max-min, not here)
		\begin{mprog*}
		\maximize{\alpha\ge 0} \sum_{l=1}^m \alpha_l-\frac{1}{2} \sum_{j,k=1}^{m} \alpha_j \alpha_k y_j y_k \alert{k(x_j, x_k )}
		\stc \sum_{l=1}^M \alpha_l y_l = 0
		\end{mprog*}
		\item \textbf{Representer theorem}: (classification test):
		\[f(z) = \sum_{l=1}^m \alpha_l y_l \alert{k(z, x_l)} > 0\]
	\end{itemize}
\end{frame}

\begin{frame} \frametitle{Kernels}
	\begin{itemize}
		\item Polynomial kernel: ($d$ is a coefficient)
		\[ k(x_1,x_2) = \left( 1 + x_1\tr x_2 \right)^d\]
		\item Radial kernel: ($\gamma$ is a coefficient)
		\[ k(x_1,x_2) = \exp\left( -\gamma \| x_1 - x_2 \|_2^2 \right) \]
		\item Many many more: Distance measure must be \textbf{positive definite}.
	\end{itemize}
\end{frame}

\begin{frame} \frametitle{Polynomial and Radial Kernels}
	\begin{center}\includegraphics[width=\linewidth]{{../islrfigs/Chapter9/9.9}.pdf}\end{center}
\end{frame}

\begin{frame} \frametitle{Polynomial Kernel}
	\[ k(x_1,x_2) = \left( 1 + x_1\tr x_2 \right)^d \]
	\begin{itemize}
		\item Common choice $d = 2$
		\item Recall that:
		\[ k(x_1,x_2) = \langle  \phi(x_1), \phi(x_2) \rangle = \phi(x_1)\tr \phi(x_2) \]
		\item What is $\phi$ in a polynomial kernel?
		\pause
		\item Assume $2$ features: $x_{11}, x_{1,2}$
		\item Polynomial expansion of features:
		\[ \phi(x_1) = \begin{pmatrix}
		1 & 2 x_{11} & 2 x_{12} & x_{11}^2 & x_{12}^2 & x_{11} x_{12}
		\end{pmatrix} \]
	\end{itemize}
\end{frame}

\begin{frame} \frametitle{Other Popular Kernels}
	\begin{itemize}
		\item \textbf{Fisher kernel}: Motivated by statistical properties
		\item \textbf{String kernels}: Weighted sum of common substrings and other variations
		\item \textbf{Graph kernels}: Similarity between two graphs
		\item Problem specific kernels
	\end{itemize}
\end{frame}

\begin{frame} \frametitle{Custom Kernels}
	\begin{itemize}
		\item Can any function $k(x_1,x_2)$ be used as a kernel?
		\pause
		\item No. Two options.
		\pause
		\item \textbf{Option 1}: Positive semi-definite and must satisfy Mercer's conditions. Then:
		\[ k(x_1,x_2) = \langle  \phi(x_1), \phi(x_2) \rangle \]
		\item \textbf{Option 2:} Positive semi-definite only. SVM will work just fine, but for any $\phi$:
		\[ k(x_1,x_2) \neq \langle  \phi(x_1), \phi(x_2) \rangle \]
	\end{itemize}
\end{frame}

\begin{frame} \frametitle{SVM vs LDA: Train}
	\begin{center}\includegraphics[width=\linewidth]{{../islrfigs/Chapter9/9.10}.pdf}\end{center}
\end{frame}

\begin{frame} \frametitle{SVM vs LDA: Test}
	\begin{center}\includegraphics[width=\linewidth]{{../islrfigs/Chapter9/9.11}.pdf}\end{center}
\end{frame}

\begin{frame} \frametitle{Multiple Classes}
	\begin{itemize}
		\item One-vs-one
		\vfill
		\item One-vs-all
	\end{itemize}
\end{frame}

\begin{frame} \frametitle{SVM vs Logistic Regression}
	\begin{itemize}
		\item \textbf{Logistic regression}: Minimize negative log likelihood
		\item \textbf{SVM}: Minimize \underline{hinge loss}
		\begin{center}\includegraphics[width=0.6\linewidth]{{../islrfigs/Chapter9/9.12}.pdf}\end{center}
	\end{itemize}
Use SVM when classes are well separated or there is a good \emph{kernel}
\end{frame}

  
\end{document}

%%% Local Variables:
%%% mode: latex
%%% TeX-master: "svm2"
%%% End:
