\documentclass{beamer}


\let\val\undefined
\usepackage{pgf}
\usepackage{pgfplots}
\usepackage{tikz}
\usepackage{booktabs}
\usepackage{natbib}
\usepackage[ruled,linesnumbered]{algorithm2e}
\usepackage{siunitx}
\usepackage{framed}
\usepackage{longtable}
\usepackage{amsmath}
\usepackage{amsthm}
\usepackage{nicefrac}
\usepackage{grffile}

\usetheme[progressbar=frametitle]{metropolis}
\usepackage{libertine}

\usetikzlibrary{arrows,automata,backgrounds,positioning,decorations,intersections,matrix}

% *** Styles ***
\setbeamertemplate{navigation symbols}{}
\usecolortheme{dolphin}
%\usecolortheme{rose}
%\setbeamercovered{transparent}
\usefonttheme{professionalfonts}
%\usefonttheme[onlymath]{serif}

% *** Colors ***
\newcommand{\tc}[2]{\textcolor{#1}{#2}}
\newcommand{\tcb}[1]{\tc{blue}{#1}}
\newcommand{\tcr}[1]{\tc{red}{#1}}
\newcommand{\tcg}[1]{\tc{green}{#1}}

\def\checkmark{\tikz\fill[scale=0.4](0,.35) -- (.25,0) -- (1,.7) -- (.25,.15) -- cycle;}

\newcommand{\Ex}{\mathbb{E}}
\newcommand{\var}{\operatorname{Var}}
%\newcommand{\Pr}{\mathbb{P}}

\definecolor{varcolor}{RGB}{132,23,49}
\newcommand{\varname}[1]{\textcolor{varcolor}{\mathsf{#1}}}

\title{Model Selection and Regularization}
\subtitle{Selecting/Constructing Features}
\author{Marek Petrik}
\date{Mar 7, 2022}

\begin{document}

\begin{frame} \maketitle \end{frame}

\begin{frame} \frametitle{Last Time}
	\begin{itemize}
		\item Successfully using basic machine learning methods
		\item Problems:
		\begin{enumerate}
			\item How well is the machine learning method doing
			\item Which method is best for my problem?
			\item How many features (and which ones) to use?
			\item What is the uncertainty in the learned parameters?
		\end{enumerate}
		\item<2-> Methods:
		\begin{enumerate}
			\item Validation set
			\item Leave one out cross-validation
			\item k-fold cross validation
			\item Bootstrapping
		\end{enumerate}
	\end{itemize}
\end{frame}

\begin{frame} \frametitle{Solution 1: Validation Set}
	\begin{itemize}
		\item Just evaluate how well the method works on a validation set
		\item \textbf{Randomly} split data to:
		\begin{enumerate}
			\item \underline{Training set}: about half of all data
			\item \underline{Validation set} (AKA hold-out set): remaining half
		\end{enumerate}
		\begin{center}\includegraphics[width=\linewidth]{{../islrfigs/Chapter5/5.1}.pdf}\end{center}
		\item<2-> Choose the number of features/representation based on minimizing error on \textbf{validation set}
	\end{itemize}
\end{frame}

\begin{frame} \frametitle{Solution 2: Leave-one-out}
	\begin{itemize}
		\item Addresses problems with validation set
		\item Split the data set into 2 parts:
		\begin{enumerate}
			\item \underline{Training}: Size $n-1$
			\item \underline{Validation}: Size $1$
		\end{enumerate}
		\item Repeat $n$ times: to get $n$ learning problems
		\begin{center}\includegraphics[width=0.8\linewidth]{{../islrfigs/Chapter5/5.3}.pdf}\end{center}
	\end{itemize}
\end{frame}

\begin{frame} \frametitle{Solution 3: k-fold Cross-validation}
	\begin{itemize}
		\item Hybrid between validation set and LOO
		\item Split training set into $k$ subsets
		\begin{enumerate}
			\item \underline{Training set}: $n - \nicefrac{n}{k} $
			\item \underline{Validation set}: $\nicefrac{n}{k}$
		\end{enumerate}
		\item $k$ learning problems
		\begin{center}\includegraphics[width=0.8\linewidth]{{../islrfigs/Chapter5/5.5}.pdf}\end{center}
		\item Cross-validation error:
		\[ \operatorname{CV}_{(k)} = \frac{1}{k} \sum_{i=1}^k \operatorname{MSE}_i \]
	\end{itemize}
\end{frame}

\begin{frame} \frametitle{Limits of Cross-validation}
	\begin{itemize}
		\item Successfully using basic machine learning methods
		\item Cross-validation is not the end of the story
		\item<2->  Feasible to test options:
		\begin{enumerate}
			\item $\varname{mpg} = \beta_0 + \beta_1 \, \varname{power} $
			\item $\varname{mpg} = \beta_0 + \beta_1 \, \varname{power} + \beta_2 \, \varname{power}^2$
			\item $\varname{mpg} = \beta_0 + \beta_1 \, \varname{power} + \beta_2 \, \varname{power}^2 + \beta_3 \, \varname{power}^3 $
			\item $\varname{mpg} = \beta_0 + \beta_1 \, \varname{power} + \beta_2 \, \varname{power}^2 + \beta_3 \, \varname{power}^3 + \beta_4 \, \varname{power}^4$
			\item $\ldots$
		\end{enumerate}
		\item<3-> This is just one feature!
		\item<3-> What is we add $\varname{displacement}$, $\varname{weight}$, $\varname{top speed}$, $\varname{wheel size}$, $\ldots$?
		\item<4-> Exponential growth!
	\end{itemize}
\end{frame}

\begin{frame} \frametitle{Today}
	\begin{itemize}
		\item How to choose the right features if we have (too) many options
		\vfill
		\item Methods:
		\begin{enumerate}
			\item Subset selection
			\item Regularization (shrinkage)
			\item Dimensionality reduction (next class)
		\end{enumerate}
	\end{itemize}
\end{frame}

%\begin{frame} \frametitle{Importance of Feature Engineering}
%	\begin{itemize}
%		\item MNIST handwritten digit recognition (see R notebook)
%		\vfill
%		\item Example results: see \url{http://yann.lecun.com/exdb/mnist/}
%	\end{itemize}
%end{frame}


\begin{frame} \frametitle{Why Few Features}
	\begin{enumerate}
		\item \textbf{Improve prediction accuracy}: reduce overfitting
		\vfill
		\item\textbf{Improve interpretability}: small number of coefficients are easier to understand
	\end{enumerate}
\end{frame}

\begin{frame} \frametitle{Best Subset Selection}
	\begin{itemize}
		\item Want to find a subset of $p$ features
		\item The subset should be \underline{small} and predict \underline{well}
		\item Example: $\varname{credit} \sim \varname{rating} + \varname{income} + \varname{student} + \varname{limit}$
	\end{itemize}
	\vspace{5mm}

	\begin{algorithm}[H] \caption{Best Subset Selection}
		$\mathcal{M}_0 \leftarrow $ \emph{null model} (no features)\;
		\For{$k = 1,2,\ldots, p$}{
			Fit all ${p \choose k} $ models that contain $k$ features \;
			$\mathcal{M}_k \leftarrow $ best of ${p \choose k}$ models according to a metric (CV error, $R^2$, etc)
		}
		\Return Best of $\mathcal{M}_0, \mathcal{M}_1, \ldots, \mathcal{M}_p$ according to metric above
	\end{algorithm}

\end{frame}

\begin{frame}
  \frametitle{Best Subset Selection}
  \centering
  \includegraphics[width=\linewidth]{../islr2figs/Chapter6/6_1.pdf}
\end{frame}

\begin{frame} \frametitle{Achieving Scalability}
	\begin{itemize}
		\item<1-> Complexity of \emph{Best Subset Selection}?
		\item<2-> Examine all possible subsets? How many?
		\item<3-> $O(2^p)$!
		\vfill
		\item<4-> Heuristic approaches:
		\begin{enumerate}
			\item \textbf{Stepwise selection}: Solve the problem approximately: \underline{greedy}
			\item \textbf{Regularization}: Solve a different (easier) problem: \underline{relaxation}
		\end{enumerate}
	\end{itemize}
\end{frame}

\begin{frame} \frametitle{Forward Stepwise Selection}
	\begin{itemize}
		\item Greedy approximation of \emph{Best Subset Selection}
		\item Iteratively add more features
		\item Example: $\varname{credit} \sim \varname{rating} + \varname{income} + \varname{student} + \varname{limit}$
	\end{itemize}
	\vspace{5mm}
	\begin{algorithm}[H] \caption{Forward Stepwise Selection}
	$\mathcal{M}_0 \leftarrow $ \emph{null model} (no features) \;
	\For{$k =  \alert{0}, 1,2,\ldots, \alert{p-1}$}{
		Fit all $p - k$ models that augment $\mathcal{M}_k$ by one new feature \;
		$\mathcal{M}_{k+1} \leftarrow $ best of $p-k$ models according to a metric (CV error, $R^2$, etc)
	}
	\Return Best of $\mathcal{M}_0, \mathcal{M}_1, \ldots, \mathcal{M}_p$ according to metric above
	\end{algorithm}
	\begin{itemize}
		\item<2-> Complexity? \visible<3>{$O(p^2)$}
	\end{itemize}
\end{frame}

\begin{frame} \frametitle{Backward Stepwise Selection}
	\begin{itemize}
		\item Greedy approximation of \emph{Best Subset Selection}
		\item Iteratively remove features
		\item Example: $\varname{credit} \sim \varname{rating} + \varname{income} + \varname{student} + \varname{limit}$
	\end{itemize}
	\vspace{5mm}
	\begin{algorithm}[H] \caption{Backward Stepwise Selection}
		$\mathcal{M}_p \leftarrow $ \emph{full model} (all features) \;
		\For{$k =  \alert{p, p-1, \ldots, 1}$}{
			Fit all $k$ models that remove one feature from $\mathcal{M}_k$  \;
			$\mathcal{M}_{k-1} \leftarrow $ best of $k$ models according to a metric (CV error, $R^2$, etc)
		}
		\Return Best of $\mathcal{M}_0, \mathcal{M}_1, \ldots, \mathcal{M}_p$ according to metric above
	\end{algorithm}
	\begin{itemize}
		\item<2-> Complexity? \visible<3>{$O(p^2)$}
	\end{itemize}
\end{frame}

\begin{frame} \frametitle{Which Metric to Use?}
		\begin{algorithm}[H] \caption{Best Subset Selection}
		$\mathcal{M}_0 \leftarrow $ \emph{null model} (no features)\;
		\For{$k = 1,2,\ldots, p$}{
			Fit all ${p \choose k} $ models that contain $k$ features \;
			$\mathcal{M}_k \leftarrow $ best of ${p \choose k}$ models according to a \alert{metric} (CV error, $R^2$, etc)
		}
		\Return Best of $\mathcal{M}_0, \mathcal{M}_1, \ldots, \mathcal{M}_p$ according to \alert{metric} above
	\end{algorithm}
	\begin{enumerate}
		\item<2-> \textbf{Direct error estimate}: Cross validation, precise but computationally intensive
		\item<3-> \textbf{Indirect error estimate}: Mellow's $C_p$:
		\[ C_p = \frac{1}{n} (\operatorname{RSS} + 2 d \hat{\sigma}^2) \text{ where } \hat{\sigma}^2 \approx \var[\epsilon] \]
		Akaike information criterion, BIC, and many others. %Theoretical foundations
%		\item<4-> \textbf{Interpretability Penalty}: What is the cost of extra features
	\end{enumerate}
\end{frame}

\begin{frame} \frametitle{Regularization}
	\begin{enumerate}
		\item \textcolor{gray}{\textbf{Stepwise selection}: Solve the problem approximately}
		\vfill
		\item \textbf{Regularization}: Solve a different (easier) problem: \underline{relaxation}
		\begin{itemize}
			\item Solve with all features, but penalize solutions that use ``too much/many'' of the features
		\end{itemize}

	\end{enumerate}
\end{frame}

\begin{frame} \frametitle{Recall: Linear Regression}
	\begin{itemize}
		\item With one feature:
		\[ Y \approx \beta_0 + \beta_1 X \qquad Y = \beta_0 + \beta_1 X + \epsilon \]
		\item Prediction:
		\[ \hat{y}_i = \hat\beta_0 + \hat{\beta_1} x_i\]
		\item Errors ($y_i$ are true values):
		\[ e_i = y_i - \hat{y}_i  \]
		\begin{center}\includegraphics[width=0.7\linewidth]{{../islrfigs/Chapter3/3.1}.pdf}\end{center}
	\end{itemize}
\end{frame}

\begin{frame} \frametitle{Recall: Solving Linear Regression}
	\begin{itemize}
		\item Errors ($y_i$ are true values):
		\[ e_i = y_i - \hat{y}_i  \]
		\item Residual Sum of Squares
		\[ \operatorname{RSS} = e_1^2 + e_2^2 + e_3^2 + \cdots + e_n^2  = \sum_{i=1}^n e_i^2\]
		\item Equivalently:
		\[ \operatorname{RSS} = \sum_{i=1}^n ( y_i - \hat{\beta}_0 - \hat\beta_1 x_i )^2 \]
		\item \textbf{Minimize RSS} (for $p$ features, $x_{ij}$: $i$th sample, $j$th feature)
		\[ \min_{\beta} \operatorname{RSS}(\beta)  = \min_{\beta} \sum_{i=1}^n \left( y_i - \beta_0 - \sum_{j=1}^p \beta_j x_{ij} \right)^2 \]
	\end{itemize}
\end{frame}

\begin{frame} \frametitle{Regularization}
	\begin{itemize}
		\item \textbf{Ridge regression} (parameter $\lambda$), $\ell_2$ penalty
		\begin{gather*} \min_{\beta} \operatorname{RSS}(\beta) + \lambda \sum_j \beta_j^2  =\\
		 \min_{\beta} \sum_{i=1}^n \left( y_i - \beta_0 - \sum_{j=1}^p \beta_j x_{ij} \right)^2  + \lambda \sum_j \beta_j^2
		 \end{gather*}
		\item \textbf{Lasso} (parameter $\lambda$), $\ell_1$ penalty
		\begin{gather*} \min_{\beta} \operatorname{RSS}(\beta) + \lambda \sum_j |\beta_j| = \\
		\min_{\beta} \sum_{i=1}^n \left( y_i - \beta_0 - \sum_{j=1}^p \beta_j x_{ij} \right)^2 + \lambda \sum_j |\beta_j|
		\end{gather*}
		\item Approximations to the $\ell_0$ solution
	\end{itemize}
\end{frame}

\begin{frame} \frametitle{Ridge Regression: Coefficient Values}
	\begin{center}\includegraphics[width=\linewidth]{{../islrfigs/Chapter6/6.4}.pdf}\end{center}
\end{frame}

\begin{frame} \frametitle{Why Ridge Regression Works}
	\begin{itemize}
		\item Bias-variance trade-off
		\item Increasing $\lambda$ increases bias
	\end{itemize}
	\begin{center}\includegraphics[width=\linewidth]{{../islrfigs/Chapter6/6.5}.pdf}\\
	\textcolor{purple}{purple: test MSE}, black: bias, \textcolor{green}{green: variance}\end{center}
\end{frame}

\begin{frame} \frametitle{Regularization}
	\begin{itemize}
		\item \textbf{Ridge regression} (parameter $\lambda$), $\ell_2$ penalty
		\begin{gather*} \min_{\beta} \operatorname{RSS}(\beta) + \lambda \sum_j \beta_j^2  =\\
		\min_{\beta} \sum_{i=1}^n \left( y_i - \beta_0 - \sum_{j=1}^p \beta_j x_{ij} \right)^2  + \lambda \sum_j \beta_j^2
		\end{gather*}
		\item \textbf{Lasso} (parameter $\lambda$), $\ell_1$ penalty
		\begin{gather*} \min_{\beta} \operatorname{RSS}(\beta) + \lambda \sum_j |\beta_j| = \\
		\min_{\beta} \sum_{i=1}^n \left( y_i - \beta_0 - \sum_{j=1}^p \beta_j x_{ij} \right)^2 + \lambda \sum_j |\beta_j|
		\end{gather*}
		\item Approximations to the $\ell_0$ solution
	\end{itemize}
\end{frame}

\begin{frame} \frametitle{Lasso: Coefficient Values}
	\begin{center}\includegraphics[width=\linewidth]{{../islrfigs/Chapter6/6.6}.pdf}\end{center}
\end{frame}

\begin{frame} \frametitle{Why Lasso Works}
	\begin{itemize}
		\item Bias-variance trade-off
		\item Increasing $\lambda$ increases bias
		\item Example: all features relevant
	\end{itemize}
	\begin{center}\includegraphics[width=\linewidth]{{../islrfigs/Chapter6/6.8}.pdf}\\
		\textcolor{purple}{purple: test MSE}, black: bias, \textcolor{green}{green: variance} \\
		dotted (ridge) \end{center}
\end{frame}

\begin{frame} \frametitle{Why Lasso Works}
	\begin{itemize}
		\item Bias-variance trade-off
		\item Increasing $\lambda$ increases bias
		\item Example: some features relevant
	\end{itemize}
	\begin{center}\includegraphics[width=\linewidth]{{../islrfigs/Chapter6/6.9}.pdf}\\
		\textcolor{purple}{purple: test MSE}, black: bias, \textcolor{green}{green: variance} \\
		dotted (ridge) \end{center}
\end{frame}

\begin{frame} \frametitle{Regularization}
	\begin{itemize}
		\item \textbf{Ridge regression} (parameter $\lambda$), $\ell_2$ penalty
		\begin{gather*} \min_{\beta} \operatorname{RSS}(\beta) + \lambda \sum_j \beta_j^2  =\\
		\min_{\beta} \sum_{i=1}^n \left( y_i - \beta_0 - \sum_{j=1}^p \beta_j x_{ij} \right)^2  + \lambda \sum_j \beta_j^2
		\end{gather*}
		\item \textbf{Lasso} (parameter $\lambda$), $\ell_1$ penalty
		\begin{gather*} \min_{\beta} \operatorname{RSS}(\beta) + \lambda \sum_j |\beta_j| = \\
		\min_{\beta} \sum_{i=1}^n \left( y_i - \beta_0 - \sum_{j=1}^p \beta_j x_{ij} \right)^2 + \lambda \sum_j |\beta_j|
		\end{gather*}
		\item Approximations to the $\ell_0$ solution
	\end{itemize}
\end{frame}

\begin{frame} \frametitle{Regularization: Constrained Formulation}
	\begin{itemize}
		\item \textbf{Ridge regression} (parameter $\lambda$), $\ell_2$ penalty
		\begin{gather*}
		\min_{\beta} \sum_{i=1}^n \left( y_i - \beta_0 - \sum_{j=1}^p \beta_j x_{ij} \right)^2  \text{ subject to } \sum_j \beta_j^2 \le s
		\end{gather*}
		\item \textbf{Lasso} (parameter $\lambda$), $\ell_1$ penalty
		\begin{gather*}
		\min_{\beta} \sum_{i=1}^n \left( y_i - \beta_0 - \sum_{j=1}^p \beta_j x_{ij} \right)^2 \text{ subject to }  \sum_j |\beta_j| \le s
		\end{gather*}
		\item Approximations to the $\ell_0$ solution
	\end{itemize}
\end{frame}

\begin{frame} \frametitle{Lasso Solutions are Sparse}
	Constrained Lasso (left) vs Constrained Ridge Regression (right)
	\begin{center}\includegraphics[width=\linewidth]{{../islrfigs/Chapter6/6.7}.pdf}\end{center}
	Constraints are blue, red are contours of the objective
\end{frame}

\begin{frame}\frametitle{How to Choose $\lambda$?}
	\begin{itemize}
		\item<2-> Cross-validation
	\end{itemize}
	\visible<2>{
	\begin{center}\includegraphics[width=\linewidth]{{../islrfigs/Chapter6/6.12}.pdf}\end{center}}
\end{frame}

\begin{frame} \frametitle{Standardizing/Normalizing Features}
	\begin{itemize}
		\item Regularization (and PCR and others) depend on scales of features
		\item \underline{Often} good to \emph{standardize} features to have \textbf{same variance}
		\[ \tilde{x}_{ij} = \frac{x_{ij}}{\sqrt{\frac{1}{n}\sum_{i=1}^n (x_{ij} - \bar{x}_j)^2}} \]
		\item Do not standardize features when they have the same units
		\item Which other methods are sensitive to feature normalization?
	\end{itemize}

\end{frame}

\end{document}
%%% Local Variables:
%%% mode: latex
%%% TeX-master: t
%%% End:
