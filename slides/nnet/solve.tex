\documentclass{beamer}


\let\val\undefined
\usepackage{pgf}
\usepackage{pgfplots}
\usepackage{tikz}
\usepackage{booktabs}
\usepackage{natbib}
\usepackage{algorithm2e}
\usepackage{siunitx}
\usepackage{framed}
\usepackage{longtable}
\usepackage{amsmath}
\usepackage{amsthm}

\usetheme[progressbar=frametitle]{metropolis}
\usepackage{libertine}

\usetikzlibrary{arrows,automata,backgrounds,positioning,decorations,intersections,matrix}

% *** Styles ***
\setbeamertemplate{navigation symbols}{}
\usecolortheme{dolphin}
%\usecolortheme{rose}
%\setbeamercovered{transparent}
\usefonttheme{professionalfonts}
%\usefonttheme[onlymath]{serif}

% *** Colors ***
\newcommand{\tc}[2]{\textcolor{#1}{#2}}
\newcommand{\tcb}[1]{\tc{blue}{#1}}
\newcommand{\tcr}[1]{\tc{red}{#1}}
\newcommand{\tcg}[1]{\tc{green}{#1}}
\newcommand{\tr}{^{\top}}

\def\checkmark{\tikz\fill[scale=0.4](0,.35) -- (.25,0) -- (1,.7) -- (.25,.15) -- cycle;}

\newcommand{\Ex}{\mathbb{E}}
\newcommand{\var}{\operatorname{Var}}
%\newcommand{\Pr}{\mathbb{P}}

\definecolor{varcolor}{RGB}{132,23,49}
\newcommand{\varname}[1]{\textcolor{varcolor}{\mathsf{#1}}}

\title{Optimization for Machine Learning: Logistic Regression and Neural Nets}

\author{Marek Petrik}
\date{Apr 13, 2022}

\begin{document}

\begin{frame} \maketitle \end{frame}

\begin{frame} \frametitle{Artificial Neuron (Unit, Perceptron) }
  \begin{itemize}
  \item \textbf{Inputs} (features): $X_1, \dots, X_p$ 
  \item \textbf{Output} (target): $Y$
  \item \textbf{Parameters} (weights): $w_0, w_1, \dots , w_p$, or vector $w$
    \item \textbf{Activation function}: $g \colon \mathbb{R} \to \mathbb{R} $ a nonlinear function
  \end{itemize}
\vfill 
  Compute output as:
  \[
    Y = g \left( w_0 + \sum_{j = 1}^{p} w_j\cdot X_j \right) = g \left( w_0 + w\tr X \right)
  \]
\end{frame}
\begin{frame} \frametitle{Convolutional Neural Nets: Main Idea}
  Image composed from local features and image is location invariant
  \begin{center}
   \includegraphics[width=\linewidth]{../islr2figs/Chapter10/10_6.jpg} 
  \end{center}
\end{frame}
\begin{frame} \frametitle{Bag of Words Model}
  Represent each document as a boolean vector over most common words
  \[
   d = (d_1, d_2, \dots , d_{10000}) 
  \]
  For example:
  \begin{align*}
   d_1 &= 1 \text{  iff ``audience'' in document} \\
   d_2 &= 1 \text{  iff ``document'' in document} \\
        &\vdots
\end{align*}
\end{frame}

\begin{frame} \frametitle{Sequential Models: Recurrent Neural Nets}
  \begin{center}
   \includegraphics[width=\linewidth]{../islr2figs/Chapter10/10_12.pdf} 
 \end{center}
 More advanced model: LSTM
\end{frame}


\begin{frame} \frametitle{Overfitting and Double Descent: Splines}
  MNIST: 60,000 data points, NNET: About 300,000 parameters
  \begin{center}
   \includegraphics[width=\linewidth]{../islr2figs/Chapter10/10_20.pdf} 
 \end{center}
\end{frame}

\begin{frame}{Today}
\begin{enumerate}
\item Machine Learning as Optimization
\item Gradient Descent
\item Newton's Method
\item Stochastic Gradient Descent
\item Computational tools
\end{enumerate}
\end{frame}

\begin{frame}{Machine Learning as Optimization}
	\textbf{Philosophy}: Formalize the goal before deciding on the solution

	\begin{itemize}
		\item \textbf{Decision variables}: vector $x$ = what can we change?
		\item \textbf{Objective function}: $f(x)$ = what is the goal?
		\item \textbf{Constraints}:  what kinds of decisions are acceptable?
	\end{itemize}
	\pause
	Machine learning examples of:
	\begin{enumerate}
		\item decision variables
		\item objective function
		\item constraints
	\end{enumerate}
\end{frame}

\begin{frame}{Log-Likelihoods}
\begin{itemize}
	\item Linear regression (Gaussian noise with $\sigma^2 = 1$)
	\[ \ell(\beta) = -\frac{1}{2} \sum_{i=1}^n \left(  y_i - \hat{y}(x_i; \beta)\right)^2 + O(1)  \]
	\item Logistic regression, $p(x_i;\beta) =$ prediction prob. (see ESL 4.4.1)
	\begin{align*}
		\ell(\beta) &= \sum_{i=1}^{n} \Bigl( y_i \log p(x_i; \beta) + (1-y_i) \log (1 - p(x_i; \beta)) \Bigr) = \\
		&= \sum_{i=1}^{n} \left( y_i \sum_{j=1}^k \beta^j x_i^j + (1-y_i) \log \left(1 + e^{\sum_{j=1}^k \beta^j x_i^j} \right) \right)
	\end{align*}
\end{itemize}
\end{frame}


\begin{frame} \frametitle{Minimizing Objective Function}
  \begin{center}
   \includegraphics[width=\linewidth]{../islr2figs/Chapter10/10_17.pdf} 
  \end{center}
\end{frame}

\begin{frame}{Approximate Objective Function}
	\begin{itemize}
		\item \textbf{First-order methods}: Gradient descent, SGD, Nesterov's acceleration, \ldots
		\[ f(x) \approx f(a) + \nabla f(a)\tr  (x-a) \]
		\item \textbf{Second-order methods}: Newton's method, Quasi-Newton, BFGS, \ldots
		\[ f(x) \approx f(a) + \nabla f(a)\tr  (x-a) + \frac{1}{2} (x-a)\tr \underbrace{\nabla^2 f(a)}_{\text{Hessian}} (x-a) \]
	\end{itemize}
\end{frame}


\begin{frame} \frametitle{Descending Linear Function}

Consider a function (for $a = 0$):
\[
  \nabla f(x)\tr (x - a)  = 2 \cdot x_1 + 6 \cdot x_2
\]
What is the \emph{steepest} descent direction? (Hint: gradient) \\
\vfill

\pause
Descent direction $\Delta x$:
\[
  \nabla f(x)\tr \Delta x < 0
\]
\end{frame}

\begin{frame}{Gradient Descent}
\begin{center}
	\begin{enumerate}
		\item Start with some initial point $x_0$
		\item Initialize counter: $i = 0$
		\item Compute direction: $\Delta x = -\nabla f(x_i)$
		\item Compute step size: $t > 0$ (backtracking line search or diminishing like $t = \frac{1}{i + 1}$)
		\item Update solution: $x_{i+1} = x_i + t \cdot \Delta x$
		\item Let $i = i + 1$ and \underline{goto} 3
	\end{enumerate}
\end{center}
Any descent direction can be used!
\end{frame}

\begin{frame} \frametitle{Is Steepest Direction Best?}
  Consider a function:
  \[
   f(x) = 0.01 \cdot x_1^2 + 1000 \cdot x_2^2
  \]
  How will gradient descent behave?
  \pause
  Newton's method uses this update:
  \[
     x_{i+1} = x_i - t \cdot (\nabla^2 f(x_i))^{-1} \nabla f(x_i)
   \]
   Which is a descent direction, but converges much faster.
\end{frame}

\begin{frame}{Computational Tools}
 See R notebook
% \begin{center}
% \includegraphics[width=\linewidth]{../figs/class6_5/cannon.jpg}
% \end{center}
\end{frame}


\begin{frame} \frametitle{Backpropagation}
  Neuron is a function that computes:
  \[
    Y = g \left( w_0 + \sum_{j = 1}^{p} w_j\cdot X_j \right) = g \left( w_0 + w\tr X \right)
  \]
  Gradient descent: Compute gradient
  \[
   \nabla_w Y
  \]
  Propagate the gradient / derivative through layers using \textbf{chain rule}
  \pause
  \vfill
  Tools like TensorFlow, PyTorch, automate the computation of gradients
\end{frame}

\begin{frame} \frametitle{Stochastic Gradient Descent}
  Long time to compute gradient when $n$ is large:
  \[ \nabla_{\beta} \ell(\beta) = -\frac{1}{2} \sum_{i=1}^n \nabla_{\beta} \left(  y_i - \hat{y}(x_i; \beta)\right)^2  \]
  \textbf{Main idea}: Approximate gradient by taking a smaller subset of samples (\emph{minibatch})
  \vfill
  Sampling errors cancel out over time + many other improvement (momentum and others)
\end{frame}

\begin{frame} \frametitle{Regularization and Validation Sets}
  \begin{center}
   \includegraphics[width=\linewidth]{../islr2figs/Chapter10/10_18.pdf} 
  \end{center}
\end{frame}

\end{document}
%%% Local Variables:
%%% mode: latex
%%% TeX-master: t
%%% End:
