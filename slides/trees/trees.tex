\documentclass{beamer}

\usepackage{booktabs}
\usepackage{natbib}
\usepackage{algorithm2e}
\usepackage{siunitx}
\usepackage{framed}
\usepackage{longtable}
\usepackage{amsmath}
\usepackage{amsthm}
\usepackage{grffile}

\usetheme[progressbar=frametitle]{metropolis}
\usepackage{libertine}

% *** Styles ***
\setbeamertemplate{navigation symbols}{}
\usecolortheme{dolphin}
%\usecolortheme{rose}
%\setbeamercovered{transparent}
\usefonttheme{professionalfonts}
%\usefonttheme[onlymath]{serif}

% *** Colors ***
\newcommand{\tc}[2]{\textcolor{#1}{#2}}
\newcommand{\tcb}[1]{\tc{blue}{#1}}
\newcommand{\tcr}[1]{\tc{red}{#1}}
\newcommand{\tcg}[1]{\tc{green}{#1}}

\newcommand{\Ex}{\mathbb{E}}
\newcommand{\var}{\operatorname{Var}}
%\newcommand{\Pr}{\mathbb{P}}

\definecolor{varcolor}{RGB}{132,23,49}
\newcommand{\varname}[1]{\textcolor{varcolor}{\mathsf{#1}}}

\title{Decision Trees}
\subtitle{Boosting and bagging}
\author{Marek Petrik}
\date{Mar 23rd, 2022}


\begin{document}

\begin{frame} \maketitle
\end{frame}

\begin{frame} \frametitle{Regression Methods}

	\begin{itemize}
		\item Covered 4+ classification methods
		\item<2-> Regression methods (4+)?
		\item<3-> Which ones are generative/discriminative?
	\end{itemize}

\end{frame}

\begin{frame} \frametitle{Regression Trees}
	\begin{itemize}
		\item Predict Baseball $\varname{Salary}$ based on $\varname{Years}$ played and $\varname{Hits}$
		\item Example:
		\begin{center}\includegraphics[width=0.5\linewidth]{{../islrfigs/Chapter8/8.1}.pdf}\end{center}
	\end{itemize}
\end{frame}

\begin{frame} \frametitle{Tree Partition Space}
	\begin{center}
		\begin{center}
			\includegraphics[width=0.4\linewidth]{{../islrfigs/Chapter8/8.1}.pdf} \includegraphics[width=0.5\linewidth]{{../islrfigs/Chapter8/8.2}.pdf}
		\end{center}
	\end{center}
\end{frame}

\begin{frame} \frametitle{Advantages/Disadvantages of Decision Trees}
	\begin{itemize}
		\item<2-> Advantages:
		\begin{itemize}
			\item Interpretability
			\item Non-linearity
			\item Little data preparation, scale invariance
			\item Works with qualitative and quantitative features
		\end{itemize}
		\vfill
		\item<3-> Disadvantages:
		\begin{itemize}
			\item Hard to encode prior knowledge
			\item Difficult to fit
			\item Limited generalization
		\end{itemize}
	\end{itemize}
\end{frame}

\begin{frame} \frametitle{Decision Tree Terminology}
	\begin{itemize}
		\item Internal nodes
		\item Branches
		\item Leaves
	\end{itemize}
	\begin{center}\includegraphics[width=0.5\linewidth]{{../islrfigs/Chapter8/8.1}.pdf}\end{center}
\end{frame}

\begin{frame} \frametitle{Types of Decision Trees}
	\begin{itemize}
		\item Regression trees
		\vfill
		\item Classification tree
	\end{itemize}
\end{frame}

\begin{frame} \frametitle{Learning a Decision Tree}
	\begin{itemize}
		\item NP Hard problem
		\vfill
		\item<2-> Approximate algorithms (heuristics):
		\begin{itemize}
			\item ID3, C4.5, C5.0 (classification)
			\item CART (classification and regression trees)
			\item MARS (regression trees)
			\item \ldots
		\end{itemize}
	\end{itemize}
\end{frame}


\begin{frame} \frametitle{CART: Learning Regression Trees}
	Two basic steps:
	\begin{enumerate}
		\item Divide predictor space into regions $R_1, \ldots, R_J$
		\vfill
		\item Make the same prediction for all data points that fall in $R_j$
	\end{enumerate}
\end{frame}

\begin{frame} \frametitle{CART: Recursive Binary Splitting}
	\begin{itemize}
		\item Greedy top-to-bottom approach
		\vfill
		\item Recursively divide regions to minimize RSS
		\[\sum_{x_i\in R_1} (y_i - \bar{y}_1)^2 + \sum_{x_i\in R_2} (y_i - \bar{y}_2)^2 \]
	\end{itemize}
\end{frame}

\begin{frame} \frametitle{CART: Splitting Example}
	\begin{center}\includegraphics[width=0.7\linewidth]{{../islrfigs/Chapter8/8.3}.pdf}\end{center}
\end{frame}

\begin{frame} \frametitle{Tree Pruning}
	\begin{itemize}
		\item Bias-variance trade-off with regression trees?
		\item May overfit with many leaves.
		\item Better to build a large tree and then prune it (regularization parameter $\alpha$)
		\item Choose a subtree $T$ with $|T|$ leaves, predicting
		\[ \underbrace{\sum_{m=1}^{|T|} \sum_{x_i\in R_m} (y_i - \hat{y}_{R_m})^2}_\text{Classification error} + \overbrace{\alpha |T|}^{\text{Regularization penalty}} \]
		\begin{itemize}
			\item Remove subtrees for which regularization penalty is not offset by the reduction in classification error
		\end{itemize}
		\item Choose best $\alpha$ by cross validation
		\item Why is it better to prune than to stop early?
	\end{itemize}
\end{frame}

\begin{frame} \frametitle{Pruning Example}
	\begin{center}
		\includegraphics[width=0.45\linewidth]{{../islrfigs/Chapter8/8.1}.pdf}
		\hspace{5mm}
		\includegraphics[width=0.45\linewidth]{{../islrfigs/Chapter8/8.4}.pdf}
	\end{center}
\end{frame}

\begin{frame} \frametitle{Impact of Pruning}
\begin{center}\includegraphics[width=\linewidth]{{../islrfigs/Chapter8/8.5}.pdf}\end{center}
\end{frame}

%library(ggplot2)
%x = seq(0.01,0.99,length.out = 100)
%y1 = 1 - pmax(x,1-x)
%y2 = x *(1-x) * 2
%y3 = -x * log(x) - (1-x)*log(1-x)
%data <- data.frame(x=x,class=y1,gini=y2,entropy=y3)
%data2 <- tidyr::pivot_longer(data, c(-x), names_to = "method", values_to = "value")
%ggplot(data2, aes(x=x, y = value, color = method)) + geom_line() + theme_light() + labs(x=TeX("\\hat{p}_{m1}"), title="2-class Metrics")

\begin{frame} \frametitle{Classification Trees: Metrics}
	\begin{itemize}
		\item $\hat{p}_{mk}$ is proportion of observations in $R_m$ in class $k$
		\item Partition to \emph{minimize} a metric:
		\begin{enumerate}
			\item Classification error rate (of the best class)
			\[ 1 - \max_k \hat{p}_{mk} = \min_k (1 - \hat{p}_{mk} ) \]
			Often too pessimistic in practice
			\item Gini (impurity) index (CART):
			\[ \sum_{k=1}^K \hat{p}_{mk} (1-\hat{p}_{m_k}) \]
			\item Cross-entropy (information gain) (ID3, C4.5):
			\[ - \sum_{k=1}^K \hat{p}_{mk} \log \hat{p}_{m_k} \]
		\end{enumerate}
		\item ID3, C4.5 do not prune
	\end{itemize}
\end{frame}

\begin{frame}{Comparison of Metrics}
	$\hat{p}_{mk}$ is proportion of observations in $R_m$ in class $k$
	\begin{enumerate}
		\item Classification error:
		$1 - \max_k \hat{p}_{mk} = \min_k (1 - \hat{p}_{mk} )$
		\item Gini:
		$\sum_{k=1}^K \hat{p}_{mk} (1-\hat{p}_{m_k})$
		\item Cross-entropy:
		$ - \sum_{k=1}^K \hat{p}_{mk} \log \hat{p}_{m_k} $
	\end{enumerate}
	\begin{center}
		\includegraphics[width=0.8\linewidth]{../figs/class13/metrics_plot.pdf}
	\end{center}
\end{frame}

\begin{frame} \frametitle{Why Not Use Classification Error?}
Decision tree with classification error
\begin{center}\includegraphics[width=\linewidth]{{../figs/class13/classerror}.png}\end{center}
\tiny{Source: \url{https://sebastianraschka.com/faq/docs/decisiontree-error-vs-entropy.html}}
\end{frame}

\begin{frame} \frametitle{Why Not Use Classification Error?}
Decision tree with information gain
\begin{center}\includegraphics[width=\linewidth]{{../figs/class13/entropy}.png}\end{center}
\tiny{Source: \url{https://sebastianraschka.com/faq/docs/decisiontree-error-vs-entropy.html}}
\end{frame}

\begin{frame} \frametitle{Why Not Use Classification Error?}
Entropy is more optimistic
\begin{center}\includegraphics[width=0.9\linewidth]{../figs/class13/entropy_plot.png}\end{center}
\tiny{Source: \url{https://sebastianraschka.com/faq/docs/decisiontree-error-vs-entropy.html}}
\end{frame}

\begin{frame} \frametitle{Pruning in Classification Trees}
\begin{center}\includegraphics[width=0.6\linewidth]{{../islrfigs/Chapter8/8.6}.pdf}\end{center}
\end{frame}

\begin{frame} \frametitle{Trees vs. Linear Models}
\begin{center}\includegraphics[width=0.6\linewidth]{{../islrfigs/Chapter8/8.7}.pdf}\end{center}
\end{frame}

\begin{frame} \frametitle{Trees vs. KNN}
\visible<2->{
\begin{itemize}
	\item Trees do not require a distance metric
	\item Trees work well with categorical predictors
	\item Trees work well in large dimensions
	\item KNN are better in low-dimensional problems with complex decision boundaries
\end{itemize}}
\end{frame}

\begin{frame} \frametitle{Bagging and Boosting}
	\begin{itemize}
		\item Methods for reducing variance of decision trees
		\item Make predictions using a \emph{weighted vote} of multiple trees
		\item Boosted trees are some of the most successful general machine learning methods (on Kaggle)
		\vfill
		\item<2-> Disadvantage of using votes of multiple trees?
	\end{itemize}
\end{frame}

\begin{frame} \frametitle{Bagging}
	\begin{itemize}
		\item Stands for ``Bootstrap Aggregating''
		\item Construct multiple bootstrapped training sets:
		\[ T_1, T_2, \ldots, T_B \]
		\item Fit a tree to each one:
		\[  \hat{f}_1, \hat{f}_2, \ldots , \hat{f}_B \]
		\item Make predictions by averaging individual tree predictions
		\[  \hat{f}(x) = \frac{1}{B} \sum_{b=1}^B \hat{f}_b(x)\]
		\item Large values of $B$ are not likely to overfit, $B \approx 100$ is a good choice
	\end{itemize}
\end{frame}

\begin{frame} \frametitle{Random Forests}
	\begin{itemize}
		\item Many trees in bagging will be similar
		\item Algorithms choose the same features to split on
		\item Random forests help to address similarity:
		\begin{itemize}
			\item At each split, choose only from $m$  randomly sampled features
		\end{itemize}
		\item Good empirical choice is $m = \sqrt{p}$
	\end{itemize}
	\begin{center}
		\vspace{-10mm}
		\includegraphics[width=0.6\linewidth]{{../islrfigs/Chapter8/8.10}.pdf}\end{center}
\end{frame}

\begin{frame} \frametitle{Cross-validation and Bagging}
	\begin{itemize}
		\item No need for cross-validation when bagging
		\item Evaluating trees on out-of bag samples is sufficient
	\end{itemize}
	\begin{center}
	\vspace{-3mm}
	\includegraphics[width=0.6\linewidth]{{../islrfigs/Chapter8/8.8}.pdf}\end{center}
\end{frame}

\begin{frame} \frametitle{Boosting (Gradient Boosting, AdaBoost)}
	What Kaggle has to say: \\
	\begin{center}
		\includegraphics[width=0.9\linewidth]{../figs/class13/kaggle_boosting.png} \\
		{\tiny source: \url{http://blog.kaggle.com/2017/01/23/a-kaggle-master-explains-gradient-boosting/}}
	\end{center}
\end{frame}

\begin{frame} \frametitle{Gradient Boosting (Regression)}
	\begin{itemize}[<+->]
		\item Boosting uses all of data, not a random subset (usually)
		\item Also builds trees $\hat{f}_1, \hat{f}_2, \ldots$
		\item \alert{and} weights $\lambda_1, \lambda_2, \ldots$
		\item Combined prediction:
		\[ \hat{f}(x)  = \sum_{i} \lambda_i \hat{f}_i(x) \]
		\item  Assume we have $1 \ldots m$ trees and weights, next best tree?
	\end{itemize}
\end{frame}

\begin{frame} \frametitle{Gradient Boosting (Regression)}
	\begin{itemize}[<+->]
		\item Just use \alert{gradient descent}
		\item \textbf{Objective} is to minimize RSS (1/2):
		\[ \frac{1}{2} \sum_{i=1}^n (y_i - f(x_i))^2 \]
		\item \textbf{Objective} with the new tree $m+1$:
		\[ \frac{1}{2} \sum_{i=1}^n \left(y_i - \sum_{j=1}^m \lambda_j \hat{f}_j(x_i) - \alert{\hat{f}_{m+1}(x_i)} \right)^2 \]
		\item Greatest reduction in RSS: \alert{gradient}
		\[ y_i - \sum_{j=1}^m \lambda_j \hat{f}_j(x_i) \approx \alert{\hat{f}_{m+1}(x_i)} \]
	\end{itemize}
\end{frame}

\begin{frame} \frametitle{Gradient Boosting}
	\begin{itemize}[<+->]
		\item Greatest reduction in RSS: \alert{gradient}
		\[ y_i - \sum_{j=1}^m \lambda_j \hat{f}_j(x_i) \approx \alert{\hat{f}_{m+1}(x_i)} \]
		\item \textbf{Fit new tree to the following target} (instead of $y_i$)
		\[ y_i - \sum_{j=1}^m \lambda_j \hat{f}_j(x_i) \]
		\item Compute the weight $\lambda_{m+1}$ by \textbf{line search}:
		\[ \min_{\tcb{\lambda_{m+1}}} \left(y_i - \sum_{j=1}^m \lambda_j \hat{f}_j(x_i) - \tcb{\lambda_{m+1}}\alert{\hat{f}_{m+1}(x_i)} \right)^2 \]
		\item And many other improvements
	\end{itemize}
\end{frame}

\begin{frame} \frametitle{XGBoost}
	\begin{itemize}
		\item Scalable and flexible gradient boosting
		\item Interfaces for many languages and environments
	\end{itemize}
\begin{center}
	\includegraphics[width=0.8\linewidth]{../figs/class13/xgboost.png}
\end{center}
\end{frame}

\end{document}
%%% Local Variables:
%%% mode: latex
%%% TeX-master: t
%%% End:
