\documentclass[10pt]{beamer}


\usepackage{pgf}
\usepackage{pgfplots}
\usepackage{tikz}
\usepackage{booktabs}
\usepackage{natbib}
\usepackage{algorithm2e}
\usepackage{siunitx}
\usepackage{framed}
\usepackage{longtable}
\usepackage{amsmath}
\usepackage{amsthm}

\usepackage{ulem}
\usepackage{cancel}
\usepackage{marvosym}
\usepackage{arydshln}
\usepackage{grffile}


\usepackage{libertine}

\usetikzlibrary{arrows,automata,backgrounds,positioning,decorations,intersections,matrix}

% *** Styles ***
\setbeamertemplate{navigation symbols}{}
\usecolortheme{dolphin}
%\usecolortheme{rose}
%\setbeamercovered{transparent}
\usefonttheme{professionalfonts}
%\usefonttheme[onlymath]{serif}

% *** Colors ***
\newcommand{\tc}[2]{\textcolor{#1}{#2}}
\newcommand{\tcb}[1]{\tc{blue}{#1}}
\newcommand{\tcr}[1]{\tc{red}{#1}}
\newcommand{\tcg}[1]{\tc{green}{#1}}

\def\checkmark{\tikz\fill[scale=0.4](0,.35) -- (.25,0) -- (1,.7) -- (.25,.15) -- cycle;}

\newcommand{\Ex}{\mathbb{E}}
\DeclareMathOperator{\var}{Var}
%\newcommand{\Pr}{\mathbb{P}}

\definecolor{varcolor}{RGB}{132,23,49}
\newcommand{\varname}[1]{\textcolor{varcolor}{\mathsf{#1}}}

\definecolor{midgreen}{rgb}{0,0.7,0}
\definecolor{purple}{rgb}{0.5, 0.0, 0.5}
\definecolor{bluegreen}{rgb}{0.0,0.5, 0.5}
\definecolor{orange}{rgb}{.8,0.33, 0}
\definecolor{redgreen}{rgb}{0.5, 0.5, 0.0}
\definecolor{lightgray}{rgb}{0.8,0.8,0.8}

\usetheme[progressbar=frametitle]{metropolis}
\usepackage{libertine}

\def\blue{\color{blue}}
\def\red{\color{red}}
\def\green{\color{midgreen}}
\def\purple{\color{purple}}
\def\bluegreen{\color{bluegreen}}
\def\orange{\color{orange}}
\def\redgreen{\color{redgreen}}
\def\black{\color{black}}
\def\gray{\color{gray}}
\def\ltgray{\color{lightgray}}

\newcommand\V{\mathbb{V}}
\newcommand\U{\mathbb{U}}
\newcommand\W{\mathbb{W}}
\newcommand\R{\mathbb{R}}
\newcommand\C{\mathbb{C}}
\newcommand\vv{\vec{v}}
\newcommand\vu{\vec{u}}
\newcommand\vw{\vec{w}}
\newcommand\vx{\vec{x}}
\newcommand\vy{\vec{y}}
\newcommand\vz{\vec{z}}
\newcommand\ve{\vec{e}}
\newcommand\vd{\vec{d}}
\newcommand\vc{\vec{c}}
\newcommand\va{\vec{a}}
\newcommand\vb{\vec{b}}
\newcommand\vzero{\vec{0}}
\newcommand\cB{\mathcal{B}}


\def\divideline{\line(1,0){352}}

\DeclareMathOperator{\Span}{Span}
\DeclareMathOperator{\Nul}{Nul}
\DeclareMathOperator{\Col}{Col}
\DeclareMathOperator{\Ker}{Ker}
\DeclareMathOperator{\Null}{Null}

\title{Linear Algebra}
\subtitle{Introduction}
\author{Marek Petrik, Adapted from Linear Algebra Lectures by Martin Scharlemann}
\date{Feb 2nd, 2022}


\begin{document}
\begin{frame} \maketitle
\end{frame}

\begin{frame} \frametitle{Review}
  \begin{enumerate}
  \item Probability space 
  \item Random variable
  \item Probability distributions
  \item Probability and conditional probability
  \item Bayes' theorem
  \item Expected value
  \end{enumerate}
\end{frame}

\begin{frame} \frametitle{Today: Linear Algebra}
	\textbf{Why?}
	\begin{enumerate}
		\item \textbf{Understand ML Methods}
		\vfill
	 	\item \textbf{Implement ML Methods}:
 		\vfill
	 	\item \textbf{Help Data Processing}
	\end{enumerate}
\end{frame}

\begin{frame} \frametitle{Suggested Linear Algebra Books}
	\begin{enumerate}
		\item Strang, G. (2016). Introduction to linear algebra (5th ed.) \url{http://math.mit.edu/~gs/linearalgebra/} \\
			\textbf{Watch online lectures}: \url{https://ocw.mit.edu/courses/mathematics/18-06-linear-algebra-spring-2010/video-lectures/}
		\vfill
		\item Hefferon, J. (2017). Linear algebra (3rd ed.).\\ \textbf{Free PDF}: \url{http://joshua.smcvt.edu/linearalgebra/}
	\end{enumerate}
\end{frame}

\begin{frame} \frametitle{Linear Equation}
	\parbox{2in}{Equation of a line:
		\includegraphics[scale=0.65]{../figs/class11/LineEqnA.pdf}
	}
	\pause
	\parbox{2in}{\centering
		$y = {\color{midgreen}{m}}x +{\color{midgreen}{b}}$ \\
		\pause
		$\Updownarrow$ \\
		$y - mx = b$ \\
		\vspace{1in}
	}
\end{frame}

 \begin{frame} \frametitle{Linear Equation}
	\parbox{2in}{Equation of a line:
		\includegraphics[scale=0.65]{../figs/class11/LineEqnB.pdf}
	}
	\parbox{2in}{\centering
		$y = {\color{midgreen}{m}}x +{\color{midgreen}{b}}$ \\
		$\Updownarrow$ \\
		$y - mx = b$
		\\
		$\Updownarrow$
		\\
		${\color{red}{x_2}} - m{\color{red}{x_1}} = b$
		\\
		\pause
		$\Updownarrow$
		\\
		$-mx_1 + x_2 = b$
		\\
		\pause
		$\Downarrow (\Uparrow a_2 \neq 0)$
		%\\
		%(multiply by constant)
		\\
		$a_1x_1 + a_2x_2 = b'$
	}
\end{frame}

\begin{frame} \frametitle{Linear Equation}
	{\color{blue}{\em Linear equation}} in {\color{red}{\em variables}} $x_1, ..., x_n$:  \[a_1x_1 + a_2x_2 + \ldots + a_nx_n = b\]
	where $a_1, ..., a_n$ and $b$ are known in advance.
	\bigskip
	\pause

	{\color{midgreen}{\em Solution}} is a list $s_1, ..., s_n$ of numbers so
	\[a_1s_1 + a_2s_2 + \ldots + a_ns_n = b\]
	\pause

	\alert{Example:}  For the equation
	\[\beta_1 x + (-1) y = -\beta_0\]
	of a line, for some $\beta_0, \beta_1$:
	\bigskip

	The pair $x, y$ is a solution $\iff$ the point $(x, y)$ is on the line.
\end{frame}

%\begin{frame} \frametitle{Example 2}
%	Converting grades to the standard scale: $F = 0 \leq grade \leq 4 = A$, let
%
%	\begin{itemize} \item $x_1$ be your first midterm grade
%		\item $x_2$ be your second midterm grade
%		\item $x_3$ be your grade on the final
%		\item $x_4$ be your homework \& quiz grade
%		\item $x_5$ be your i-clicker grade
%	\end{itemize}
%	\pause
%
%	Then
%
%	\includegraphics[width=12cm]{../figs/class11/grade_calc.pdf}
%
%	translates to
%	\[
%	0.2x_1 + 0.2x_2 + 0.4x_3 + 0.15x_4 + 0.05x_5 = your \; course \; grade
%	\]
%
%\end{frame}

\section{Systems of linear equations}

\begin{frame} \frametitle{System of Linear Equations}

	A {\color{blue}{\em system}} of linear equations is a bunch of linear equations:
	\pause
	\begin{align*} a_{11}x_1 + a_{12}x_2 + \ldots + a_{1n}x_n &= b_1 \\  a_{21}x_1 + a_{22}x_2 + \ldots + a_{2n}x_n &= b_2 \\ \vdots \\   a_{m1}x_1 + a_{m2}x_2 + \ldots + a_{mn}x_n &= b_m\end{align*}
	\pause
	A \alert{solution} to the system is a list $$s_1, ..., s_n \in \R$$ that is simultaneously a solution to  \alert{all $m$} equations.
	\pause

	That is,  \alert{all $m$} equations are true when $x_1 = s_1, x_2 = s_2, ..., x_n = s_n$.

\end{frame}

\begin{frame} \frametitle{Example}
	Conceptual example:
	\begin{align*} a_{11}x_1 + a_{12}x_2 &= b_1 \\  a_{21}x_1 + a_{22}x_2 &= b_2 \end{align*}
	\pause
	Solution $\Leftrightarrow$ two lines intersect:

	\parbox{2in}{
		\includegraphics[scale=0.65]{../figs/class11/LinePairA.pdf}
	}
	\pause
	\parbox{2in}{\centering
		Example:
		\begin{align*} 1x_1 + 2x_2 &= 3 \\  2x_1 + 1x_2 & =3 \\ \Leftrightarrow (x_1, x_2) &= (1, 1)\end{align*}
	}
\end{frame}

\begin{frame} \frametitle{Line Configurations}
	Other possibilities:

	\parbox{2in}{
		\includegraphics[scale=0.5]{../figs/class11/LinePairB.pdf}
	}
	\pause
	\parbox{2in}{\centering
		\begin{align*} 1x_1 + 2x_2 &= 3 \\  1x_1 + 2x_2 & =4 \end{align*} \\ inconsistent
		\pause
		\divideline
		\begin{align*} 1x_1 + 2x_2 &= 3 \\  2x_1 + 4x_2 & =6\end{align*} \\ redundant
	}
\end{frame}

\begin{frame} \frametitle{3 Possible Line Configurations}
	Upshot:  There are exactly three possibilities
	\pause
	\begin{enumerate}
		\item  There could be no solution
		\pause
		\item  There could be exactly one solution
		\pause
		\item There could be infinitely many solutions
	\end{enumerate}
	\pause
	\bigskip

	Some goals:
	\begin{itemize}
		\item Figure out which possibility applies.
		\pause
		\item Write down {\em the} solution if it's unique.
		\pause
		\item  if there are infinitely many solutions, figure out a way to describe them all.
	\end{itemize}
\end{frame}


\section{Solving a linear system}

\begin{frame} \frametitle{Matrix}
	The critical information is in the $a_{ij}, b_i$.
	\pause

	So extract just those numbers into a {\green {\em matrix}}

	\parbox{2in}{
		\begin{align*} a_{11}x_1 + \ldots + a_{1n}x_n &= b_1 \\  a_{21}x_1  + \ldots + a_{2n}x_n &= b_2 \\ \vdots \\   a_{m1}x_1  + \ldots + a_{mn}x_n &= b_m\end{align*}
	}
	\pause
	\parbox{2in}{\centering
		\[ \begin{bmatrix}
		a_{11} & a_{12} & \ldots & a_{1n}  \\   a_{21} & a_{22} & \ldots & a_{2n}  \\ \vdots & \vdots & \ddots & \vdots \\    a_{m1} & a_{m2} & \ldots & a_{mn}  \end{bmatrix} \]
		$m \times n$ coefficient matrix
		\pause
		\divideline
		\[ \left[\begin{array}{cccc:c}
		a_{11} & a_{12} & \ldots & a_{1n}  & b_1 \\   a_{21} & a_{22} & \ldots & a_{2n} & b_2 \\ \vdots & \vdots & \ddots & \vdots & \vdots \\    a_{m1} & a_{m2} & \ldots & a_{mn} & b_m \end{array}\right] \]
		$m \times (n+1)$ augmented matrix
	}
\end{frame}

\begin{frame} \frametitle{Solving Linear Systems: Gaussian Elimination}
	These don't change the set of solutions for a system of linear equations:

	\parbox{2in}{\emph{Equation operations}:
		\begin{itemize}
			\item Reorder the equations
			\pause
			\bigskip

			\item Multiply an equation by $c \neq 0$
			\pause
			\bigskip

			\item Replace one equation by itself plus a multiple of another equation
		\end{itemize}
	}
	\pause
	\parbox{2in}{\emph{Matrix operations}:
		\begin{itemize}
			\item Reorder the rows  (interchange)
			\pause
			\item Multiply a row by $c \neq 0$ (scaling)
			\pause
			\item Replace one row by itself plus a multiple of another row (replacement)
		\end{itemize}
	}
	\divideline

	Grand strategy:  Do this until the equations are easy to solve.
\end{frame}

%\section{Some examples}
%
%\begin{frame} \frametitle{Example}
%	\parbox{2in}{
%		\begin{align*} 1x_1 + 2x_2 &= 3 \\  2x_1 + 1x_2 & =3 \end{align*}
%		\pause
%		%\divideline
%		Subtract twice $1^{st}$ from $2^{nd}$:
%		\begin{align*} 1x_1 + 2x_2 &= 3 \\   -3x_2 & =-3 \end{align*}
%		\pause
%		Add $\frac23$ second to first:
%		\bigskip
%
%		$1x_1 = 1$ \\   $ -3x_2  =-3$
%		\medskip
%		\pause
%
%		Multiply second by $-\frac13$
%		\medskip
%
%		$x_1 = 1$ \\ $ x_2 = 1$
%	}
%	\pause
%	\parbox{2in}{
%		\medskip
%
%		\[ \left[\begin{array}{cc:c}
%		1 & 2 & 3 \\   2 & 1 & 3   \end{array}\right] \]
%		\medskip
%		\pause
%
%		\[ \left[\begin{array}{cc:c}
%		1 & 2 & 3 \\   0 & -3 & -3   \end{array}\right] \]
%		\medskip
%		\pause
%
%		\[ \left[\begin{array}{cc:c}
%		1 & 0 & 1 \\  0 & -3 & -3    \end{array}\right] \]
%		\medskip
%
%		\[ \left[\begin{array}{cc:c}
%		1 & 0 & 1 \\  0 & 1 & 1    \end{array}\right] \]
%	}
%\end{frame}

%\begin{frame}  \frametitle{Step one:}
%
%	Use first $x_1$ term [upper left matrix entry] to eliminate all other $x_1$ terms [change rest of first column to $0$]:
%
%	\parbox{2in}{
%		\begin{align*} L1: &  & x_1 - 3x_2 - 2x_3 & = & 6 \\  L2:  &  &2x_1 - 4x_2 - 3x_3  & = & 8 \\  L3: &  &-3x_1 + 6x_2 + 8x_3 &  = & -5 \end{align*}
%		\pause
%		Subtract $2 \times$ first from second \\
%		Add $3 \times$ first to third:
%		\begin{align*} L1: &  & x_1 - 3x_2 - 2x_3 & = & 6 \\  L2 - 2L1:  &  & 2x_2 + x_3  & = & -4 \\  3L1+L3: &  & -3x_2 + 2x_3 &  = & 13 \end{align*}
%	}
%	\pause
%	\parbox{1.8in}{
%		\[ \left[\begin{array}{ccc:c}
%		1 & -3 & -2 & 6 \\   2 & -4 & -3 & 8  \\   -3 & 6 & 8 & -5\end{array}\right] \]
%		\medskip
%		\pause
%
%		\[ \left[\begin{array}{ccc:c}
%		1 & -3 & -2 & 6 \\   0 & 2 & 1 & -4  \\   0 & -3 & 2 & 13\end{array}\right] \]
%	}
%	\medskip
%
%	Move on to {\blue Step two!} - the second column
%\end{frame}
%
%\begin{frame} \frametitle{Step two:}
%
%	\parbox{2in}{
%		\begin{align*} L1: &  & x_1 - 3x_2 - 2x_3 & = & 6 \\  L2:  &  & 2x_2 + x_3  & = & -4 \\  L3: &  & -3x_2 + 2x_3 &  = & 13\end{align*}
%		\pause
%		Add $\frac32 \times$ L2 to L1 and L3:
%		\begin{align*} L1 + \frac32 L2: &  & x_1 \quad \quad - \frac12x_3 & = & 0 \\  L2:  &  & 2x_2 + x_3  & = & -4 \\  L3 + \frac32 L2: &  & \frac72x_3 &  = & 7 \end{align*}
%	}
%	\pause
%	\parbox{1.8in}{
%		\[ \left[\begin{array}{ccc:c}
%		1 & -3 & -2 & 6 \\   0 & 2 & 1 & -4  \\   0 & -3 & 2 & 13\end{array}\right] \]
%
%		\[ \left[\begin{array}{ccc:c}
%		1 & 0 & -\frac12 & 0 \\   0 & 2 & 1 & -4  \\   0 & 0 & \frac72 & 7\end{array}\right] \]
%		\centering
%		Multiply L3 by $\frac27$
%		\[ \left[\begin{array}{ccc:c}
%		1 & 0 & -\frac12 & 0 \\   0 & 2 & 1 & -4  \\   0 & 0 & 1 & 2\end{array}\right] \]
%	}
%	\medskip
%
%	Move on to {\blue Step three!} - the third column
%\end{frame}
%
%\begin{frame} \frametitle{Step three:}
%
%	\parbox{2in}{
%		\begin{align*} L1: &  & x_1 \quad \quad - \frac12x_3 & = & 0 \\  L2:  &  & 2x_2 + x_3  & = & -4 \\  L3: &  & x_3 &  = & 2 \end{align*}
%		Subtract L3 from L2;
%
%		add $\frac12$ L3 to L1:
%		\begin{align*} L1 + \frac12 L3: &  & x_1 \quad \quad & = & 1 \\  L2 - \frac12 L3:  &  & 2x_2 \quad  & = & -6 \\  L3: &  & x_3 &  = & 2 \end{align*}
%	}
%	\pause
%	\parbox{1.8in}{
%		\[ \left[\begin{array}{ccc:c}
%		1 & 0 & -\frac12 & 0 \\   0 & 2 & 1 & -4  \\   0 & 0 & 1 & 2\end{array}\right] \]
%		\[ \left[\begin{array}{ccc:c}
%		1 & 0 & 0 & 1 \\   0 & 2 & 0 & -6  \\   0 & 0 & 1 & 2\end{array}\right] \]
%		\centering
%		Multiply L2 by $\frac12$
%		\[ \left[\begin{array}{ccc:c}
%		1 & 0 & 0 & 1 \\   0 & 1 & 0 & -3  \\   0 & 0 & 1 & 2\end{array}\right] \]
%	}
%	\medskip
%
%	We're done:  $(x_1, x_2, x_3) = (1, -3, 2)$
%\end{frame}
%
\section{Solution Possibilities}
\begin{frame}  \frametitle{What can go wrong? }

	\parbox{2in}{\centering
		\begin{align*} x_1 + 2x_2 &= 3 \\  x_1 + 2x_2 & =4 \end{align*}
		\pause
		Subtract L1 from L2:
		\begin{align*} x_1 + 2x_2 &= 3 \\ {\red 0} & {\red =1} \end{align*}
		\\ inconsistent

		NO SOLUTION
	}
	\pause
	\parbox{2in}{\centering\begin{align*} x_1 + 2x_2 &= 3 \\  2x_1 + 4x_2 & =6\end{align*}
		\pause
		Subtract 2L1 from L2:
		\begin{align*} x_1 + 2x_2 &= 3 \\  {\green 0} & {\green =0} \end{align*}
		\\ redundant

		SOLUTIONS INFINITE
	}

\end{frame}

\begin{frame} \frametitle{Configurations}

	If the system has \alert{$3$} variables, an equation determines a {\blue plane} in $\R^{\alert{3}}$.
	\pause

	So a solution will be the intersection of $3$ planes: $P_1, P_2, P_3$:
	\pause

	\begin{center}
		\includegraphics[width=6cm]{../figs/class11/inconsistent}

		inconsistent equations (no solution)
		\pause

		\includegraphics[width=3.2cm]{../figs/class11/solution}
		\hspace{1cm}
		\includegraphics[width=3.2cm]{../figs/class11/redundant}

		single solution  \hspace{1cm}   redundant

	\end{center}
\end{frame}

%\begin{frame}
%	\frametitle{Question?}
%	\large{
%		George tells you the system of equations
%		\begin{align*} 5x_1 + 2x_2 - 3x_3 &= 4 \\  12x_1 -7x_2 + 2x_3 &= 8 \\    -3x_1 + 4x_2 + 5x_3 &= 10\end{align*}
%		has exactly \alert{three solutions.}
%		\pause
%		\begin{enumerate}
%			\item[A)] George is probably right, since he's Honest George.
%			\pause
%			\item[B)] George is probably wrong.
%			\pause
%			\item[C)] George is definitely right.
%			\pause
%			\item[D)] George is definitely wrong.
%			\pause
%			\item[E)] My brain is full.
%		\end{enumerate}
%	}
%\end{frame}

\section{Vectors}

\begin{frame} \frametitle{Vector}
	An $m$-\alert{vector} [column vector, vector in $\R^m$] is an $m \times 1$ matrix:

	\[
	\vec{a} =\begin{bmatrix}
	a_1  \\   a_2  \\ a_3 \\ \vdots \\    a_m \end{bmatrix}
	\]
	\pause
	Can add two $m$-vectors in the obvious way, or multiply a vector by a real number:
	\[
	\begin{bmatrix} a_1  \\   a_2  \\ a_3 \\ \vdots \\    a_m \end{bmatrix} +
	\begin{bmatrix} b_1  \\   b_2  \\ b_3 \\ \vdots \\    b_m \end{bmatrix} =
	\begin{bmatrix} a_1 +b_1  \\   a_1 +b_2  \\ a_1 +b_3 \\ \vdots \\    a_1 +b_m \end{bmatrix} ;
	\pause
	\hspace{1cm}
	c \begin{bmatrix} a_1  \\   a_2  \\ a_3 \\ \vdots \\    a_m \end{bmatrix} =
	\begin{bmatrix} ca_1  \\   ca_2  \\ ca_3 \\ \vdots \\    ca_m \end{bmatrix}
	\]
	\alert{Do not} multiply two vectors together like this.
\end{frame}

\begin{frame} \frametitle{Vector Operations}
	Examples:
	\[
	\begin{bmatrix} 1  \\   2  \\ 3 \end{bmatrix} +
	\begin{bmatrix} 4  \\   5 \\ 6 \end{bmatrix} =
	\begin{bmatrix} 5  \\   7  \\ 9 \end{bmatrix} ;
	\pause
	\hspace{1cm}
	-1 \cdot \begin{bmatrix} 1  \\   2  \\ 3  \end{bmatrix} =
	\begin{bmatrix} -1  \\   -2  \\ -3  \end{bmatrix}
	\]
	\pause
	\divideline
	\[
	3\cdot \begin{bmatrix} 1  \\   2  \\ 3 \end{bmatrix} -\;
	7\cdot  \begin{bmatrix} 0  \\   2 \\ 4 \end{bmatrix} +\;
	2 \cdot \begin{bmatrix} 1  \\   -1 \\ 0 \end{bmatrix} = \] \pause
	\[
	\begin{bmatrix} 3\cdot 1 - 7\cdot 0 + 2\cdot 1  \\   3\cdot 2 - 7\cdot 2 + 2\cdot (-1)  \\
	3\cdot 3 - 7\cdot 4 + 2\cdot 0 \end{bmatrix} =  \pause
	\begin{bmatrix} 5  \\   -10  \\  -19  \end{bmatrix} \]

\end{frame}

\begin{frame}	\frametitle{Question: }
	\[
	7\cdot \begin{bmatrix} 1  \\   2  \\ 3 \end{bmatrix}
	-\;
	3\cdot  \begin{bmatrix} 0  \\   2 \\ 4 \end{bmatrix}
	-\;
	2 \cdot \begin{bmatrix} 1  \\   -1 \\ 0 \end{bmatrix} = \] \pause
	%\[
	%\begin{bmatrix} 3\cdot 1 - 7\cdot 0 + 2\cdot 1  \\   3\cdot 2 - 7\cdot 2 + 2\cdot (-1)  \\
	%3\cdot 3 - 7\cdot 4 + 2\cdot 0 \end{bmatrix} =  \pause
	%\begin{bmatrix} 5  \\   -10  \\  -19  \end{bmatrix} \]
	\bigskip

	\divideline

	\bigskip

	\alert{A)}  $\begin{bmatrix} 5  \\   -10  \\  -19  \end{bmatrix}$ \quad
	\alert{B)} $\begin{bmatrix} 5  \\   -9  \\  10  \end{bmatrix}$  \quad
	\alert{C)} $\begin{bmatrix} 5  \\   10  \\  9  \end{bmatrix}$  \quad
	\alert{D)} $\begin{bmatrix} 5  \\   -19  \\  -10  \end{bmatrix}$  \quad
	\alert{E)} $\begin{bmatrix} 5  \\   10  \\  19  \end{bmatrix}$

	%\begin{enumerate}
	%\item[A)] The equations have a single solution.
	%\pause
	%\item[B)] The equations are inconsistent.
	%\pause
	%\item[C)] The equations are redundant.
	%\item[D)] The equations are  consistent.
	%\item[E)] Mary made a mistake.
	%\end{enumerate}


\end{frame}

\section{Picturing vectors}

\begin{frame} \frametitle{Picturing Vectors}
	$2$- and $3$-vectors:
	\begin{itemize}
		\item think of vector as arrow from $0$
		\item \alert<2>{multiply number} with vector via scaling.
		\item \alert<3>{add} vectors head-to-tail; \visible<7>{\alert{parallelogram rule};}
	\end{itemize}
	\only<1-4>{\includegraphics[width=7cm]{../figs/class11/VecAdd1}}
	\only<5>{\includegraphics[width=7cm]{../figs/class11/VecAdd2}}
	\only<6>{\includegraphics[width=7cm]{../figs/class11/VecAdd3}}
	\only<7>{\includegraphics[width=7cm]{../figs/class11/VecAdd4}}
\end{frame}

\begin{frame} \frametitle{Application}
	Gives more flexible way to describe a line.
	\pause

	For a line through a point $p$, in direction $\vec{d}$, use
	\[  \vec{x} = \vec{p} + t\cdot \vec{d}, \quad t \in \R \]
	Argument:

	\includegraphics[width=7cm]{../figs/class11/LineViaVec1}
\end{frame}

\begin{frame} \frametitle{Application}
	Gives more flexible way to describe a line.

	For a line through a point $p$, in direction $\vec{d}$, use
	\[  \vec{x} = \vec{p} + \alert{1}\cdot \vec{d}, \quad \alert{t = 1}\]
	Argument:

	\includegraphics[width=7cm]{../figs/class11/LineViaVec2}
\end{frame}

\begin{frame} \frametitle{Application}
	Gives more flexible way to describe a line.

	For a line through a point $p$, in direction $\vec{d}$, use
	\[  \vec{x} = \vec{p} + \alert{-1}\cdot \vec{d}, \quad  \alert{t = -1} \]
	Argument:

	\includegraphics[width=7cm]{../figs/class11/LineViaVec3}
\end{frame}

%\begin{frame} \frametitle{Application}
%	Gives more flexible way to describe a line.
%
%	For a line through a point $p$, in direction $\vec{d}$, use
%	\[  \vec{x} = \vec{p} + \alert{t}\cdot \vec{d}, \quad \alert{t \in \R} \]
%	Argument:
%
%	\includegraphics[width=7cm]{../figs/class11/LineViaVec4}
%\end{frame}

\begin{frame}
	\frametitle{Question:  }
	Pictured are points $u, v \in \R^2$.
	\pause

	Which point represents $\vec{u} - 3 \vec{v}$?
	\bigskip

	\includegraphics[width=5cm]{../figs/class11/VecAddClicker0}
\end{frame}

\begin{frame}
	\frametitle{Question:  }
	Pictured are points $u, v \in \R^2$.
	%\pause

	Which point represents $\vec{u} - 3 \vec{v}$?
	\bigskip

	\includegraphics[width=5cm]{../figs/class11/VecAddClicker1}
\end{frame}

\begin{frame}
	\frametitle{Question:  }
	Pictured are points $u, v \in \R^2$.
	%\pause

	Which point represents $\vec{u} - 3 \vec{v}$?
	\bigskip

	\includegraphics[width=5cm]{../figs/class11/VecAddClicker2}
\end{frame}

\begin{frame}
	\frametitle{Question:}
	Pictured are points $u, v \in \R^2$.
	%\pause

	Which point represents $\vec{u} - 3 \vec{v}$?
	\bigskip

	\includegraphics[width=5cm]{../figs/class11/VecAddClicker3}
\end{frame}


\section{Properties of vectors}
\begin{frame} \frametitle{Properties of Vectors}
	For all $\vw, \vx, \vy \in \R^n$ and $s,t \in \R$, we have the following.
	\pause

	\begin{itemize}
		\item $\vx + \vy = \vy + \vx$ \hspace{1cm} (commutative)
		\pause
		\item $(\vx + \vy) + \vw = \vx + (\vy + \vw)$ \hspace{1cm} (associative)
		\pause
		\item $\vz + \vec{0} = \vec{0} +\vz  = \vz$
		\pause
		\item $\vx + (-\vx) = -\vx + \vx = \vec{0}$
		\pause
		\item $t(\vx + \vy) = t\vx + t\vy$ \hspace{1cm} (distributive law)
		\pause
		\item $(s+t)\vx = s\vx + t\vx$ \hspace{1cm} (distributive law)
		\pause
		\item $s(t \vx) = (st) \vx$ \hspace{1cm} (associative)
		\pause
		\item $1 \vx = \vx$
	\end{itemize}

\end{frame}

\section{Linear combination}

\begin{frame} \frametitle{Linear Combination}
	\begin{definition} Suppose $\{ t_1, t_2 \dots  t_k \}$ are all real numbers.

		The vector \[ \vec{y} = t_1 \vec{v}_1 + \dots + t_k \vec{v}_k \] is called a \alert{linear combination} of the vectors $\{ \vec{v}_1, \vec{v}_2 \dots  \vec{v}_k \}$.
	\end{definition}

%	\pause
%	\divideline
%
%	Sample problem:
%
%	Given vectors $\{ \vec{a}_1, \vec{a}_2 \dots  \vec{a}_n, \vec{b} \}$ in $\R^m$, find real numbers $\{ t_1, t_2 \dots  t_n \}$ so that
%	\[ t_1 \vec{a}_1 + \dots + t_n \vec{a}_n = \vec{b}. \]
%	\pause
%	Off hand, could have any number of $\{ t_1, t_2 \dots  t_n \}$ solutions.
\end{frame}

\begin{frame} \frametitle{Linear Combination}
	How to think about solving for $\{ t_1, t_2 \dots  t_n \}$ in the equation
	\[ t_1 \vec{a}_1 + \dots + t_n \vec{a}_n = \vec{b}: \]
	\pause
	Let
	\[ \vec{b} = \begin{bmatrix} b_1  \\   b_2  \\ b_3 \\ \vdots \\    b_m \end{bmatrix}; \hspace{1cm}
	\vec{a}_j = \begin{bmatrix} a_{1j}  \\   a_{2j}  \\ a_{3j} \\ \vdots \\    a_{mj} \end{bmatrix} \; for \;  1 \leq j \leq n
	\]
	\pause
	Then for any $1 \leq i \leq m$, the $i^{th}$ row of the equation becomes:
	\[ t_1 a_{i1} + t_2 a_{i2} + \dots + t_n a_{in}  = b_i \; or\]
	\[{\green a_{i1}t_1 +  a_{i2}t_2 + \dots + a_{in} t_n   = b_i }\]
\end{frame}

\begin{frame} \frametitle{Linear Combination}

	In other words, solving
	\[ t_1 \vec{a}_1 + \dots + t_n \vec{a}_n = \vec{b}: \]
	is the same as solving this system of $m$ linear equations:
	\begin{align*} a_{11}t_1 + \ldots + a_{1n}t_n &= b_1 \\  a_{21}t_1  + \ldots + a_{2n}t_n &= b_2 \\ \vdots \\   a_{m1}t_1  + \ldots + a_{mn}t_n &= b_m\end{align*}
	We just learned how to do this!

\end{frame}

\begin{frame} \frametitle{Linear Combination}
	Solving
	\[ t_1 \vec{a}_1 + \dots + t_n \vec{a}_n = \vec{b}: \]
	is the same as solving a system of $m$ linear equations.
	\pause

	The system has augmented matrix
	\[ \left[\begin{array}{cccc:c}
	a_{11} & a_{12} & \ldots & a_{1n}  & b_1 \\   a_{21} & a_{22} & \ldots & a_{2n} & b_2 \\ \vdots & \vdots & \ddots & \vdots & \vdots \\    a_{m1} & a_{m2} & \ldots & a_{mn} & b_m \end{array}\right] \]

	\pause
	Since each $\vec{a}_j$ is a column of $i$ numbers, can just write

	\[ \begin{bmatrix}
	\vec{a}_1 & \vec{a}_2 & \ldots & \vec{a}_n  & \vec{b} \end{bmatrix} \]

\end{frame}

\begin{frame} \frametitle{Example}
	\bigskip

	Suppose
	\[
	\vec{a}_1 =\begin{bmatrix}
	0 \\   2\\   4 \\ 8 \end{bmatrix} \hspace{3mm}
	\vec{a}_2 =\begin{bmatrix}
	0 \\   2  \\   4  \\ 8\end{bmatrix} \hspace{3mm}
	\vec{a}_3 =\begin{bmatrix}
	6 \\   -1  \\  1 \\  -1\end{bmatrix} \hspace{3mm}
	\vec{a}_4 =\begin{bmatrix}
	0 \\ 6  \\   10 \\ 26\end{bmatrix} \]

	and want to find $c_1, c_2, c_3, c_4$ so that
	\[
	c_1 \vec{a}_1 +c_2 \vec{a}_2+c_3 \vec{a}_3 + c_4 \vec{a}_4 = \begin{bmatrix}
	12 \\ 4  \\   13 \\ 23\end{bmatrix}
	\]

\end{frame}

\begin{frame} \frametitle{Example}
	This translates to the system of linear equations whose augmented matrix is
	\[ \left[\begin{array}{cccc:c}
	0 & 0 & 6 & 0 & 12 \\   2 & 2 & -1 & 6 & 4  \\   4 & 4 & 1 & 10 & 13 \\ 8 & 8 & -1 & 26 & 23\end{array}\right] \]
	\pause

	which reduces to:
	\[  \left[\begin{array}{cccc:c}
	1 & {\orange 1} & 0 & 0 & \frac32  \\   0 & {\orange 0} & 1 & 0 & 2 \\ 0 & {\orange 0} &0 & 1 & \frac12\\   0 & {\orange 0} &  0 & 0 & 0 \end{array}\right]  \]
	\pause

	and so has general solution

	\[ {\orange c_2} = anything, \; c_1  = \frac32 -{\orange c_2} ,  c_3 = 2 , c_4 = \frac12 \]
\end{frame}

%\section{Span}
%
%\begin{frame} \frametitle{Span}
%	\begin{definition}
%		Given a collection $\{ \vec{v}_1,  \vec{v}_2,  \dots ,\vec{v}_k \}$ of vectors in $\R^m$, the set of \alert{all linear combinations} of these vectors, that is all vectors that can be written as
%		\[
%		c_1 \vec{v}_1 + \dots + c_k \vec{v}_k\]
%		for some  $c_1,\dots,c_k \in \R$
%		is denoted
%		\[
%		\Span \left\{\vec{v}_1,\dots,\vec{v}_k\right\}
%		\]
%		and    is called the \alert{span} of $\{\vec{v}_1,\dots,\vec{v}_k\}$.
%	\end{definition}
%	\pause
%
%	Easy example:  If $k = 1$ so there is only one vector $\vec{v}$, then $\Span\{\vec{v}\}$ is just all vectors that are multiples of $\vec{v}$.   That is, $\Span\{\vec{v}\} = \{ c\vec{v} \;|\; c \in \R \}$
%
%\end{frame}
%
%\begin{frame} \frametitle{Span}
%	Picturing the span when $m = 2, 3$:
%	\pause
%
%	When there is only one vector $\vec{v}$ then $\Span\{\vec{v}\} = \{ c\vec{v} \;|\; c \in \R \}$ is just the line that contains both $\vec{0}$ (take $c = 0$) and $\vec{v}$ (take $c = 1$).
%	\pause
%	\divideline
%
%	With two vectors $\vec{u}$ and $\vec{v}$, $\Span\{\vec{u}, \vec{v}\} = \{ c_1\vec{u} + c_2\vec{v}\}$ pictured via the parallelogram rule ($\Span =$ entire plane; $c_i \geq 0$ highlighted):
%
%	\begin{center}
%		\includegraphics[width=7cm]{../figs/class11/Span2in3}
%	\end{center}
%\end{frame}
%
%
%\begin{frame} \frametitle{Span}
%
%	So we can think of the set of \alert{all solutions} as
%	\[
%	\begin{bmatrix}  \frac32 \\ 0  \\   2 \\  \frac12\end{bmatrix} + \Span\left\{\begin{bmatrix}  -1 \\ 1  \\   0 \\ 0\end{bmatrix}\right\}
%	\]
%	\pause
%	So we can picture the solution as a line in the direction of the second vector, going through the point given by the first vector (but in $\R^4$!)
%
%	\includegraphics[width=7cm]{../figs/class11/LineViaVec4}
%\end{frame}
%
%

\section{Compact notation}
\begin{frame} \frametitle{Matrix Multiplication}

	\begin{definition}
		The linear combination \[
		x_1 \vec{a}_1 + \dots + x_k \vec{a}_k \] is abbreviated
		\[
		\begin{bmatrix} \vec{a}_1  &   \vec{a}_2  & \vec{a}_3 & \ldots &    \vec{a}_k \end{bmatrix}
		\begin{bmatrix} x_1  \\   x_2  \\ x_3 \\ \vdots \\    x_k \end{bmatrix}
		\].
	\end{definition}

	Here $\begin{bmatrix} \vec{a}_1  &   \vec{a}_2  & \vec{a}_3 & \ldots &    \vec{a}_k
	\end{bmatrix} $ is the matrix $A$ with $i^{th}$ column $\vec{a}_i$.

\end{frame}

\begin{frame}\frametitle{Matrix Multiplication}
	Simplest example:  each $\vec{a_i} \in \R^1$, i. e. each column in the matrix is just a number:

	\[
	\begin{bmatrix} a_1  &   a_2  & a_3 & \ldots &    a_k \end{bmatrix}
	\begin{bmatrix} b_1  \\   b_2  \\ b_3 \\ \vdots \\    b_k \end{bmatrix} = a_1 b_1 + a_2 b_2 + \ldots a_k b_k.
	\]
	\pause
	so
	\[
	\begin{bmatrix}1  &  -2 & 3 & -4 \end{bmatrix}
	\begin{bmatrix} 7  \\   3  \\ 1 \\ 2 \end{bmatrix} = \]
	\pause
	\[1\cdot 7 + (-2)\cdot 3 +3 \cdot 1 + (-4)\cdot 2 = 7 - 6 + 3 - 8 = -4
	\pause
	\in \R^1.
	\]

\end{frame}

%\begin{frame}
%	\frametitle{Question}
%	\[
%	\begin{bmatrix} 1  &  -2 & 3 & -4 \end{bmatrix}
%	\begin{bmatrix} 3  \\   7  \\ 2 \\ 1 \end{bmatrix}   =
%	\]
%
%	\bigskip
%
%	\begin{enumerate}
%		\item[A)] -3
%		\item[B)] -6
%		\item[C)] -9
%		\item[D)] 6
%		\item[E)] 9
%	\end{enumerate}
%
%\end{frame}

\section{Matrix-vector multiplication}

\begin{frame} \frametitle{Matrix-vector Multiplication}
	More complicated example:  For $\vec{a_i} \in \R^2, i = 1, 2, 3$:

	\[
	\begin{bmatrix} 2  &   3  & -1 \\ 4 & -2 & 5 \end{bmatrix}
	\begin{bmatrix} 2  \\   1  \\ 4 \end{bmatrix} =
	2\begin{bmatrix} 2  \\  4 \end{bmatrix} +
	1 \begin{bmatrix} 3  \\  -2 \end{bmatrix} +
	4\begin{bmatrix} -1  \\  5 \end{bmatrix}
	\]
	\pause
	so
	\[
	\begin{bmatrix} 2  &   3  & -1 \\ 4 & -2 & 5 \end{bmatrix}
	\begin{bmatrix} 2  \\   1  \\ 4 \end{bmatrix} =
	\begin{bmatrix} 2 \cdot 2 + 3 \cdot 1 -1 \cdot 4  \\  4\cdot 2 - 2 \cdot 1 + 5 \cdot 4 \end{bmatrix}  =
	\begin{bmatrix} 3  \\  26 \end{bmatrix}
	\pause
	\in \R^2.
	\]

	Think of doing the simple case on each row of the matrix $A$:

\end{frame}

\begin{frame} \frametitle{Matrix-vector Multiplication}

	Apply simple case to first row:

	\[
	\begin{bmatrix} 2  &   3  & -1 \\ {\ltgray 4} &  {\ltgray -2} &  {\ltgray 5}  \end{bmatrix}  \begin{bmatrix} 2  \\   1  \\ 4  \end{bmatrix}  =
	\begin{bmatrix} 2 \cdot 2 + 3 \cdot 1 -1 \cdot 4  \\   {\ltgray 4\cdot 2 - 2 \cdot 1 + 5 \cdot 4}  \end{bmatrix}   =
	\begin{bmatrix} 3  \\   {\ltgray 26}  \end{bmatrix}
	\]
	\pause

	Apply simple case to second row:

	\[
	\begin{bmatrix}  {\ltgray 2}  &    {\ltgray 3}  &  {\ltgray -1} \\ 4 & -2 & 5 \end{bmatrix}
	\begin{bmatrix} 2  \\   1  \\ 4 \end{bmatrix}  =
	\begin{bmatrix}  {\ltgray 2 \cdot 2 + 3 \cdot 1 -1 \cdot 4}  \\  4\cdot 2 - 2 \cdot 1 + 5 \cdot 4 \end{bmatrix}   =
	\begin{bmatrix}  {\ltgray 3}  \\  26 \end{bmatrix}
	\pause
	\in \R^2.
	\]

\end{frame}


\begin{frame} \frametitle{Matrix-vector Multiplication}

	Further abbreviation:

	Use vector notation:
	\[
	\begin{bmatrix} x_1  \\   x_2  \\ x_3 \\ \vdots \\    x_k \end{bmatrix} =
	\vec{x}
	\]
	then
	\pause
	\[ x_1 \vec{a}_1 + \dots + x_k \vec{a}_k=
	{\green
		\begin{bmatrix} \vec{a}_1  &   \vec{a}_2  & \vec{a}_3 & \ldots &    \vec{a}_k \end{bmatrix} }
	{\blue
		\begin{bmatrix} x_1  \\   x_2  \\ x_3 \\ \vdots \\    x_k \end{bmatrix} }=
	{\green A}{\blue\vec{x}} \]

\end{frame}

\begin{frame} \frametitle{Matrix-vector Multiplication}
	Summary:  For $A$ an $m \times n$ matrix, and a vector $\vec{x} \in \R^{\alert{n}}$, multiplication $A \vec{x}$ is defined and gives a vector in  $\R^{\alert{m}}$.
	%\pause
	\bigskip

	Multiplication has two important properties:
	\begin{itemize}
		\item  For any vectors $\vu, \vv \in \R^n$, $A(\vu + \vv ) = A\vu + A\vv  $
		\pause
		\item  For any vector $\vu \in \R^n$ and any $c \in \R$, $A(c\vu) = c(A\vu)$.
	\end{itemize}
	\pause
	For example:
	\[
	A({\blue \vu} + {\green \vv} ) =
	\begin{bmatrix}\vec{a}_1  &\vec{a}_2  & \vec{a}_3 & \ldots &    \vec{a}_n \end{bmatrix}( {\blue
		\begin{bmatrix}u_1  \\   u_2  \\ u_3 \\ \vdots \\    u_n \end{bmatrix} } +  {\green
		\begin{bmatrix}v_1  \\   v_2  \\ v_3 \\ \vdots \\    v_n \end{bmatrix} }) =
	\]
	\pause
	\[
	(u_1 + v_1) \vec{a}_1 + \dots + (u_n + v_n) \vec{a}_n =(u_1 \vec{a}_1 + \dots + u_n \vec{a}_n ) + ( v_1 \vec{a}_1 + \dots + v_n \vec{a}_n) = \]
	\[
	A{\blue \vu}  + A{\green \vv}  \]

\end{frame}

%\begin{frame}  \frametitle{Question}
%	\[ \begin{bmatrix} 1  &  -2 & 3 & -4 \\ -2 & 1 & -4 & 3 \end{bmatrix}
%	\begin{bmatrix}3  \\   7  \\ 2 \\ 1 \end{bmatrix} = \]
%
%	\bigskip
%
%	A) $\begin{bmatrix} 7 \\   -9 \end{bmatrix}$ \quad
%	B) $\begin{bmatrix} -9 \\ -4 \end{bmatrix}$ \quad
%	C) $\begin{bmatrix} -9 \\ 1 \end{bmatrix}$ \quad
%	D) $\begin{bmatrix} -4 \\ -9 \end{bmatrix}$ \quad
%	E) $\pi^e$
%
%\end{frame}


\section{Matrix equation}

\begin{frame} \frametitle{Matrix Equation}
	Sample problem from before:

	Given vectors $\{ \vec{a}_1, \vec{a}_2 \dots  \vec{a}_n \}$ and $ \vec{b}$ in $\R^m$, find real numbers $\{ x_1, x_2 \dots  x_n \}$ so that
	\[ x_1 \vec{a}_1 + \dots + x_n \vec{a}_n = \vec{b}. \]

	\pause

	Might arise from the question: Is $\vec{b}$ in $\Span \{ \vec{a}_1, \vec{a}_2 \dots  \vec{a}_n \}$?
	\divideline
	\pause

	\alert{New}: translation to matrix equation:

	Given $m \times n$ matrix $A$ and $\vec{b} \in \R^m$ find a vector $\vec{x} \in \R^n$ so that \[A\vec{x} = \vec{b}\]
	\pause
	where $A =
	\begin{bmatrix} \vec{a}_1  &   \vec{a}_2  & \vec{a}_3 & \ldots &    \vec{a}_n \end{bmatrix}. $
	\pause
	Note: the Matrix-vector multiplication $A\vec{x}$ makes sense only if the number of \alert{columns} in $A$ matches the number of \alert{entries} in $x$.
\end{frame}

%\begin{frame} \frametitle{Background}
%	Background thoughts:
%	\divideline
%
%	If two non-trivial vectors $\vx_1, \vx_2$ both lie on the same line, then
%	$Span\{\vx_1, \vx_2\}$ is just that line.
%
%	\pause
%	\bigskip
%
%	On the other hand, if they {\it don't} lie on the same line, then $Span\{\vx_1, \vx_2\}$ consists of an entire plane.
%	\pause
%
%	(The plane determined by the heads of the vectors and $\vec{0}.$)
%	\bigskip
%
%	\pause
%	So the span of two vectors may be a plane, or it could be something simpler: either a line, or even just $\vec{0}$  in the case that $\vx_1 = \vec{0} = \vx_2$.
%\end{frame}
%
%
%\begin{frame} \frametitle{Background}
%
%	Similarly, if {\it three} non-trivial vectors $\vx_1, \vx_2, \vx_3$ all lie on the same line, then  $Span\{\vx_1, \vx_2, \vx_3\}$ is just that line.
%	\pause
%	\bigskip
%
%	If they don't all lie on the same line, but lie on the same plane, then  $Span\{\vx_1, \vx_2, \vx_3\}$ is just that plane.
%	\pause
%	\bigskip
%
%	If they don't all lie in the same plane, then  $Span\{\vx_1, \vx_2, \vx_3\}$ looks like space.
%
%\end{frame}

\begin{frame} \frametitle{Linear Independence}

	How do we put these ideas into math lingo, so we can be precise?
	\begin{definition}
		A set of vectors $\{\vv_1,\dots,\vv_k\}$ in $\R^m$ is \alert{linearly independent} if and only if the only solution to the equation
		\[
		x_1 \vv_1 + \dots + x_k \vv_k = \vec{0}
		\]
		is the solution $x_i=0$ for $1 \le i \le k$.

		Conversely, the set of vectors
		$\{\vv_1,\dots,\vv_k\}$  is \alert{linearly dependent} if there are real numbers $c_1, ..., c_k$, not all zero, such that
		\[
		c_1 \vv_1 + \dots + c_k \vv_k = \vec{0}.
		\]
	\end{definition}
	\pause

	Idea: if the {\blue {set}} is linearly \alert{independent}, then span is big as possible.
	\pause

	If the {\blue {set}} is linearly \alert{dependent} then span is ``thinner" than it has to be;
	\pause
	you could even throw some away and not change the span.
\end{frame}

\begin{frame} %%%%%%%%%%%
	In pictures:

	\centering
	\includegraphics[width=8cm]{../figs/class11/planeofSoltnsD}

	$\{ \vv_1, \vv_2, \vv_3\}$ linearly \alert{dependent}.

\end{frame}

\begin{frame} %%%%%%%%%%%
	\centering
	\includegraphics[width=8cm]{../figs/class11/planeofSoltnsEye}

	$\{ \vv_1, \vv_2, \vv_3\}$ linearly \alert{in}dependent.
\end{frame}



\begin{frame} \frametitle{Linear Independence}


	If any \alert{subset} of $\{\vv_1,\dots,\vv_k\}$ is linearly dependent, so is the whole set.

	\pause
	\divideline

	Argument:  Suppose, say, $\{\vv_1, \vv_2, \vv_3 \} \subset  \{\vv_1,\dots,\vv_k\}$ is linearly dependent.
	\pause
	This means that there are $c_1, c_2, c_3$ not all $0$ so that
	\[
	c_1\vv_1 + c_2\vv_2 + c_3\vv_3 = \vzero
	\]
	But then,
	\[ c_1\vv_1 + c_2\vv_2 + c_3\vv_3 + {\red 0}\vv_4 + ... + {\red 0}\vv_n = \vzero \]
	so the whole set is linearly dependent.
	\pause
	\divideline

	Equivalently: if  $\{\vv_1,\dots,\vv_k\}$ is linearly \alert{independent} then so is every subset of vectors from $\{\vv_1,\dots,\vv_k\}$.
\end{frame}

\section{How to determine (in)dependence}

\begin{frame} \frametitle{Determining Independence}
	How to check whether a set of vectors $\{\vec{a}_1,\dots,\vec{a}_n\}$ is linearly dependent:
	\pause  It's equivalent to saying there is \alert{non-zero} solution to
	\[ x_1 \vec{a}_1 + \dots + x_n \vec{a}_n = \vec{0}. \]
	\pause
	But this is a homogeneous system of linear equations - we can answer that question!
	\pause
	\bigskip

	Construct associated matrix of column vectors:
	\[
	A = \begin{bmatrix}  a_{11} & a_{12} & \ldots & a_{1n} \\   a_{21} & a_{22} & \ldots & a_{2n} \\ \vdots & \vdots & \ddots & \vdots \\    a_{m1} & a_{m2} & \ldots & a_{mn} \end{bmatrix}
	\]

	Reduce to {\blue echelon form} and see if there are any \alert{free variables}.
	\pause

	There are  \alert{no free variables} if and only if \alert{linearly independent}
\end{frame}

\end{document}

%%% Local Variables:
%%% mode: latex
%%% TeX-master: t
%%% End:
