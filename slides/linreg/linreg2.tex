\documentclass{beamer}

\let\val\undefined
\usepackage{pgf}
\usepackage{pgfplots}
\usepackage{tikz}
\usepackage{booktabs}
\usepackage{natbib}
\usepackage{algorithm2e}
\usepackage{siunitx}
\usepackage{framed}
\usepackage{longtable}
\usepackage{amsmath}
\usepackage{amsthm}
\usepackage{grffile}

\usetheme[progressbar=frametitle]{metropolis}
\usepackage{libertine}


\usetikzlibrary{arrows,automata,backgrounds,positioning,decorations,intersections,matrix}

% *** Styles ***
\setbeamertemplate{navigation symbols}{}
\usecolortheme{dolphin}
%\usecolortheme{rose}
%\setbeamercovered{transparent}
\usefonttheme{professionalfonts}
%\usefonttheme[onlymath]{serif}

\definecolor{maroon(x11)}{rgb}{0.69, 0.19, 0.38}
\newcommand{\tc}[2]{\textcolor{#1}{#2}}
\newcommand{\tcb}[1]{\tc{blue}{#1}}
\newcommand{\tcr}[1]{\tc{red}{#1}}
\newcommand{\tcg}[1]{\tc{green}{#1}}
\newcommand{\tcm}[1]{\tc{maroon(x11)}{#1}}

\def\checkmark{\tikz\fill[scale=0.4](0,.35) -- (.25,0) -- (1,.7) -- (.25,.15) -- cycle;}

\newcommand{\Ex}{\mathbb{E}}
\newcommand{\var}{\operatorname{Var}}
%\newcommand{\Pr}{\mathbb{P}}

\definecolor{varcolor}{RGB}{132,23,49}
\newcommand{\varname}[1]{\textcolor{varcolor}{\mathsf{#1}}}

\let\Var\undefined
\DeclareMathOperator{\RSS}{RSS}
\DeclareMathOperator{\Var}{Var}

\title{Linear Regression: Practical Considerations}
\author{Marek Petrik}
\date{February 9, 2022}

\begin{document}
\begin{frame}
	\maketitle
	\tiny{Some of the figures in this presentation are taken from "An Introduction to Statistical Learning, with applications in R"  (Springer, 2013) with permission from the authors: G. James, D. Witten,  T. Hastie and R. Tibshirani }
\end{frame}

\begin{frame}\frametitle{Last Class}
	\begin{enumerate}
		\item Simple linear regression
		\vfill
		\item Estimating coefficients ($\beta$)
		\vfill
		\item $R^2$ error and correlation coefficient
	\end{enumerate}
\end{frame}

\begin{frame} \frametitle{Simple Linear Regression}
\begin{itemize}
	\item We have only one feature
	\[ Y \approx \beta_0 + \beta_1 X \qquad Y = \beta_0 + \beta_1 X + \epsilon \]
	\item Example:
	\begin{center}
		\includegraphics[width=0.75\linewidth]{{../figs/class2/sales_tv_reg}.pdf}
	\end{center}
	\[ \textrm{Sales} \approx \beta_0 + \beta_1 \times \varname{TV} \]
\end{itemize}
\end{frame}


\begin{frame} \frametitle{How To Estimate Coefficients}
	\begin{itemize}
		\item No line that will have no errors on data $x_i$
		\item Prediction:
		\[ \hat{y}_i = \hat\beta_0 + \hat{\beta_1} x_i\]
		\item Errors ($y_i$ are true values):
		\[ e_i = y_i - \hat{y}_i  \]
		\begin{center}\includegraphics[width=0.75\linewidth]{{../islrfigs/Chapter3/3.1}.pdf}\end{center}
	\end{itemize}
\end{frame}


\begin{frame} \frametitle{Residual Sum of Squares}
	\begin{itemize}
		\item Residual Sum of Squares
		\[ \operatorname{RSS} = e_1^2 + e_2^2 + e_3^2 + \cdots + e_n^2  = \sum_{i=1}^n e_i^2\]
		\item Equivalently:
		\[ \operatorname{RSS} = \sum_{i=1}^n ( y_i - \hat{\beta}_0 - \hat\beta_1 x_i )^2 \]
	\end{itemize}
\end{frame}

\begin{frame}\frametitle{$R^2$ Statistic}
	\[ R^2 = 1 - \frac{\operatorname{RSS}}{\operatorname{TSS}} = 1 - \frac{\sum_{i=1}^n (\tcm{y_i} - \tcr{\hat{y}_i})^2 }{\sum_{i=1}^n (\tcm{y_i} - \tcb{\bar{y}})^2} \]
	\begin{itemize}
		\item RSS - residual sum of squares, TSS - total sum of squares
		\item $R^2$ measures the goodness of the fit as a proportion
		\item Proportion of data variance explained by the model
		\item Extreme values:
		\begin{description}
			\item[$0$:] Model does not explain data
			\item[$1$:] Model explains data perfectly
		\end{description}
	\end{itemize}
\end{frame}


\begin{frame}\frametitle{Today: Useful Linear Regression}
    \begin{Large}
	\begin{enumerate}
        \item \textbf{Multiple linear regression}
        \vfill
		\item Inference with linear regression
        \vfill
		\item Designing features and non-linear functions
        \vfill
		\item Possible problems: What can go wrong?
        \vfill
		\item Bias and Variance Tradeoff
        \vfill
		\item Demos
	\end{enumerate}
    \end{Large}
\end{frame}

\begin{frame} \frametitle{Multiple Linear Regression}
    \begin{itemize}
        \item Usually more than one feature is available
        \[ \varname{sales} = \beta_0 + \beta_1 \times \varname{TV} + \beta_2 \times \varname{radio} + \beta_3 \times \varname{newspaper}  + \epsilon \]
        \item In general:
        \[ Y = \beta_0 + \sum_{j=1}^p \beta_j X_j \]
    \end{itemize}
\end{frame}


\begin{frame} \frametitle{Multiple Linear Regression}
    \begin{center}\includegraphics[width=\linewidth]{{../islrfigs/Chapter3/3.4}.pdf}\end{center}
\end{frame}

\begin{frame} \frametitle{Estimating Coefficients}
    \begin{itemize}
        \item Prediction:
        \[ \hat{y}_i = \hat\beta_0 + \sum_{j=1}^p \hat\beta_j x_{ij} \]
        \item Errors ($y_i$ are true values):
        \[ e_i = y_i - \hat{y}_i  \]
        \item Residual Sum of Squares
        \[ \operatorname{RSS} = e_1^2 + e_2^2 + e_3^2 + \cdots + e_n^2  = \sum_{i=1}^n e_i^2\]
        \item How to minimize RSS? Linear algebra!
    \end{itemize}
\end{frame}

\begin{frame}\frametitle{Today: Useful Linear Regression}
    \begin{Large}
        \begin{enumerate}
            \item Multiple linear regression
            \vfill
            \item \textbf{Inference with linear regression}
            \vfill
            \item Designing features and non-linear functions
            \vfill
            \item Possible problems: What can go wrong?
            \vfill
            \item Bias and Variance Tradeoff
            \vfill
            \item Demos
        \end{enumerate}
    \end{Large}
\end{frame}


\begin{frame} \frametitle{Inference from Linear Regression}
\begin{enumerate}
%	\item Are predictors $X_1, X_2, \ldots, X_p$ really predicting $Y$?
	\item Is only a subset of predictors useful?
    \vfill
	\item How well does linear model fit data?
    \vfill
	\item What $Y$ should we predict and how accurate is it?
\end{enumerate}
\end{frame}

%\begin{frame} \frametitle{Hypothesis Testing}
%\begin{itemize}
%\item Null hypothesis $H_0$:
%\begin{center}There is no relationship between $X$ and $Y$ \end{center}
%\[ \beta_1 = 0 \]
%\vfill
%\item Alternative hypothesis $H_1$:
%\begin{center}There is some relationship between $X$ and $Y$ \end{center}
%\[ \beta_1 \neq 0 \]
%\vfill
%\item Seek to reject hypothesis $H_0$ with small ``probability'' ($p$-value) of making a mistake
%\item \alert{Not covered \& neccessary for this class!} No need to compute $p$-values
%\end{itemize}
%\end{frame}

%\begin{frame} \frametitle{Inference 1}
%	\begin{center}``Are predictors $X_1, X_2, \ldots, X_p$ really predicting $Y$?''\end{center}
%	\begin{itemize}
%		\item Null hypothesis $H_0$:
%		\begin{center}There is no relationship between $X$ and $Y$ \end{center}
%		\[ \beta_1 = 0 \]
%		\vfill
%		\item Alternative hypothesis $H_1$:
%		\begin{center}There is some relationship between $X$ and $Y$ \end{center}
%		\[ \beta_1 \neq 0 \]
%		\vfill
%		\item Seek to reject hypothesis $H_0$ with small ``probability'' ($p$-value) of making a mistake
%		\item See ISL 3.2.2 on how to compute F-statistic and reject $H_0$
%	\end{itemize}
%\end{frame}

\begin{frame}{What Happens When Adding Features?}
\emph{Want to answer: Is only a subset of predictors useful?}

\begin{enumerate}
\pause
\item RSS on the training data (why?)
\pause
\item $R^2$
\pause
\item MSE on the test data	(why?)
\end{enumerate}
\end{frame}

\begin{frame} \frametitle{Minimizing Residual Sum of Squares}
\[ \min_{\beta_0, \beta_1}\; \operatorname{RSS}  = \min_{\beta_0, \beta_1}\; \sum_{i=1}^n e_i^2 = \min_{\beta_0, \beta_1}\; \sum_{i=1}^n ( y_i - \beta_0 - \beta_1 x_i )^2 \]
\begin{center}\includegraphics[width=0.6\linewidth]{{../islrfigs/Chapter3/3.2b}.pdf}\end{center}
\end{frame}


\begin{frame}	\frametitle{Inference}
\begin{center} ``Is only a subset of predictors useful?'' \end{center}
\begin{itemize}
\item Compare prediction accuracy with only a subset of features
\item<2-> \textbf{RSS never increases with more features!}
\item<3-> Penalize using more features:
\begin{enumerate}
\item Mallows $C_p$
\item Akaike information criterion
\item Bayesian information criterion
\item Adjusted $R^2$
\end{enumerate}
\item<4-> Testing all subsets of features is impractical: $2^p$ options!
\item<5> \textbf{Cross-validation} and \textbf{LASSO} (later)
\end{itemize}
\end{frame}


%\begin{frame} \frametitle{Inference 2}
%\begin{center} ``How well does linear model fit data?'' \end{center}
%\begin{itemize}
%\item $R^2$ also always increases with more features (like RSS)
%\item Is the model linear? Plot it:
%\begin{center}\includegraphics[width=0.8\linewidth]{{../islrfigs/Chapter3/3.5}.pdf}\end{center}
%\end{itemize}
%\end{frame}


%\begin{frame} \frametitle{Inference 4}
%\begin{center}
%``What $Y$ should we predict and how accurate is it?''
%\end{center}
%\begin{itemize}
%\item The linear model is used to make predictions:
%\[ y_{\text{predicted}} = \hat{\beta}_0 + \hat{\beta}_1 \, x_{\text{new}} \]
%\item Can also predict a confidence interval (based on estimate on $\epsilon$):
%\item<2> \textbf{Example}: Spent $\$100,000$ on TV and $\$20,000$ on Radio advertising
%\begin{itemize}
%\item \textbf{Confidence interval}: predict $f(X)$ (the average response):
%\[ f(x) \in [10.985, 11,528] \]
%\item \textbf{Prediction interval}: predict $f(X) + \epsilon$ (response + possible noise)
%\[ f(x) \in [7.930,14.580] \]
%\end{itemize}
%\end{itemize}
%\end{frame}

\begin{frame} \frametitle{Feature Engineering}
	\begin{center}
		What if we have \ldots
	\end{center}
	\begin{enumerate}
		\item Qualitative features: (gender, car color, major)
		\item Interaction between features: non-additivity
		\item Nonlinear relationships
	\end{enumerate}
\end{frame}

\begin{frame}\frametitle{Today: Useful Linear Regression}
    \begin{Large}
        \begin{enumerate}
            \item Multiple linear regression
            \vfill
            \item Inference with linear regression
            \vfill
            \item \textbf{Designing features and non-linear functions}
            \vfill
            \item Possible problems: What can go wrong?
            \vfill
            \item Bias and Variance Tradeoff
            \vfill
            \item Demos
        \end{enumerate}
    \end{Large}
\end{frame}


\begin{frame} \frametitle{Qualitative Features: 2 Values}
	\begin{itemize}
		\item Predict $\varname{salary}$ as a function of $\varname{gender}$
		\item Feature $\varname{gender}_i \in \{ \operatorname{male}, \operatorname{female} \}$
		\item<2-> Introduce \textbf{indicator variable} $x_i$: (AKA dummy variable, \ldots)
		\[x_i = \begin{cases}
			0 & \text{if } \varname{gender}_i = \operatorname{male} \\
			1 & \text{if } \varname{gender}_i = \operatorname{female} \\
		\end{cases} \]
		\item<2-> Predict salary as:
		\[ \varname{salary} = \beta_0 + \beta_1 \times x_i  = \begin{cases}
			\beta_0 & \text{if } \varname{gender}_i = \operatorname{male} \\
			\beta_0 + \beta_1 & \text{if } \varname{gender}_i = \operatorname{female} \\
		\end{cases} \]
		\item<3-> $\beta_1$ is the difference between female and male salaries
	\end{itemize}
\end{frame}

\begin{frame} \frametitle{Qualitative Features: Many Values}
		\begin{itemize}
		\item Predict $\varname{salary}$ as a function of $\varname{state}$
		\item Feature $\varname{state}_i \in \{ \operatorname{MA}, \operatorname{NH}, \operatorname{ME} \}$
		\item What about $x_i$:
		\[x_i = \begin{cases}
		0 & \text{if } \varname{state}_i = \operatorname{MA} \\
		1 & \text{if } \varname{state}_i = \operatorname{NH} \\
		2 & \text{if } \varname{state}_i = \operatorname{ME} \\
		\end{cases} \]
		\item<2-> Predict salary as:
		\[ \varname{salary} = \beta_0 + \beta_1 \times x_i  = \begin{cases}
		\beta_0  & \text{if } \varname{state}_i = \operatorname{MA} \\
		\beta_0 + \beta_1 & \text{if } \varname{state}_i = \operatorname{NH} \\
		\beta_0 + 2\times \beta_1 & \text{if } \varname{state}_i = \operatorname{ME} \\
		\end{cases} \]
		\item<3> \alert{Does not work}: NH salary always average of MA and ME
	\end{itemize}
\end{frame}

\begin{frame} \frametitle{Qualitative Features: Many Values The Right Way}
\begin{itemize}
	\item Predict $\varname{salary}$ as a function of $\varname{state}$
	\item Feature $\varname{state}_i \in \{ \operatorname{MA}, \operatorname{NH}, \operatorname{ME} \}$
	\item<2-> Introduce 2 \textbf{indicator variables} $x_i, z_i$:
	\[x_i = \begin{cases}
	0 & \text{if } \varname{state}_i \neq \operatorname{MA} \\
	1 & \text{if } \varname{state}_i = \operatorname{MA} \\
	\end{cases} \qquad
	z_i = \begin{cases}
	0 & \text{if } \varname{state}_i \neq \operatorname{NH} \\
	1 & \text{if } \varname{state}_i = \operatorname{NH} \\
	\end{cases}\]
	\item<2-> Predict salary as:
	\[ \varname{salary} = \beta_0 + \beta_1 \times x_i + \beta_2 \times z_i  =
	\begin{cases}
	\beta_0 + \beta_1 & \text{if } \varname{state}_i = \operatorname{MA} \\
	\beta_0 + \beta_2 & \text{if } \varname{state}_i = \operatorname{NH} \\
	\beta_0 		  & \text{if } \varname{state}_i = \operatorname{ME} \\
	\end{cases} \]
	\item<3-> \alert{Need an indicator variable for ME? Why?} hint: linear independence
\end{itemize}
\end{frame}

\begin{frame} \frametitle{Removing Additive Assumption}
	\begin{itemize}
		\item What is the additive assumption?
		\[ \varname{sales} = \beta_0 + \beta_1 \times \varname{TV} + \beta_2 \times \varname{radio}  \]
		\item What if $\varname{TV}$ and $\varname{radio}$ interact?
		\item<2-> Add new feature:
		\[ \varname{sales} = \beta_0 + \beta_1 \times \varname{TV} + \beta_2 \times \varname{radio} + \beta_3 \times \varname{TV} \times \varname{radio} \]
	\end{itemize}
\end{frame}

\begin{frame}	\frametitle{Example of Interaction}
	\vspace{-10mm}
	\begin{center}\includegraphics[width=\linewidth]{{../islrfigs/Chapter3/3.7}.pdf}\end{center}%
	\vspace{-10mm}
	\begin{columns}[T]
	\begin{column}{0.5\linewidth}
		\begin{gather*}
		\varname{balance}_i = \\
		\beta_0 + \\
		\beta_1\times\varname{income}_i  + \\
		\beta_2 \times\varname{student}_i
		\end{gather*}
	\end{column}
	\begin{column}{0.5\linewidth}
		\begin{gather*}
		\varname{balance}_i = \\
		\beta_0 + \beta_1\times\varname{income}_i + \\
		\beta_2 \times\varname{student}_i + \\
		\beta_3 \times\varname{student}_i \times \varname{income}_i
		\end{gather*}
	\end{column}
	\end{columns}
\end{frame}

\begin{frame} \frametitle{Nonlinear Relationship}
	Can we use linear regression to fit a nonlinear function?
	\vspace{-6mm}
	\begin{center}\includegraphics[width=\linewidth]{{../islrfigs/Chapter3/3.8}.pdf}\end{center}%
	\vspace{-10mm}
\end{frame}

\begin{frame} \frametitle{Nonlinear Relationship}
	\begin{itemize}
		\item Linear regression can fit a nonlinear function
		\item Just introduce new features!
		\item Linear regression:
		\[ \varname{mpg} = \beta_0 + \beta_1 \times \varname{power} \ \]
		\item Degree $2$ (Quadratic):
		\[ \varname{mpg} = \beta_0 + \beta_1 \times \varname{power} + \beta_2 \times \varname{power}^2 \]
		\item Degree $k$:
		\[ \varname{mpg} = \sum_{i=0}^{k} \beta_k \times \varname{power}^k \]
	\end{itemize}
\end{frame}

\begin{frame}\frametitle{Today: Useful Linear Regression}
    \begin{Large}
        \begin{enumerate}
            \item Multiple linear regression
            \vfill
            \item Inference with linear regression
            \vfill
            \item Designing features and non-linear functions
            \vfill
            \item \textbf{Possible problems: What can go wrong?}
            \vfill
            \item Bias and Variance Tradeoff
            \vfill
            \item Demos
        \end{enumerate}
    \end{Large}
\end{frame}

\begin{frame} \frametitle{What Can Wrong}
	Many ways to fail:
	\begin{enumerate}
		\item Response variable is non-linear
		\item Errors are correlated
		\item Error variance is not constant
		\item Outlier data
		\item Points with high leverage
		\item Features are collinear
	\end{enumerate}
	What can be done about it?
\end{frame}



\begin{frame}\frametitle{Response variable is Non-linear}
	\begin{itemize}
		\item We can fit a nonlinear model
		\[ \varname{mpg} = \beta_0 + \beta_1 \times \varname{power} + \beta_2 \times \varname{power}^2 \]
		\item But how do we know we should?
		\item<2-> Residual plot
		\begin{center}\includegraphics[width=\linewidth]{{../islrfigs/Chapter3/3.9}.pdf}\end{center}%
	\end{itemize}
\end{frame}

\begin{frame}\frametitle{Correlated Errors}
	\begin{itemize}
		\item The errors $\epsilon_i$ are not independent
		\item For example, use each data point twice
		\item No additional information, but error is apparently reduced
		\begin{center}\includegraphics[width=0.7\linewidth]{{../islrfigs/Chapter3/3.10}.pdf}\end{center}%
	\end{itemize}
\end{frame}


\begin{frame}\frametitle{Non-constant Variance of Errors}
	\begin{itemize}
		\item Errors $\epsilon_1, \epsilon_2, \ldots, \epsilon_n$
		\item \textbf{Homoscedastic} errors: $\var[\epsilon_1] = \var[\epsilon_2] =  \ldots = \var[\epsilon_n]$
		\item \textbf{Heteroscedastic} errors can cause a wrong fit
		\begin{center}\includegraphics[width=\linewidth]{{../islrfigs/Chapter3/3.11}.pdf}\end{center}%
		\item \textbf{Remedy}: scale response variable $Y$ or use \emph{weighted linear regression}
	\end{itemize}
\end{frame}

\begin{frame}\frametitle{Outlier Data Points}
	\begin{itemize}
		\item Data point that is far away from others
		\item Measurement failure, sensor fails, missing data point
		\item Can seriously influence prediction quality
	\end{itemize}
	\begin{center}\includegraphics[width=\linewidth]{{../islrfigs/Chapter3/3.12}.pdf}\end{center}%
\end{frame}

\begin{frame}\frametitle{Points with High Leverage}
	\begin{itemize}
		\item Points with unusual value of $x_i$
		\item Single data point can have significant impact on prediction
		\item R and other packages can compute leverages of data points
		\begin{center}\includegraphics[width=\linewidth]{{../islrfigs/Chapter3/3.13}.pdf}\end{center}%
		\item Good to remove points with high leverage and residual
	\end{itemize}
\end{frame}

%\begin{frame}\frametitle{Collinear Features}
%	\begin{itemize}
%		\item Collinear features can reduce prediction confidence
%		\[\varname{credit} \approx  \beta_0 + \beta_1 \times \varname{age} + \beta_2\times\varname{limit} \]
%		\begin{center}\includegraphics[width=\linewidth]{{../islrfigs/Chapter3/3.14}.pdf}\end{center}%
%		\item Detect by computing feature correlations
%		\item Solution: remove collinear feature or combine them
%	\end{itemize}
%\end{frame}

%\begin{frame} \frametitle{Other Material}
%	\begin{enumerate}
%		\item Analyzing results of linear regression (LR notebook)
%		\item QQ-plots (QQplot notebook)
%	\end{enumerate}
%\end{frame}


\begin{frame}\frametitle{Today: Useful Linear Regression}
    \begin{Large}
        \begin{enumerate}
            \item Multiple linear regression
            \vfill
            \item Inference with linear regression
            \vfill
            \item Designing features and non-linear functions
            \vfill
            \item Possible problems: What can go wrong?
            \vfill
            \item \textbf{Bias and Variance Tradeoff}
            \vfill
            \item Demos
        \end{enumerate}
    \end{Large}
\end{frame}

\begin{frame} \frametitle{Flexibility of ML Methods}
	\begin{itemize}
		\item Why not just test on the training data?
		\begin{center}
			KNN Error
			\includegraphics[width=0.9\linewidth]{{../islrfigs/Chapter2/2.17}.pdf}
		\end{center}
	\end{itemize}
\end{frame}

\begin{frame}\frametitle{Understanding Flexibility: Bias-Variance Decomposition}
	\[ Y = f(X) + \alert{\epsilon} \]
	Mean Squared Error for point $x_0$ of trained $\hat{f}$ can be decomposed as:
	\[ \varname{MSE} = \Ex [(y_0 - \hat{f}(x_0))^2] = \underbrace{\Var[\hat{f}(x_0)]}_{\text{\tcg{Variance}}} + \underbrace{\Ex[\hat{f}(x_0) - f(x_0)]^2}_{\text{\tcb{Bias}$^2$}} + \Var[\tcr{\epsilon}] \]
	\begin{itemize}
		\item \textbf{\tcb{Bias}}: How well would method work with average dataset
		\item \textbf{\tcg{Variance}}: How much prediction varies with different datasets
	\end{itemize}
\end{frame}

\begin{frame} \frametitle{Bias-Variance Trade-off}
	\begin{center}\includegraphics[width=\linewidth]{{../islrfigs/Chapter2/2.12}.pdf}\end{center}
	Flexibility:
	\begin{enumerate}
		\item Increases with number of features in LR
		\item Increases if decreasing $k$ in KNN
	\end{enumerate}
\end{frame}

\begin{frame}\frametitle{Bias-Variance Decomposition}
	Consider a \textbf{fixed} data point (test set of size 1):
	\[ y_0 = f(x_0) + \tcr{\epsilon} \]
	How well would the classifier do on $x_0$ if trained on \textbf{many} different datasets:
	\[ \Ex_{\hat{f}} [(y_0 - \hat{f}(x_0))^2] = \underbrace{\Var_{\hat{f}} [\hat{f}(x_0)]}_{\text{\tcg{Variance}}} + \underbrace{\Ex_{\hat{f}} [\hat{f}(x_0) - f(x_0)]^2}_{\text{\tcb{Bias}$^2$}} + \Var[\tcr{\epsilon}] \]
	Always ask: \emph{What is random? What is a random variable?}
\end{frame}

\begin{frame}\frametitle{Today: Useful Linear Regression}
    \begin{Large}
        \begin{enumerate}
            \item Multiple linear regression
            \vfill
            \item Inference with linear regression
            \vfill
            \item Designing features and non-linear functions
            \vfill
            \item Possible problems: What can go wrong?
            \vfill
            \item Bias and Variance Tradeoff
            \vfill
            \item \textbf{Demos}
        \end{enumerate}
    \end{Large}
\end{frame}

\begin{frame} \frametitle{Other Material}
	\begin{enumerate}
        \item QQ-plot
		\item Demonstration of Bias and Variance
		\item Nonlinear linear regression
	\end{enumerate}
\end{frame}

\end{document}