\documentclass{beamer}


\let\val\undefined
\usepackage{pgf}
\usepackage{pgfplots}
\usepackage{tikz}
\usepackage{booktabs}
\usepackage{natbib}
\usepackage{algorithm2e}
\usepackage{siunitx}
\usepackage{framed}
\usepackage{longtable}
\usepackage{amsmath}
\usepackage{amsthm}
\usepackage{bigdelim,multirow}
\usepackage{grffile}

\usetheme[progressbar=frametitle]{metropolis}
\usepackage{libertine}




\usetikzlibrary{arrows,automata,backgrounds,positioning,decorations,intersections,matrix}

% *** Styles ***
\setbeamertemplate{navigation symbols}{}
\usecolortheme{dolphin}
%\usecolortheme{rose}
%\setbeamercovered{transparent}
\usefonttheme{professionalfonts}
%\usefonttheme[onlymath]{serif}

% *** Colors ***
\definecolor{maroon(x11)}{rgb}{0.69, 0.19, 0.38}
\newcommand{\tc}[2]{\textcolor{#1}{#2}}
\newcommand{\tcb}[1]{\tc{blue}{#1}}
\newcommand{\tcr}[1]{\tc{red}{#1}}
\newcommand{\tcg}[1]{\tc{green}{#1}}
\newcommand{\tcm}[1]{\tc{maroon(x11)}{#1}}

\newcommand{\Ex}{\mathbb{E}}
\newcommand{\E}{\mathbb{E}}
\newcommand{\Real}{\mathbb{R}}
\renewcommand{\P}{\mathbb{P}}
\newcommand{\V}{\mathbb{V}}
\newcommand{\I}{\mathbb{I}}
\newcommand{\sd}{\operatorname{sd}}
\newcommand{\cov}{\operatorname{Cov}}
\newcommand{\corr}{\operatorname{corr}}
\let\Var\undefined
\DeclareMathOperator{\RSS}{RSS}
\DeclareMathOperator{\Var}{Var}

\def\checkmark{\tikz\fill[scale=0.4](0,.35) -- (.25,0) -- (1,.7) -- (.25,.15) -- cycle;}

%\newcommand{\Pr}{\mathbb{P}}

\definecolor{varcolor}{RGB}{132,23,49}
\newcommand{\varname}[1]{\textcolor{varcolor}{\mathsf{#1}}}

\title{Linear Regression}
\author{Marek Petrik}
\date{February 7, 2022}

\begin{document}
\begin{frame}
	\maketitle
	\tiny{Some of the figures in this presentation are taken from "An Introduction to Statistical Learning, with applications in R"  (Springer, 2013) with permission from the authors: G. James, D. Witten,  T. Hastie and R. Tibshirani }
\end{frame}

\begin{frame}
  \frametitle{Linear Algebra Review}
  \begin{itemize}
  \item Solve a system of linear equations
   \vfill
  \item Linear combination of vector
   \vfill
  \item Matrix and vector operations
  \end{itemize}
  
\end{frame}


\begin{frame}\frametitle{Linear Regression}
\begin{itemize}
\item \textbf{Today:} What is linear regression
\begin{enumerate}
\item Correlation and samples
\item What is linear regression
\item Evaluating fit
\item Solving linear regression
\end{enumerate}
\pause
\vfill
\item \textbf{Next time:} How to really make it work
\begin{itemize}
\item Multiple features
\item Nonlinear linear regression
\item How things may go wrong
\item How to fix and diagnose it
\end{itemize}
\end{itemize}
\end{frame}

\begin{frame} \frametitle{Machine Learning Choices ...}
    \centering
    \includegraphics[width=\linewidth]{../figs/class1/ml_map.png}\\
    {\tiny Source: \url{http://scikit-learn.org/stable/tutorial/machine_learning_map/index.html}} \\[3mm]
\end{frame}

\begin{frame}
  \frametitle{Variance and Standard Deviation}
  Variance
  \[ \V[X] = \E\left[(X - \E[X])^2\right] \]
  \vfill 
  Standard deviation
  \[ \sd[X] = \sqrt{\V[X]} = \sqrt{\E[(X - \E[X])^2]} \]
  \vfill 
  Why standard deviation?
\end{frame}

\begin{frame}
  \frametitle{Variance Rules}
\begin{align*}
  \V[X] &= \E[X^2] - \E[X]^2 \\
  \\
  \V[a \cdot X] &= a^2\cdot \V[X] \\
  \\
  \V \left[ \sum_{i\in \mathcal{I}} X_i \right] &= \sum_{i\in \mathcal{I}} \sum_{j \in \mathcal{I}}  \cov(X_i, X_j)
\end{align*}
\end{frame}

\begin{frame}
  \frametitle{Covariance and Correlation}
  Covariance
\[ \cov(X,Y) = \E[(X - \E[X]) \cdot (Y - \E[Y]) ] \]  
Between $(-\infty , \infty )$
\vfill 
Correlation
\[ \corr(X,Y) = \frac{\cov(X,Y)}{\sd[X]\cdot \sd[Y]} \]
Between $[-1, +1]$
\end{frame}

\begin{frame}
  \frametitle{Covariance and Correlation: Properties}
  \[ \cov(X,X) = ? \]
  \vfill
  \[ \corr(X,X) = ? \]
  \vfill
  \[ \corr(X,Y) = ? \text{ when } X,Y \text{ are independent } \]
\end{frame}

\begin{frame}\frametitle{Correlation Coefficient}
    \textbf{Correlation coefficient} $\corr(X,Y)$ is between $[-1,1]$
    \begin{description}
        \item[$0$:] Variables are not related
        \item[$1$:] Variables are perfectly  related (same)
        \item[$-1$:] Variables are negatively related (different)
    \end{description}
\end{frame}

\begin{frame} \frametitle{Correlation Example}
    \centering
    {\tiny Plotting code in \texttt{slides/figs/class2/plots.R}}
    \includegraphics[width=0.8\linewidth]{../figs/class2/students_example0.pdf} \\[3mm]
    \visible<2>{Correlation: $0$}
\end{frame}

\begin{frame} \frametitle{Correlation Example}
    \centering
    \includegraphics[width=0.8\linewidth]{../figs/class2/students_example1.pdf} \\[3mm]
    \visible<2>{Correlation: $1$}
\end{frame}

\begin{frame} \frametitle{Correlation Example}
    \centering
    \includegraphics[width=0.8\linewidth]{../figs/class2/students_example1_stretched.pdf} \\[3mm]
    \visible<2>{Correlation: $1$}
\end{frame}

\begin{frame} \frametitle{Correlation Example}
    \centering
    \includegraphics[width=0.8\linewidth]{../figs/class2/students_example2.pdf} \\[3mm]
    \visible<2>{Correlation: $-1$}
\end{frame}

\begin{frame} \frametitle{Correlation Example}
    \centering
    \includegraphics[width=0.8\linewidth]{../figs/class2/students_example3.pdf} \\[3mm]
    \visible<2>{Correlation: $0.5$}
\end{frame}

\begin{frame} \frametitle{Correlation Example}
    \centering
    \includegraphics[width=0.8\linewidth]{../figs/class2/students_example4.pdf} \\[3mm]
    \visible<2>{Correlation: $0.0$}
\end{frame}

\begin{frame} \frametitle{Sample Statistics}
    Sample: $x_1, x_2, \ldots, x_n$ \\
    \vfill
    \textbf{Sample Mean}:
    \[ \bar{X} = \frac{1}{n} \sum_{i=1}^n x_i \]
    \textbf{Sample Variance (Biased!)}:
    \[ \sigma^2_X = \frac{1}{n} \sum_{i=1}^n (x_i - \bar{X})^2  \]
    \textbf{Sample Variance (Unbiased)}:
    \[ \sigma^2_X = \frac{1}{n-1} \sum_{i=1}^n (x_i - \bar{X})^2  \]
    Elementary proof: {\tiny\url{https://en.wikipedia.org/wiki/Variance\#Sample_variance}}
\end{frame}


\begin{frame} \frametitle{Today's Overview}
    {\Large
        \begin{enumerate}
            \item Correlation and samples
            \vfill
            \item \textbf{What is linear regression}
            \vfill
            \item Evaluating fit
            \vfill
            \item Solving linear regression
        \end{enumerate}
    }
\end{frame}

\begin{frame}{Why Linear Regression: Simple and Efficient}
\centering
\begin{minipage}{0.4\linewidth}
	\includegraphics[width=\linewidth]{../figs/class2/powder_mill_bridge.pdf}
	{\small Powder Mill Bridge, MA}
\end{minipage}
\hspace{0.15\linewidth}
\begin{minipage}{0.4\linewidth}
	\includegraphics[width=\linewidth]{../figs/class2/powder_mill_bridge_sens.pdf}
	{\small Strain gauges, PMB, MA}
\end{minipage}\\
\vspace{0.6cm}
\begin{minipage}{0.9\linewidth}
	{\small Bridge damage detection success (higher is better)} \\
	\includegraphics[width=\linewidth]{../figs/class2/kathryn_results.pdf}
	{\small Training time: LR: seconds, ANN: days} {\tiny[Kathryn Kaspar, A protocol for using long-term structural health monitoring data to detect and localize damage in bridges, UNH MS Thesis, 2018]}
\end{minipage}
\end{frame}

\begin{frame} \frametitle{What is Machine Learning}
\begin{itemize}
\item Discover unknown function $\alert{f}$:
\[ \tcb{Y} = \tcr{f}(\tcg{X}) + \epsilon \]
\item $\tcr{f}$ = \textbf{hypothesis}, or model
\item $\tcg{X}$ = \textbf{features}, or predictors, or inputs
\item $\tcb{Y}$ = \textbf{response}, or target
\end{itemize}
\end{frame}

\begin{frame}\frametitle{How Good are Predictions?}
    \begin{itemize}
        \item Learned function $\hat{f}$
        \item Test data: ${(x_1,y_1), (x_2, y_2), \ldots}$
        \item \textbf{Mean Squared Error (MSE)}:
        \[
        \varname{MSE} = \frac{1}{n} \sum_{i=1}^{n} (y_i - \hat{f}(x_i))^2
        \]
%        \item This is the estimate of:
%        \[ \varname{MSE} = \Ex[(Y - \hat{f}(X))^2]  = \frac{1}{|\Omega|} \sum_{\omega\in\Omega} (Y(\omega) - \hat{f}(X(\omega)))^2 \]
        \item \textbf{Root Mean Squared Error (RMSE)}: Unchanged units
        \[
        \varname{RMSE} = \sqrt{\varname{MSE}}
        \]
        \item Important: Samples $x_i$ are i.i.d.
    \end{itemize}
\end{frame}

\begin{frame} \frametitle{Prediction Error: Training and test data sets}
    \centering
    \begin{tabular}{rrrll}
        \hline
        & \tcb{mpg} & \tcg{horsepower} & name & \\
        \hline
        1 & 18.00 & 130.00 & chevrolet chevelle malibu & \rdelim\}{3}{3mm}[test] \\
        2 & 15.00 & 165.00 & buick skylark 320 &\\
        3 & 18.00 & 150.00 & plymouth satellite &\\
        \hline \hline
        4 & 16.00 & 150.00 & amc rebel sst & \rdelim\}{7}{3mm}[training] \\
        5 & 17.00 & 140.00 & ford torino & \\
        6 & 15.00 & 198.00 & ford galaxie 500 & \\
        7 & 14.00 & 220.00 & chevrolet impala & \\
        8 & 14.00 & 215.00 & plymouth fury iii & \\
        9 & 14.00 & 225.00 & pontiac catalina & \\
        10 & 15.00 & 190.00 & amc ambassador dpl & \\
        \hline
    \end{tabular}
\end{frame}


\begin{frame} \frametitle{Do We Need Test Data?}
    Why not just test on the training data?
    \pause
    \begin{center}            \includegraphics[width=0.8\linewidth]{../figs/class1/auto_knn_compare.pdf}
    \end{center}
\end{frame}

\begin{frame} \frametitle{KNN Error}
    \includegraphics[width=0.8\linewidth]{{../islrfigs/Chapter2/2.17}.pdf}
\end{frame}
\begin{frame} \frametitle{Simple Linear Regression}
	\begin{itemize}
          \item The function $f$ is \emph{linear} and only one feature:
			\[ \tcr{Y} \approx \tcg{\beta_0} + \tcb{\beta_1} X \qquad \tcr{Y} = \tcg{\beta_0} + \tcb{\beta_1} X  + \epsilon \]
		\item Example:
			\begin{center}\includegraphics[width=0.75\linewidth]{{../figs/class2/sales_tv_reg}.pdf}\end{center}
			\[ \tcr{\textrm{Sales}} \approx \tcg{\beta_0} + \tcb{\beta_1} \times \varname{TV} \]
	\end{itemize}
\end{frame}

\begin{frame} \frametitle{How To Estimate Coefficients}
	\begin{itemize}
		\item No line that will have no errors on data $x_i$
		\item Prediction:
			\[ \tcr{\hat{y}_i} = \tcg{\hat\beta_0} + \tcb{\hat{\beta_1}} x_i\]
		\item Errors ($y_i$ are true values):
	 		\[ e_i = \tcm{y_i} - \tcr{\hat{y}_i}  \]
		\begin{center}\includegraphics[width=0.7\linewidth]{{../islrfigs/Chapter3/3.1}.pdf}\end{center}
	\end{itemize}
\end{frame}

\begin{frame} \frametitle{Residual Sum of Squares}
	\begin{itemize}
		\item Residual Sum of Squares
		\[ \RSS = e_1^2 + e_2^2 + e_3^2 + \cdots + e_n^2  = \sum_{i=1}^n e_i^2\]
		\item Equivalently:
		\[ \RSS = \sum_{i=1}^n ( \tcm{y_i} - \tcg{\hat{\beta}_0} - \tcb{\hat\beta_1} x_i )^2 \]
	\end{itemize}
\end{frame}


\begin{frame} \frametitle{Why Minimize RSS}
    \begin{enumerate}
        \pause
        \item It is convenient: can be solved in closed form
        \vfill
        \pause
        \item Maximize likelihood when $Y = \beta_0 + \beta_1 X + \epsilon$ when $\epsilon \sim \mathcal{N}(0,\sigma^2)$
        \vfill
        \pause
        \item Best Linear Unbiased Estimator (BLUE): Gauss-Markov Theorem (ESL 3.2.2)
    \end{enumerate}
\end{frame}

\begin{frame} \frametitle{Today's Overview}
    {\Large
        \begin{enumerate}
            \item Correlation and samples
            \vfill
            \item What is linear regression
            \vfill
            \item \textbf{Evaluating fit }
            \vfill
            \item Solving linear regression
        \end{enumerate}
    }
\end{frame}


\begin{frame}\frametitle{How Good is Fit?}
    \[ \varname{Sales} = f(\varname{TV}, \varname{Radio}, \varname{Newspaper}) \]
    \begin{center}\includegraphics[width=0.75\linewidth]{{../islrfigs/Chapter2/2.1}.pdf}\end{center}
    \vfill
    \begin{itemize}
        \item How well is linear regression predicting the training data?
        \item Can we be sure that TV advertising really influences the sales?
        \item \alert{Does RSS/MSE answer these questions?}
    \end{itemize}
\end{frame}


\begin{frame}\frametitle{$R^2$ Statistic}
\[ R^2 = 1 - \frac{\operatorname{RSS}}{\operatorname{TSS}} = 1 - \frac{\sum_{i=1}^n (\tcm{y_i} - \tcr{\hat{y}_i})^2 }{\sum_{i=1}^n (\tcm{y_i} - \tcb{\bar{y}})^2} \]
\begin{itemize}
\item RSS - residual sum of squares, TSS - total sum of squares
\item $R^2$ measures the goodness of the fit as a proportion
\item Proportion of data variance explained by the model
\item Extreme values:
\begin{description}
	\item[$0$:] Model does not explain data
	\item[$1$:] Model explains data perfectly
\end{description}
\end{itemize}
\end{frame}

\begin{frame} \frametitle{Example: TV Impact on Sales}
\begin{center}
\includegraphics[width=0.9\linewidth]{{../figs/class2/sales_tv_reg}.pdf} \\
\visible<2>{$R^2 = 0.61$}
\end{center}
\end{frame}

\begin{frame} \frametitle{Example: Radio Impact on Sales}
\begin{center}
\includegraphics[width=0.9\linewidth]{{../figs/class2/sales_radio}.pdf} \\
\visible<2>{$R^2 = 0.33$}
\end{center}
\end{frame}

\begin{frame} \frametitle{Example: Newspaper Impact on Sales}
	\begin{center}
	\includegraphics[width=0.9\linewidth]{{../figs/class2/sales_newspaper}.pdf} \\
	\visible<2>{$R^2 = 0.05$}
	\end{center}
\end{frame}

\begin{frame} \frametitle{$R^2$ Statistic and Correlation Coefficient}
    \visible<2->{\[ R^2 = \corr(X,Y)^2 \]}
    \vfill
    \visible<3->{
        Correlation coefficient $\corr(X,Y)$:
        \begin{description}
            \item[$0$:] Variables are not related, $R^2 = 0$
            \item[$1$:] Variables are perfectly  related (same), $R^2 = 1$
            \item[$-1$:] Variables are negatively related (different), $R^2 = 1$
    \end{description}}
\end{frame}


\begin{frame} \frametitle{Today's Overview}
    {\Large
        \begin{enumerate}
            \item Correlation and samples
            \vfill
            \item What is linear regression
            \vfill
            \item Evaluating fit
            \vfill
            \item \textbf{Solving linear regression}
        \end{enumerate}
    }
\end{frame}

\begin{frame}\frametitle{Computing Best Linear Fit}
	\begin{center}\includegraphics[width=0.8\linewidth]{{../figs/class2/sales_tv_reg}.pdf}\end{center}
\end{frame}

\begin{frame} \frametitle{Minimizing Residual Sum of Squares}
	\[ \min_{\beta_0, \beta_1}\; \operatorname{RSS}  = \min_{\beta_0, \beta_1}\; \sum_{i=1}^n e_i^2 = \min_{\beta_0, \beta_1}\; \sum_{i=1}^n ( y_i - \beta_0 - \beta_1 x_i )^2 \]
	\only<1>{\begin{center}\includegraphics[width=0.6\linewidth]{{../islrfigs/Chapter3/3.2b}.pdf}\end{center}}%
	\only<2>{\begin{center}\includegraphics[width=0.6\linewidth]{{../islrfigs/Chapter3/3.2a}.pdf}\end{center}}
\end{frame}

\begin{frame} \frametitle{Solving for Minimal RSS}
	\[ \min_{\beta_0, \beta_1}\; \sum_{i=1}^n ( y_i - \beta_0 - \beta_1 x_i )^2 \]
	\begin{itemize}
		\item $\operatorname{RSS}$ is a \textbf{convex} function of $\beta_0,\beta_1$
		\item Minimum achieved when (recall the chain rule):
		\begin{align*}
			\frac{\partial \operatorname{RSS}}{\partial \beta_0} &= - 2 \sum_{i=1}^n ( y_i - \beta_0 - \beta_1 x_i ) = 0 \\
			\frac{\partial \operatorname{RSS}}{\partial \beta_1} &= - 2 \sum_{i=1}^n x_i ( y_i - \beta_0 - \beta_1 x_i ) = 0
		\end{align*}
	\end{itemize}
\end{frame}

\begin{frame} \frametitle{Linear Regression Coefficients}
	\[ \min_{\beta_0, \beta_1}\; \sum_{i=1}^n ( y_i - \beta_0 - \beta_1 x_i )^2 \]
	Solution:
	\begin{align*}
		\beta_0 &= \bar{y} - \beta_1 \bar{x} \\
		\beta_1 &= \frac{\sum_{i=1}^{n} (x_i - \bar{x})(y_i - \bar{y}) }{\sum_{i=1}^{n} (x_i - \bar{x})^2} = \frac{\sum_{i=1}^{n} x_i (y_i - \bar{y}) }{\sum_{i=1}^{n} x_i (x_i - \bar{x})}
	\end{align*}
	where
	\[ \bar{x} = \frac{1}{n} \sum_{i=1}^n x_i \qquad \bar{y} = \frac{1}{n} \sum_{i=1}^n y_i \]
\end{frame}


\end{document}
%%% Local Variables:
%%% mode: latex
%%% TeX-master: t
%%% End:
