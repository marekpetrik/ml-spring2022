\documentclass{beamer}


\let\val\undefined
\usepackage{pgf}
\usepackage{pgfplots}
\usepackage{tikz}
\usepackage{booktabs}
\usepackage{natbib}
\usepackage{algorithm2e}
\usepackage{siunitx}
\usepackage{framed}
\usepackage{longtable}
\usepackage{amsmath}
\usepackage{amsthm}

\usetheme[progressbar=frametitle]{metropolis}
\usepackage{libertine}

\usetikzlibrary{arrows,automata,backgrounds,positioning,decorations,intersections,matrix}

% *** Styles ***
\setbeamertemplate{navigation symbols}{}
\usecolortheme{dolphin}
%\usecolortheme{rose}
%\setbeamercovered{transparent}
\usefonttheme{professionalfonts}
%\usefonttheme[onlymath]{serif}

% *** Colors ***
\newcommand{\tc}[2]{\textcolor{#1}{#2}}
\newcommand{\tcb}[1]{\tc{blue}{#1}}
\newcommand{\tcr}[1]{\tc{red}{#1}}
\newcommand{\tcg}[1]{\tc{green}{#1}}

\def\checkmark{\tikz\fill[scale=0.4](0,.35) -- (.25,0) -- (1,.7) -- (.25,.15) -- cycle;}

\newcommand{\Ex}{\mathbb{E}}
\newcommand{\var}{\operatorname{Var}}
%\newcommand{\Pr}{\mathbb{P}}

\definecolor{varcolor}{RGB}{132,23,49}
\newcommand{\varname}[1]{\textcolor{varcolor}{\mathsf{#1}}}

\title{Gradient Descent, Newton's Method}
\subtitle{Optimization for Machine Learning}

\author{Marek Petrik}
\date{Feb 28, 2022}

\begin{document}

\begin{frame} \maketitle \end{frame}


%\begin{frame}
%\centering
%\includegraphics[width=\linewidth]{../figs/class6_5/tuckerbrook.jpg}
%\end{frame}

%\begin{frame}{Fitting Machine Learning Models}
%\begin{enumerate}
%	\item \textbf{Maximum likelihood}: linear regression, logistic regression, LDA, QDA
%	\vfill
%	\item \textbf{MAP}: Lasso, ridge regression, Bayesian models (Latent Dirichlet Allocation)
%	\vfill
%	\item \textbf{Empirical risk minimization}: KNN, SVM, decision trees, NN, linear regression
%\end{enumerate}
%\end{frame}

%\begin{frame} \frametitle{Bayesian Maximum A Posteriori (MAP) }
%	\begin{enumerate}
%		\item \textbf{Maximum likelihood}
%		\[ \max_{\tcr{\operatorname{model}}} \Pr[\tcb{\operatorname{data}} \mid \tcr{\operatorname{model}}]  \]
%		\item \textbf{Maximum a posteriori estimate (MAP)}
%		\[  \max_{\tcr{\operatorname{model}}} \Pr[\tcr{\operatorname{model}} \mid \tcb{\operatorname{data}}] \visible<2->{=  \max_{\tcr{\operatorname{model}}} \Pr[\tcb{\operatorname{data}} \mid \tcr{\operatorname{model}}] \frac{\Pr[\tcr{\operatorname{model}}]}{\Pr[\tcb{\operatorname{data}} ]}} \]
%	\end{enumerate}
%\end{frame}
%
%\begin{frame} \frametitle{Maximum A Posteriori Estimate}
%	\[
%	\max_{\tcr{\operatorname{model}}} \Pr[\tcr{\operatorname{model}} \mid \tcb{\operatorname{data}}] =  \max_{\tcr{\operatorname{model}}} \Pr[\tcb{\operatorname{data}} \mid \tcr{\operatorname{model}}] \frac{\Pr[\tcr{\operatorname{model}}]}{\Pr[\tcb{\operatorname{data}} ]}
%	\]
%	\begin{itemize}
%		\item \textbf{Prior}:
%		\[  \Pr[\tcr{\operatorname{model}}] \]
%		\item \textbf{Posterior}:
%		\[ \Pr[\tcr{\operatorname{model}} \mid \tcb{\operatorname{data}}] \]
%		\item \textbf{Likelihood}:
%		\[ \Pr[\tcb{\operatorname{data}} \mid \tcr{\operatorname{model}}] \]
%	\end{itemize}
%	Biased coin example \ldots
%\end{frame}
%
%\begin{frame} \frametitle{MAP vs Max Likelihood}
%	Computed models are the \textbf{same} when:\\
%	\vfill
%	\begin{enumerate}
%		\item Prior is uniform. \emph{Uninformative priors}
%		\item Amount of data available is large / infinite
%	\end{enumerate}
%	Biased coin example \ldots
%\end{frame}
%
%\begin{frame}{Posteriors for Biased Coin Estimation}
%\centering
%
%See section 3.3 in \emph{Murphy, K. (2012). Machine Learning: A Probabilistic Perspective. Machine Learning: A Probabilistic Perspective.}
%
%\end{frame}
%
%\begin{frame} \frametitle{MAP Advantages and Disadvantages}
%\begin{itemize}
%	\item \textbf{Advantages}: Can provide an informative \emph{prior}.
%	\pause
%	\vfill
%	\item \textbf{Disadvantages}: Must provide a \emph{prior}. Where can we get it?
%\end{itemize}
%\end{frame}
%
%\begin{frame} \frametitle{Uninformative Priors}
%
%\begin{enumerate}
%	\item \emph{Two Envelopes Paradox}: Uninformative priors can be dangerous! \\
%	\url{https://en.wikipedia.org/wiki/Two_envelopes_problem}
%\end{enumerate}
%Also see section 2.8 of \emph{Gelman, A., Carlin, J. B., Stern, H. S., \& Rubin, D. B. (2014). Bayesian Data Analysis. Chapman Texts in Statistical Science Series (3rd ed.).}
%\end{frame}
%
%\begin{frame}
%\centering
%\includegraphics[width=\linewidth]{../figs/class6_5/lowersnowfields.jpg}
%\end{frame}

\begin{frame}{Today}
\begin{enumerate}
	\item Gradient descent
	\item Likelihood for linear regression
	\item Likelihood for logistic regression
	\item Limitations of gradient descent
	\item Newton's method
	\item Computational tools
\end{enumerate}
\end{frame}

\begin{frame}{Machine learning as optimization}
	\textbf{Philosophy}: Formalize the goal before deciding on the solution

	\begin{itemize}
		\item \textbf{Decision variables}: vector $x$ = what can we change?
		\item \textbf{Objective function}: $f(x)$ = what is the goal?
		\item \textbf{Constraints}:  what kinds of decisions are acceptable?
	\end{itemize}
	\pause
	Machine learning examples of:
	\begin{enumerate}
		\item decision variables
		\item objective function
		\item constraints
	\end{enumerate}
\end{frame}

\begin{frame}{Second Order Methods}
	\begin{itemize}
		\item \textbf{First-order methods}: Gradient descent, SGD, Nesterov's acceleration, \ldots
		\[ f(x) \approx f(a) + \nabla f(a) (x-a) \]
		\item \textbf{Second-order methods}: Newton's method, Quasi-Newton, BFGS, \ldots
		\[ f(x) \approx f(a) + \nabla f(a) (x-a) + \frac{1}{2} (x-a)^T \underbrace{\nabla^2 f(a)}_{\text{Hessian}} (x-a) \]
	\end{itemize}
\end{frame}

\begin{frame}{Gradient Descent}
\begin{center}
	\begin{enumerate}
		\item Start with some initial point $x_0$
		\item Initialize counter: $i = 1$
		\item Compute direction: $\Delta x = -\nabla f(x_i)$
		\item Compute step size: $t$ (backtracking line search)
		\item Update solution: $x_i = x + t \Delta x$
		\item Let $i = i + 1$ and \underline{goto} 3
	\end{enumerate}
\end{center}
\textbf{Why}: Minimizing Taylor series approximation
\end{frame}

\begin{frame}{Gradient Descent: When to use it}

Objective function $f(x)$ is:

\begin{enumerate}
	\item Differentiable (continuous)
	\item Strongly convex
\end{enumerate}

\end{frame}

\begin{frame}{Log-Likelihoods}
\begin{itemize}
	\item Linear regression (Gaussian noise with $\sigma^2 = 1$)
	\[ \ell(\beta) = -\frac{1}{2} \sum_{i=1}^n \left(  y_i - \hat{y}(x_i; \beta)\right)^2 + O(1)  \]
	\item Logistic regression, $p(x_i;\beta) =$ prediction prob. (see ESL 4.4.1)
	\begin{align*}
		\ell(\beta) &= \sum_{i=1}^{n} \Bigl( y_i \log p(x_i; \beta) + (1-y_i) \log (1 - p(x_i; \beta)) \Bigr) = \\
		&= \sum_{i=1}^{n} \left( y_i \sum_{j=1}^k \beta^j x_i^j + (1-y_i) \log \left(1 + e^{\sum_{j=1}^k \beta^j x_i^j} \right) \right)
	\end{align*}
\end{itemize}
\end{frame}



\begin{frame}{Computational Tools}
 See R notebook
% \begin{center}
% \includegraphics[width=\linewidth]{../figs/class6_5/cannon.jpg}
% \end{center}
\end{frame}

\end{document}