\documentclass{beamer}

\let\val\undefined

\usepackage{pgf}
\usepackage{pgfplots}
\usepackage{tikz}
\usepackage{booktabs}
\usepackage{natbib}
\usepackage{algorithm2e}
\usepackage{siunitx}
\usepackage{framed}
\usepackage{longtable}
\usepackage{amsmath}
\usepackage{amsthm}
\usepackage{nicefrac}
\usepackage{multirow}
\usepackage{grffile}

\usetheme[progressbar=frametitle]{metropolis}
\usepackage{libertine}

\usetikzlibrary{arrows,automata,backgrounds,positioning,decorations,intersections,matrix}

% *** Styles ***
\setbeamertemplate{navigation symbols}{}
\usecolortheme{dolphin}
\usefonttheme{professionalfonts}

% *** Colors ***
\newcommand{\tc}[2]{\textcolor{#1}{#2}}
\newcommand{\tcb}[1]{\tc{blue}{#1}}
\newcommand{\tcr}[1]{\tc{red}{#1}}
\newcommand{\tcg}[1]{\tc{green}{#1}}

\def\checkmark{\tikz\fill[scale=0.4](0,.35) -- (.25,0) -- (1,.7) -- (.25,.15) -- cycle;}

\newcommand{\Ex}{\mathbb{E}}
\newcommand{\var}{\operatorname{Var}}
%\newcommand{\Pr}{\mathbb{P}}

\definecolor{varcolor}{RGB}{132,23,49}
\newcommand{\varname}[1]{\textcolor{varcolor}{\mathsf{#1}}}

\title{Generalized Linear Models}
\subtitle{}
\author{Marek Petrik}
\date{February 23, 2022}

\newcommand{\plus}{\textcolor{green}{$\mathbf{+}$}}
\newcommand{\minus}{\textcolor{red}{$\mathbf{-}$}}

\begin{document}

\begin{frame} \maketitle \end{frame}

\begin{frame}\frametitle{Machine Learning Methods}

  \begin{enumerate}
    \item K-NN
    \item Linear regression
    \item Logistic regression
    \item LDA, QDA
    \item Naive Bayes
  \end{enumerate}
\end{frame}
  
\begin{frame}
  \frametitle{Which Algorithm to Use?}
  \begin{enumerate}
  \item Classify emails as spam
    \vfill
  \item Recognize hand-written digits
    \vfill
  \item Predict bankruptcy from financial data
    \vfill
  \item Predict the impact of pricing on product demand
    \vfill
  \item Predict the intensity of solar fields
    \vfill
   \item Predict animal prevalence from video trap data
  \end{enumerate}
\end{frame}

\begin{frame}\frametitle{Comparing Distributions: QQ Plots}
  \textbf{QQPlot}: Quantile-quantile plot, helps to identify type of distribution \\
  \vfill
  See the notebook for examples \\
  \vfill
  Meaning:
  \begin{itemize}
  \item \textbf{Identical distributions}: QQ plot follows the line $y = x$
  \item \textbf{Linearly related distributions}: QQ plot follows \emph{some} line
    \item \textbf{Otherwise}: QQ plot does not follow a line
  \end{itemize}
\end{frame}

\begin{frame} \frametitle{Today: Generalized Linear Models}
  \begin{enumerate}
  \item \textbf{Logistic regression}
  \item Linear regression
  \item Poisson regression
  \item Generalized linear models
  \end{enumerate}
\end{frame}

\begin{frame} \frametitle{Estimating Coefficients: Maximum Likelihood}
    \begin{itemize}
            \item \textbf{Likelihood}: Probability that data is generated from a model \only<2>{(\alert{i.i.d. assumption})}
            \only<1>{\[ \ell(\tcr{\operatorname{model}}) = \Pr[\tcb{\operatorname{data}} \mid \tcr{\operatorname{model}}]  \]}
            \only<2>{\[ \ell(\tcr{\operatorname{\beta_0,\beta_1}}) = \Pr[\tcb{\operatorname{Y_1,Y_2,Y_3,\ldots}} \mid \tcr{\operatorname{\beta_0,\beta_1}}] = \prod_{i=1}^n \Pr[\tcb{\operatorname{Y_i}} \mid \tcr{\operatorname{\beta_0,\beta_1}}] \]}
            \item Find the most likely model:
            \only<1>{\[ \max_{\tcr{\operatorname{model}}} \ell(\tcr{\operatorname{model}}) = \max_{\tcr{\operatorname{model}}} \Pr[\tcb{\operatorname{data}} \mid \tcr{\operatorname{model}}]  \]}
            \only<2>{\[ \max_{\tcr{\operatorname{\beta_0,\beta_1}}} \ell(\tcr{\operatorname{\beta_0,\beta_1}}) = \max_{\tcr{\operatorname{\beta_0,\beta_1}}} \Pr[\tcb{\operatorname{Y_1,Y_2,Y_3,\ldots}} \mid \tcr{\operatorname{\beta_0,\beta_1}}]  \]}
            \item Likelihood function is difficult to maximize
            \item Transform it using $\log$ (strictly increasing)
            \only<1>{\[ \max_{\tcr{\operatorname{model}}} \log \ell(\tcr{\operatorname{model}}) \]}
            \only<2>{\[ \max_{\tcr{\operatorname{\beta_0,\beta_1}}} \log \ell(\tcr{\operatorname{\beta_0,\beta_1}}) = \max_{\tcr{\operatorname{\beta_0,\beta_1}}} \sum_{i=1}^n \log \Pr[\tcb{\operatorname{Y_i}} \mid \tcr{\operatorname{\beta_0,\beta_1}}] \]}
            \item Strictly increasing transformation preserves maximizer
    \end{itemize}
\end{frame}

\begin{frame} \frametitle{Max-likelihood: Logistic Regression}
    \begin{itemize}
            \item \textbf{Data}: Features $\tcb{x_i}$ and labels $\tcb{y_i}$
            \item \textbf{Parameters}:
            \[ p_{\tcr{\beta}}(x) = \frac{e^{\tcr{\beta_0}+\tcr{\beta_1} \, x}}{1+ e^{\tcr{\beta_0} + \tcr{\beta_1}\,x}}  \]
            \item \textbf{Assumption}: $\Pr[Y_i = 1] = p_{\tcr{\beta}}(\tcb{x_i})$, i.i.d.
            \item Likelihood:
            \[ \ell(\tcr{\beta_0},\tcr{\beta_1})  = \prod_{i : \tcb{y_i} =1} p_{\tcr{\beta}}(\tcb{x_i}) \prod_{i:\tcb{y_i}=0} (1-p_{\tcr{\beta}}(\tcb{x_i})) \]
            \item Log-likelihood:
            \[ \log \ell(\tcr{\beta_0},\tcr{\beta_1}) = \sum_{i: \tcb{y_i} =1} \log p_{\tcr{\beta}}(\tcb{x_i}) + \sum_{i:\tcb{y_i}=0} \log (1-p_{\tcr{\beta}}(\tcb{x_i})) \]
            \item Concave maximization problem
            \item Can be solved using gradient ascent (no closed form solution)
    \end{itemize}
\end{frame}

\begin{frame} \frametitle{Today: Generalized Linear Models}
  \begin{enumerate}
  \item Logistic regression
  \item \textbf{Linear regression}
  \item Poisson regression
  \item Generalized linear models
  \end{enumerate}
\end{frame}

\begin{frame} \frametitle{Normal Distribution}
 $X \sim \mathcal{N}(\mu, \sigma)$ with mean $\mu$ and standard deviation $\sigma$
\begin{center}
        \includegraphics[width=0.3\linewidth]{../figs/class5/normal.pdf}
\end{center}
Density function:
\[ d(x \mid \mu, \sigma) = \frac{1}{\sigma\sqrt{2\pi}} e^{-\frac{(x-\mu)^2}{2\sigma^2}} \]
\pause
Density function is \textbf{not} probability:
\[ \Pr[X = x] = 0,  \qquad d(x\mid \mu, \sigma) = \frac{d}{dx} \Pr[X \le x] \]
\end{frame}

\begin{frame} \frametitle{Estimating Coefficients: Maximum Likelihood}
    \begin{itemize}
            \item \textbf{Likelihood}: Probability that data is generated from a model \only<2>{(\alert{i.i.d. assumption})}
            \only<1>{\[ \ell(\tcr{\operatorname{model}}) = \Pr[\tcb{\operatorname{data}} \mid \tcr{\operatorname{model}}]  \]}
            \only<2>{\[ \ell(\tcr{\operatorname{\beta_0,\beta_1}}) = \Pr[\tcb{\operatorname{Y_1,Y_2,Y_3,\ldots}} \mid \tcr{\operatorname{\beta_0,\beta_1}}] = \prod_{i=1}^n d(\tcb{\operatorname{Y_i}} \mid \tcr{\operatorname{\beta_0,\beta_1}}) \]}
            \item Find the most likely model:
            \only<1>{\[ \max_{\tcr{\operatorname{model}}} \ell(\tcr{\operatorname{model}}) = \max_{\tcr{\operatorname{model}}} \Pr[\tcb{\operatorname{data}} \mid \tcr{\operatorname{model}}]  \]}
            \only<2>{\[ \max_{\tcr{\operatorname{\beta_0,\beta_1}}} \ell(\tcr{\operatorname{\beta_0,\beta_1}}) = \max_{\tcr{\operatorname{\beta_0,\beta_1}}} \Pr[\tcb{\operatorname{Y_1,Y_2,Y_3,\ldots}} \mid \tcr{\operatorname{\beta_0,\beta_1}}]  \]}
            \item Likelihood function is difficult to maximize
            \item Transform it using $\log$ (strictly increasing)
            \only<1>{\[ \max_{\tcr{\operatorname{model}}} \log \ell(\tcr{\operatorname{model}}) \]}
            \only<2>{\[ \max_{\tcr{\operatorname{\beta_0,\beta_1}}} \log \ell(\tcr{\operatorname{\beta_0,\beta_1}}) = \max_{\tcr{\operatorname{\beta_0,\beta_1}}} \sum_{i=1}^n \log \Pr[\tcb{\operatorname{Y_i}} \mid \tcr{\operatorname{\beta_0,\beta_1}}] \]}
            \item Strictly increasing transformation preserves maximizer
    \end{itemize}
\end{frame}

\begin{frame} \frametitle{Max-likelihood: Linear Regression}
    \begin{itemize}
            \item \textbf{Parameters}:
            \[ f_{\tcr{\beta}}(x) = \tcr{\beta_0}+\tcr{\beta_1} \, x \]
            \item \textbf{Data}: Features $\tcb{x_i}$ and labels $\tcb{y_i}$
            \item \textbf{Assumption}: $Y_i \sim \mathcal{N}(f_{\tcr{\beta}}(\tcb{x_i}),1)$ or $Y_i = f_{\tcr{\beta}}(\tcb{x_i}) + \epsilon$, $\epsilon \sim \mathcal{N}(0,1)$, i.i.d.
            \item Likelihood:
            \[ \ell(\tcr{\beta_0},\tcr{\beta_1}) = \prod_{i=1}^{n} d( \tcb{y_{i}} \mid \mu = f_{\tcr{\beta}}(\tcb{x_i}), \sigma = 1)  \]
            \item Log-likelihood:
            \[ \log \ell(\tcr{\beta_0},\tcr{\beta_1}) = - \frac{1}{2} \sum_{i=1}^{n}  ( \tcb{y_i} - \tcr{\beta_0} - \tcr{\beta_1}\cdot \tcb{x_i})^{2} + \text{constant} \]
            \item Concave maximization problem (convex minimization problem)
    \end{itemize}
\end{frame}

\begin{frame} \frametitle{Today: Generalized Linear Models}
  \begin{enumerate}
  \item Logistic regression
  \item Linear regression
  \item \textbf{Poisson regression}
  \item Generalized linear models
  \end{enumerate}
\end{frame}

\begin{frame} \frametitle{Poisson Regression: Motivation}
  \begin{enumerate}
  \item Population of a species in a given habitat
  \item Factors driving number of accidents
  \item Number of bikeshare customers on certain days / hours
  \end{enumerate}
  \begin{center}
    \includegraphics[width=0.9\linewidth]{../islr2figs/Chapter4/4_14.pdf}
  \end{center}
  \pause
  Standard deviations of the errors are \textbf{not} constant (heteroscedastic)
\end{frame}

\begin{frame} \frametitle{Binomial Distribution}
  \begin{itemize}
  \item There are 100 potential customers
  \item Each customer has 40\% chance of coming in on any day
    \item How many customers $Y$ should I expect on any random day?
  \end{itemize}
\pause
\[  \Pr[Y = y]  = \binom{1000}{y} \cdot  0.4^y \cdot (1-0.4)^{1000-y} \]  
\end{frame}

\begin{frame} \frametitle{Poisson Distribution}
  Difficult to distinguish from data:
  \begin{itemize}
  \item 100 customers, 40\% chance of coming in
  \item 1,000 customers, 4\% chance of coming in
  \item 10,000 customers, 0.4\% chance of coming in
  \item 100,000 customers, 0.04\% chance of coming in
  \end{itemize}
  \vfill
  \pause
  \textbf{Poisson distribution}: Limit with $n \to \infty$ customers
   \[ \Pr[Y = y] = \frac{e^{-\lambda} \lambda^y}{y!} \]
   Mean: $\lambda$ and variance: $\lambda$ (variance grows with the mean)
\end{frame}

\begin{frame}
  \frametitle{Poisson Regression}
\begin{itemize}
    \item Predict the \textbf{rate}: $\lambda(x)$
    \item Example: $\lambda(\varname{hour})$ rate of customers for a particular hour
    \item \textbf{Linear regression}:
    \[ f(x) = \beta_0 + \beta_1\, x  \]
    \item \textbf{Poisson regression}:
    \[ \lambda(x) = e^{\beta_0+\beta_1 \, x}  \]
    \item the same as:
    \[ \log\left( \lambda(x) \right) = \beta_0 + \beta_1\,x \]
\end{itemize}
\end{frame}

\begin{frame} \frametitle{Interpreting Poisson Regression}
  \begin{center}
    \includegraphics[width=\linewidth]{../islr2figs/Chapter4/4_15.pdf}
  \end{center}
  The effect is exponential
\end{frame}

\begin{frame} \frametitle{Max-likelihood: Poisson Regression}
    \begin{itemize}
            \item \textbf{Parameters}:
            \[ \lambda_{\tcr{\beta}}(x) = e^{\tcr{\beta_0}+\tcr{\beta_1} \, x} \]
            \item \textbf{Data}: Features $\tcb{x_i}$ and labels $\tcb{y_i}$
            \item \textbf{Assumption}: $Y_i \sim \operatorname{Pois}(\lambda_{\tcr{\beta}}(\tcb{x_i}))$  i.i.d.
            \item Likelihood:
              \[ \ell(\tcr{\beta_0},\tcr{\beta_1}) = \prod_{i=1}^{n} \Pr[Y_i = \tcb{y_{i}} \mid \lambda = \lambda_{\tcr{\beta}}(\tcb{x_i})]  \]
            \item Log-likelihood:
            \[ \log \ell(\tcr{\beta_0},\tcr{\beta_1}) = \ldots \]
    \end{itemize}
\end{frame}


\begin{frame} \frametitle{Today: Generalized Linear Models}
  \begin{enumerate}
  \item Logistic regression
  \item Linear regression
  \item Poisson regression
  \item \textbf{Generalized linear models}
  \end{enumerate}
\end{frame}

\begin{frame} \frametitle{Generalizing Regressions}
  \textbf{Linear regression} \\
  \begin{tabular}{ll}
     Prediction & $\Ex[Y \mid X = x] = \mu_{\beta}(x)$ \\
     Model & \( \mu_{\beta}(x) = \beta_0 + \beta_1 \cdot x \) \\
     Observations model & \( Y_i \sim \mathcal{N}(\mu_{\beta}(x_i), 1)\)
  \end{tabular}
  \vfill
  \textbf{Logistic regression} \\
  \begin{tabular}{ll}
     Prediction & $\Ex[Y \mid X = x] = p_{\beta}(x)$ \\
   Model & \( \log \left( \frac{p_{\beta}(x)}{1 - p_{\beta}(x)} \right) = \beta_0 + \beta_1 \cdot x \) \\
   Observations model &
    \( Y_i \sim \operatorname{Bern}(p_{\beta}(x_i))\)
  \end{tabular}
  \vfill
  \textbf{Poisson regression} \\
  \begin{tabular}{ll}
   Prediction & $\Ex[Y \mid X = x] = \lambda_{\beta}(x)$ \\
   Model & \( \log \left( \lambda_{\beta}(x) \right) = \beta_0 + \beta_1 \cdot x \) \\
   Observations model &
    \( Y_i \sim \operatorname{Pois}(\lambda_{\beta}(x_i))\)
  \end{tabular}
\end{frame}

\begin{frame} \frametitle{Generalized Linear Regression}
  \textbf{Link function}: $\eta$
  \[ \eta( \Ex[Y \mid X = x] ) = \beta_0 + \beta_1 \cdot x  \]
  \textbf{Family}: Distribution of observations
  \[ Y_i \sim \operatorname{Distribution}(\mu = \Ex[Y \mid X = x]) \]
  \vfill
  \begin{tabular}{l|ll}
    Method & Link & Family \\
    \hline
    Linear regression & $ \eta (\mu) = \mu $ & Normal \\
    Logistic regression & $ \eta(\mu) = \log (\mu  / (1-\mu))$ & Bernoulli \\
    Poisson regression & $ \eta(\mu) = \log (\mu)$ & Poisson \\
  \end{tabular} \\
  \vfill
  See {\tiny\texttt{https://en.wikipedia.org/wiki/Generalized\_linear\_model}} for more options
\end{frame}

\end{document}
%%% Local Variables:
%%% mode: latex
%%% TeX-master: t
%%% End:
