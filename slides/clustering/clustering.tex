\documentclass{beamer}


\let\val\undefined
\usepackage{booktabs}
\usepackage{natbib}
\usepackage{algorithm2e}
\usepackage{siunitx}
\usepackage{framed}
\usepackage{longtable}
\usepackage{amsmath}
\usepackage{amsthm}
\usepackage{grffile}

\usetheme[progressbar=frametitle]{metropolis}
\usepackage{libertine}

% *** Styles ***
\setbeamertemplate{navigation symbols}{}
%\usecolortheme{dolphin}
%\usecolortheme{rose}
%\setbeamercovered{transparent}
\usefonttheme{professionalfonts}
%\usefonttheme[onlymath]{serif}

% *** Colors ***
\newcommand{\tc}[2]{\textcolor{#1}{#2}}
\newcommand{\tcb}[1]{\tc{blue}{#1}}
\newcommand{\tcr}[1]{\tc{red}{#1}}
\newcommand{\tcg}[1]{\tc{green}{#1}}

\newcommand{\Ex}{\mathbb{E}}
\newcommand{\var}{\operatorname{Var}}
%\newcommand{\Pr}{\mathbb{P}}

\definecolor{varcolor}{RGB}{132,23,49}
\newcommand{\varname}[1]{\textcolor{varcolor}{\mathsf{#1}}}

\title{Clustering}
\subtitle{Unsupervised Learning}
\author{Marek Petrik}
\date{April 25th, 2022}


\begin{document}
\begin{frame} \maketitle
		\tiny{Some of the figures in this presentation are taken from "An Introduction to Statistical Learning, with applications in R"  (Springer, 2013) with permission from the authors: G. James, D. Witten,  T. Hastie and R. Tibshirani }
\end{frame}


\begin{frame} \frametitle{Learning Methods}
	\begin{enumerate}
		\item \textbf{Supervised Learning}: Learning a function $f$:
		\[ Y = f(X) + \epsilon \]
		\begin{enumerate}
			\item Regression
			\item Classification
		\end{enumerate}
		\item \textbf{Unsupervised learning}: Discover interesting properties of data (no labels)
		\[ X_1, X_2, \ldots \]
		\begin{enumerate}
			\item Dimensionality reduction or embedding
			\item Clustering
		\end{enumerate}
	\end{enumerate}
\end{frame}

\begin{frame} \frametitle{Why Unsupervised Learning}
\begin{itemize}
	\item Reduce features
	\vfill
	\item Visualization
	\vfill
	\item Exploratory data analysis
	\vfill
	\item Reduce overfitting by reducing the number of features (PCR)
\end{itemize}
\end{frame}

\begin{frame} \frametitle{Principal Components}
\centering
\begin{tabular}{rrrrr}
	\hline
	& PC1 & PC2 & PC3 & PC4 \\
	\hline
	Murder & -0.54 & 0.42 & -0.34 & 0.65 \\
	Assault & -0.58 & 0.19 & -0.27 & -0.74 \\
	UrbanPop & -0.28 & -0.87 & -0.38 & 0.13 \\
	Rape & -0.54 & -0.17 & 0.82 & 0.09 \\
	\hline
\end{tabular}\\[1cm]

Standard deviation: \\
\begin{tabular}{cccc}
	\hline
	PC1 & PC2 & PC3 & PC4 \\
	\hline
	1.5748783 &  0.9948694 & 0.5971291 & 0.4164494 \\
	\hline
\end{tabular}
\end{frame}


\begin{frame} \frametitle{Applying PCA}
\begin{enumerate}
	\item Features should be \textbf{centered} = zero mean
	\vfill
	\item \textbf{Scale} of features matters
	\vfill
	\item Should features be scaled (normalized)?
	\pause
	\item Depends.
	\pause
	\vfill
	\item The direction (sign) of principal vectors is not unique
	\vfill
	\item \textbf{Proportion of Variance Explained}: variance along the dimension / total variance
	\vfill
	\item How many principal vectors? \visible<2->{It depends \ldots}
\end{enumerate}
\end{frame}

\begin{frame}\frametitle{More Unsupervised Learning: Discovering Structure of Data}
	\begin{enumerate}
		\item K-Means Clustering
		\item Hierarchical Clustering
	\end{enumerate}
\end{frame}

\begin{frame} \frametitle{Clustering}
	Simplify data in a different way than PCA.	\\
	\vfill
	\begin{itemize}
		\item PCA finds a low-dimensional representation of data
		\vfill
		\item Clustering finds homogeneous subgroups among the observations
	\end{itemize}

\end{frame}

\begin{frame} \frametitle{Where Are We?}
\centering
\includegraphics[width=\linewidth]{../figs/class1/ml_map.png}\\
{\tiny Source: \url{http://scikit-learn.org/stable/tutorial/machine_learning_map/index.html}} \\[3mm]
\end{frame}

\begin{frame} \frametitle{Clustering: Assumptions and Goals}
	\begin{itemize}
		\item Exists a method for measuring similarity between data points
		\item Some points are more similar than others
		\vfill
		\item<2-> \textbf{Want to identify similarity patterns}
		\begin{enumerate}
			\item Discover the different types of disease
			\item Market segmentation: Types of users that visit a website
			\item Discover movie or book genres
			\item Discover types of topics in documents
		\end{enumerate}
		\item<3-> Discover \textbf{latent} patterns that exist but may not be observed/observable
	\end{itemize}
\end{frame}

\begin{frame} \frametitle{Clustering Algorithms}
	\begin{itemize}
		\item \textbf{K-Means}: simple and effective
                  \vfill 
		\item \textbf{Hierarchical clustering}: Many complex clusters
                  \vfill 
		\item Many other clustering methods, most heuristics
                  \vfill 
		\item \textbf{EM}: General algorithm for dealing with latent variables by \emph{maximizing likelihood}
                  \vfill 
                \item \textbf{Matrix Completion}: Clustering across multiple attributes simultaneously
	\end{itemize}
\end{frame}

\begin{frame} \frametitle{K-Means Clustering}
	\begin{itemize}
		\item Cluster data into \emph{complete} and \emph{non-overlapping} sets
		\item Example:
                  \begin{center}
                    \includegraphics[width=\linewidth]{{../islrfigs/Chapter10/10.5}.pdf}
                  \end{center}
        \end{itemize}
\end{frame}

\begin{frame} \frametitle{K-Means Objective}
	\begin{itemize}
		\item $k$-th cluster: $C_k$
		\item $i$-th observation in cluster $k$: $i\in C_k$
		\item<2-> Find clusters that are homogeneous: $W(C_k)$ homogeneity of clusters
		\[ \min_{C_1,\ldots,C_K} \sum_{k=1}^K W(C_k) \]
		\item<3-> Define homogeneity as in-cluster variance
		\[ \min_{C_1,\ldots,C_K} \sum_{k=1}^K \left( \frac{1}{|C_k|} \sum_{i,i'\in C_k} \sum_{j=1}^{p} (x_{ij} - x_{i'j})^2 \right) \]
		\item<4-> Minimizing objective NP hard
	\end{itemize}
\end{frame}

\begin{frame} \frametitle{K-Means Algorithm}
	\framesubtitle{Heuristic solution to the minimization problem}
	\begin{enumerate}
		\item Randomly assign cluster numbers to observations
		\item Iterate while clusters change
		\begin{enumerate}
			\item For each cluster, compute the centroid
			\item Assign each observation to the closest cluster
		\end{enumerate}
	\end{enumerate}
	\vfill
	Note that:
	\[\frac{1}{|C_k|} \sum_{i,i'\in C_k} \sum_{j=1}^{p} (x_{ij} - x_{i'j})^2 =  2 \sum_{i,i'\in C_k} \sum_{j=1}^{p} (x_{ij} - \bar{x}_{kj})^2  \]
\end{frame}

\begin{frame} \frametitle{K-Means Illustration}
	\begin{center}\includegraphics[width=0.7\linewidth]{{../islrfigs/Chapter10/10.6}.pdf}\end{center}
\end{frame}

\begin{frame} \frametitle{Properties of K-Means}
	\begin{itemize}
		\item \emph{Local minimum}: Does not necessarily find the optimal solution
		\item Multiple runs can result in different solutions
		\item Choose the result of the run with \underline{minimal objective}
		\item Cluster labels do not matter
	\end{itemize}
\end{frame}

\begin{frame} \frametitle{Multiple Runs of K-Means}
	\begin{center}\includegraphics[width=0.7\linewidth]{{../islrfigs/Chapter10/10.7}.pdf}\end{center}
\end{frame}

\begin{frame} \frametitle{Hierarchical Clustering}
	\begin{itemize}
		\item Multiple levels of similarity needed in complex domains
		\item Build a similarity tree
	\end{itemize}
	\begin{center}\includegraphics[width=\linewidth]{{../islrfigs/Chapter10/10.10}.pdf}\end{center}
\end{frame}

\begin{frame} \frametitle{Dendrogram: Similarity Tree}
	\begin{center}\includegraphics[width=\linewidth]{{../islrfigs/Chapter10/10.9}.pdf}\end{center}
\end{frame}

\begin{frame} \frametitle{Hierarchical Clustering Algorithm}
	\begin{enumerate}
		\item Begin with $n$ observations and compute ${n \choose 2}$ dissimilarity measures
		\item For $i = n, n-1, \ldots, 2$
		\begin{enumerate}
			\item Fuse 2 most similar clusters
			\item Update $i-1$ dissimilarities
		\end{enumerate}
	\end{enumerate}
\end{frame}

\begin{frame} \frametitle{Hierarchical Clustering Algorithm: Illustration}
	\begin{center}\includegraphics[width=0.8\linewidth]{{../islrfigs/Chapter10/10.11}.pdf}\end{center}
\end{frame}

\begin{frame} \frametitle{Dissimilarity Measure: Linkage}
	\begin{enumerate}
		\item Complete
		\item Single
		\item Average
		\item Centroid
	\end{enumerate}
\end{frame}

\begin{frame} \frametitle{Impact of Dissimilarity Measure}
	\begin{center}\includegraphics[width=\linewidth]{{../islrfigs/Chapter10/10.12}.pdf}\end{center}
\end{frame}

\begin{frame} \frametitle{Clustering in Practice}
	\begin{itemize}
		\item Fraught with problems: no clear measure of quality (like MSE)
		\item How to choose $k$? \pause Problem dependent
		\item Standardize features, center them?
		\item What dissimilarity to use?
		\item<2-> Careful over-explaining clustering results: \tiny{source: \url{http://miriamposner.com}}
		\begin{center}
			\includegraphics[width=0.8\linewidth]{../figs/class17/miriamposner.png}
		\end{center}
	\end{itemize}
\end{frame}


%\begin{frame} \frametitle{Expectation-Maximization}
%	\begin{itemize}
%		\item Maximum likelihood approach to clustering
%		\item General method for dealing with latent features / labels
%		\item Especially useful with \textbf{generative models}
%		\item A heuristic method used to solve complex optimization problems
%		\item Generalization of the idea: \textbf{Minorization-Maximization}
%		\item Gentle introduction: \url{https://www.cs.utah.edu/~piyush/teaching/EM_algorithm.pdf}
%	\end{itemize}
%\end{frame}

%\begin{frame} \frametitle{Recall LDA}
%\end{frame}
%
%\begin{frame}\frametitle{LDA: Linear Discriminant Analysis}
%	\begin{itemize}
%		\item \textbf{Generative model}: capture probability of predictors for each label
%		\begin{center}\includegraphics[width=\linewidth]{{../islrfigs/Chapter4/4.4}.pdf}\end{center}
%		\item Predict:
%		\begin{enumerate}
%			\item<2-> $\Pr[\varname{balance} \mid \varname{default} = \operatorname{yes} ]$ and $\Pr[\varname{default} = \operatorname{yes}]$
%			\item<3-> $\Pr[\varname{balance} \mid \varname{default} = \operatorname{no} ]$ and $\Pr[\varname{default} = \operatorname{no}]$
%		\end{enumerate}
%		\item<4-> Classes are normal: $\Pr[\varname{balance} \mid \varname{default} = \operatorname{yes} ]$
%	\end{itemize}
%\end{frame}
%
%\begin{frame} \frametitle{LDA vs Logistic Regression}
%	\begin{itemize}
%		\item \textbf{Logistic regressions}:
%		\[ \Pr[\varname{default} = \operatorname{yes} \mid \varname{balance}] \]
%		\vfill
%		\item \textbf{Linear discriminant analysis}:
%		\[ \Pr[\varname{balance} \mid \varname{default} = \operatorname{yes} ] \text{ and } \Pr[\varname{default} = \operatorname{yes}] \]
%		\[ \Pr[\varname{balance} \mid \varname{default} = \operatorname{no} ] \text{ and } \Pr[\varname{default} = \operatorname{no}] \]
%	\end{itemize}
%\end{frame}
%
%\begin{frame}\frametitle{LDA with 1 Feature}
%	\begin{itemize}
%		\item Classes are normal and class probabilities $\pi_k$ are scalars
%		\[ f_k(x) = \frac{1}{\sigma\sqrt{2\pi}} \exp\left(  - \frac{1}{2\sigma^2} (x-\mu_k)^2 \right) \]
%		\item \textbf{Key Assumption}:Class variances $\sigma_k^2$	\underline{are the same}.
%		\begin{center}\includegraphics[width=\linewidth]{{../islrfigs/Chapter4/4.4}.pdf}\end{center}
%	\end{itemize}
%\end{frame}
%
%\newcommand{\model}{\tcr{\operatorname{model}}}
%\newcommand{\latent}{\tcg{\operatorname{latent}}}
%\newcommand{\data}{\tcb{\operatorname{data}}}
%
%\begin{frame} \frametitle{EM For LDA}
%	\begin{itemize}
%		\item Labels are missing, guess them
%		\item Find the most likely model \underline{and} latent observations:
%		\begin{gather*}
%		\max_{\model} \log \ell(\model) = \max_{\substack{\model \\ \latent}} \log \sum_{\latent} \Pr[\data, \latent \mid \model] = \\
%		\visible<2->{= \max_{\substack{\model \\ \latent}} \log \sum_{\latent} \Pr[\data \mid \latent, \model] \Pr[\latent \mid \model]}
%		 \end{gather*}
%		 \item<3-> Difficult and non-convex optimization problem ($\log \sum$)
%	\end{itemize}
%\end{frame}
%
%\begin{frame} \frametitle{EM Derivation}
%	\begin{itemize}
%		\item Iteratively approximate and optimize the log-likelihood function
%		\begin{enumerate}
%			\item Construct a concave lower bound
%			\item Maximize the lower bound
%			\item Repeat
%		\end{enumerate}
%		\item Notation:
%		\begin{itemize}
%			\item Model: $\theta$
%			\item Data: $x$
%			\item Latent variables: $z$
%		\end{itemize}
%		\renewcommand{\model}{\theta}
%		\renewcommand{\latent}{z}
%		\renewcommand{\data}{x}
%		\begin{gather*}
%		\max_{\model,\latent} \log \ell(\model,\latent) = \max_{\model,\latent} \log \Pr[\data \mid \model] = \\
%		= \max_{\model,\latent} \log \sum_{\latent} \Pr[\data \mid \latent, \model] \Pr[\latent \mid \model]
%		\end{gather*}
%	\end{itemize}
%\end{frame}
%
%\begin{frame} \frametitle{EM Derivation}
%	\begin{itemize}
%		\item Suppose we have an estimate of the model $\theta_n$
%		\item How to compute $\theta_{n+1}$ that improves on it?
%	\end{itemize}
%	\begin{gather*}
%		\theta_{n+1}, z_{n+1} = \\
%		\arg\max_{\theta,z} \log \sum_{z} \Pr[x,z \mid \theta] = \arg\max_{\theta,z} \log \sum_{z} \Pr[z \mid \theta] \Pr[z \mid x, \theta] = \\
%		= \arg\max_{\theta,z} \log \sum_{z} \Pr[z \mid \theta] \Pr[z \mid x, \theta] \frac{\Pr[z \mid x, \theta_n]}{\Pr[z \mid x, \theta_n]} = \\
%		= \arg\max_{\theta,z} \log \sum_{z} \Pr[z \mid x, \theta_n] \frac{\Pr[z \mid \theta] \Pr[z \mid x, \theta]}{\Pr[z \mid x, \theta_n]} \le \\
%		\stackrel{\text{jensen's}}{\le} \arg\max_{\theta,z} \sum_{z} \Pr[z \mid x, \theta_n] \log  \frac{\Pr[z \mid \theta] \Pr[z \mid x, \theta]}{\Pr[z \mid x, \theta_n]} =  \\
%		= \arg\max_{\theta,z} \sum_{z} \Pr[z \mid x, \theta_n] \log  \Pr[x,z \mid \theta]
%	\end{gather*}
%\end{frame}
%
%\begin{frame} \frametitle{EM Algorithm}
%
%	\begin{enumerate}
%		\item \textbf{E Step}: Estimate $\Pr[z \mid x, \theta_n]$ for all values of $z$. (Construct the lower bound)
%		\item \textbf{M-Step}: Maximize the lower bound:
%		\[ \theta_{n+1} = \arg\max_{\theta} \sum_{z} \Pr[z \mid x, \theta_n] \log  \Pr[x,z \mid \theta]  \]
%		This can be solved using traditional MLE methods with weighted samples
%	\end{enumerate}
%\end{frame}
%
%
%\begin{frame} \frametitle{EM for Mixture of Gaussians}
%	\framesubtitle{\underline{Rough} sketch}
%	\begin{enumerate}
%		\item Randomly assign cluster \textbf{weights} to observations
%		\item Iterate while clusters change
%		\begin{enumerate}
%			\item For each cluster, compute the centroid based on observation \textbf{weights} of observations
%			\item Assign each observation new cluster \textbf{weights} based on the distances from centroids
%		\end{enumerate}
%	\end{enumerate}
%\end{frame}
%
%\begin{frame} \frametitle{Other Applications of EM}
%	\begin{itemize}
%		\item Very powerful and general idea!
%		\item Training with missing data for many model types
%		\item Hidden variables in Bayesian nets
%		\item Identifying confounding variables
%		\item Solving difficult (complex) optimization problem: MM
%	\end{itemize}
%\end{frame}

\end{document}

%%% Local Variables:
%%% mode: latex
%%% TeX-master: t
%%% End:
