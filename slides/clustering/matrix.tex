\documentclass{beamer}

\usepackage{pgf}
\usepackage{pgfplots}
\usepackage{tikz}
\usepackage{booktabs}
\usepackage{natbib}
\usepackage{algorithm2e}
\usepackage{mpemath}

\usetheme[progressbar=frametitle]{metropolis}
\usepackage{libertine}

\usetikzlibrary{matrix,arrows,automata,backgrounds,positioning}

% *** Styles ***
\setbeamertemplate{navigation symbols}{}
\usecolortheme{dolphin}
\setbeamercovered{transparent}
\usefonttheme{professionalfonts}

% *** Colors ***
\newcommand{\tc}[2]{\textcolor{#1}{#2}}
\newcommand{\tcb}[1]{\tc{blue}{#1}}
\newcommand{\tcr}[1]{\tc{red}{#1}}
\newcommand{\tcg}[1]{\tc{green}{#1}}

\title{Recommender Systems: Collaborative Filtering}
\author{Marek Petrik}

\date{April 27, 2022}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\begin{document}

\begin{frame}\maketitle\end{frame}

\begin{frame} {Outline}
	\begin{itemize}
		\item Collaborative filtering
		\begin{itemize}
			\item Recommender systems
			\item Online travel recommendations
			\item Collaborative filtering
			\item Matrix factorization and completion
		\end{itemize}
	\end{itemize}
\end{frame}

\begin{frame} {Example recommender systems}
	\begin{itemize}
		\item Movie recommendations (Netflix)
		\item Personalized music (Pandora)
		\item Relevant product recommendations (Amazon)
		\item Online advertising (Facebook)
		\item Online dating (OK Cupid)
		\item Special offers, coupons, and discounts (Stores)
	\end{itemize}
\end{frame}

%\begin{frame}{Google Play recommendations}
%	\centering
%	\includegraphics[width=\linewidth]{fig/google-play.png}
%\end{frame}
%
%\begin{frame} {Amazon recommendations}
%	\centering
%	\includegraphics[width=\linewidth]{fig/amazon-recommend.png}
%\end{frame}
%
%\begin{frame} {IBM travel recommendations}
%	\begin{center}
%		\includegraphics[width=0.75\linewidth]{../figs/class20_7/example_recommendation.png}
%	\end{center}
%	\begin{itemize}
%		\item Recommend relevant products on website of a tour operator
%	\end{itemize}
%\end{frame}

\begin{frame} {Travel recommendation steps}
	\begin{center}
		Unknown customer\\
		\includegraphics[width=0.1\linewidth]{../figs/class20_7/anonymous.png}
	\end{center}
	\vspace{0.1in}
	\visible<2->{
		\begin{center}
			Browsing history (trips) \\
			\includegraphics[width=0.2\linewidth]{../figs/class20_7/sea2.jpg}\hspace{0.01\linewidth}
			\includegraphics[width=0.2\linewidth]{../figs/class20_7/ski.jpg}\hspace{0.01\linewidth}
			\includegraphics[width=0.2\linewidth]{../figs/class20_7/city.jpg}\hspace{0.01\linewidth}
			\includegraphics[width=0.2\linewidth]{../figs/class20_7/sea2.jpg}
		\end{center}
	}
	\vspace{0.3in}
	\begin{columns}
	\visible<3->{
	\begin{column}{0.2\linewidth}
		\centering
		Best guess\\
		\includegraphics[width=0.8\linewidth]{../figs/class20_7/family.jpeg}
	\end{column} }%
	\hspace{0.1\linewidth}
	\visible<4->{
		\begin{column}{0.8\linewidth}
			\centering
			Recommended trips \\
			\vspace{0.06in}
			\includegraphics[width=0.2\linewidth]{../figs/class20_7/city.jpg}\hspace{0.02\linewidth}	\includegraphics[width=0.2\linewidth]{../figs/class20_7/ski.jpg}\hspace{0.02\linewidth}						\includegraphics[width=0.2\linewidth]{../figs/class20_7/mountain.jpg}
		\end{column}
	}
	\end{columns}
\end{frame}


\begin{frame} {Types of recommender systems}
	\begin{itemize}
		\item By delivery:
			\begin{itemize}
				\item \textbf{Item-to-item} recommendations: e.g. Amazon products
				\item \textbf{User-to-item} recommendations: e.g. Netflix, Google Play
			\end{itemize}
		\vfill
		\item<2-> By information used:
			\begin{itemize}
				\item<3-> \textbf{Content-based}: Use item description and user profile
				\begin{itemize}
					\item Use when rich user profile and content information is available
				\end{itemize}
				\item<4-> \textbf{Collaborative filtering}: Use preferences of other users
				\begin{itemize}
					\item When rich interaction history is available
				\end{itemize}
			\end{itemize}
	\end{itemize}
\end{frame}

\begin{frame} {Content-based or collaborative filtering?}
	\begin{center}
		Unknown customer\\
		\includegraphics[width=0.1\linewidth]{../figs/class20_7/anonymous.png}
	\end{center}
	\vspace{0.1in}
	\begin{center}
		Browsing history (trips) \\
		\includegraphics[width=0.2\linewidth]{../figs/class20_7/sea2.jpg}\hspace{0.01\linewidth}
		\includegraphics[width=0.2\linewidth]{../figs/class20_7/ski.jpg}\hspace{0.01\linewidth}
		\includegraphics[width=0.2\linewidth]{../figs/class20_7/city.jpg}\hspace{0.01\linewidth}
		\includegraphics[width=0.2\linewidth]{../figs/class20_7/sea2.jpg}
	\end{center}
	\vspace{0.3in}
	\begin{columns}
		\begin{column}{0.2\linewidth}
			\centering
			Best guess\\
			\includegraphics[width=0.8\linewidth]{../figs/class20_7/family.jpeg}
		\end{column}
		\hspace{0.1\linewidth}
		\begin{column}{0.8\linewidth}
			\centering
			Recommended trips \\
			\vspace{0.06in}
			\includegraphics[width=0.2\linewidth]{../figs/class20_7/city.jpg}\hspace{0.02\linewidth}	\includegraphics[width=0.2\linewidth]{../figs/class20_7/ski.jpg}\hspace{0.02\linewidth}	\includegraphics[width=0.2\linewidth]{../figs/class20_7/mountain.jpg}
		\end{column}
	\end{columns}
\end{frame}

\begin{frame} {Content-based recommender systems}
	\begin{itemize}
		\item Learn user preference model based on attributes
        \vfill
		\item Use historical user preference data
		\begin{center}
		\begin{tabular}{|c|c||c|c||c|}
			\hline
			\multicolumn{2}{|c||}{User} & \multicolumn{2}{c||}{Movie} & \\
			\hline
			Gender & Age & Genre & Year & Rating \\
			\hline \hline
			Male & 16 & Comedy & 1998 & 1 \\
			Male & 98 & Horror & 1912 & 5 \\
			Female & 43 & Action & 2016 & 3 \\
			\hline
		\end{tabular}
		\end{center}
        \vfill
		\item Use supervised learning methods
	\end{itemize}
\end{frame}

\newcommand{\unknw}{\textcolor{gray}{?}}
\begin{frame} {Collaborative-filtering recommender systems }
	\begin{itemize}
		\item Very flexible: no need to know content or user profiles
		\item Simple and powerful methods
		\item \textbf{Training data}: Partial user-item preferences \\
		\begin{center}
			\begin{tikzpicture}
			\matrix(bQ) [matrix of math nodes,left delimiter=(, right delimiter=),ampersand replacement=\&]
			{
				3 \& \unknw \& \unknw \& 1 \& 3 \& 1 \& 1 \& 5 \& \unknw \& 5 \\
				\unknw \& 2 \& 3 \& \unknw \& 3 \& \unknw \& 2 \& \unknw \& \unknw \& \unknw \\
				\unknw \& 5 \& \unknw \& \unknw \& 1 \& \unknw \& \unknw \& 2 \& \unknw \& 4 \\
				4 \& 3 \& \unknw \& 3 \& \unknw \& \unknw \& \unknw \& \unknw \& \unknw \& \unknw \\
				2 \& 3 \& 5 \& 2 \& \unknw \& \unknw \& \unknw \& 5 \& \unknw \& 4 \\
				\unknw \& \unknw \& 1 \& 2 \& 5 \& 3 \& 4 \& \unknw \& \unknw \& \unknw \\
			};
			\node[left=0.6 of bQ-1-1,rotate=90] {user};
			\node[above=0.0 of bQ-1-2] {item};
			\end{tikzpicture}
		\end{center}
		\item \textbf{Making recommendations}: Fill in the blanks (\unknw)
	\end{itemize}
\end{frame}

\begin{frame} {Nearest neighbors: Use similar users}
	\begin{itemize}
	\item Infer preferences from similar users
	\item \textbf{Other users}: \\
	\begin{tikzpicture}
	\matrix(bQ) [matrix of math nodes,left delimiter=(, right delimiter=),ampersand replacement=\&]
	{
		\alert<6>{3} \& \unknw \& \unknw \& 1 \& 3 \& 1 \& \alert<6>{1} \& \alert<6>{5} \& \unknw \& 5 \\
		\alert<3-4>{4} \& \alert<2>{3} \& \unknw \& \alert<2>{3} \& \unknw \& \unknw \& \unknw \& \unknw \& \alert<3-4>{5} \& \unknw \\
	};
	\node[left=0.6 of bQ-1-1,rotate=90] {user};
	\node[above=0.0 of bQ-1-2] {item};
	\end{tikzpicture}

	\item \textbf{Current user}: \\
	\begin{tikzpicture}
	\matrix(bQ) [matrix of math nodes,left delimiter=(, right delimiter=),ampersand replacement=\&]
	{
		\only<1-2>{\unknw} \only<3>{\alert{?}} \only<4->{\alert<4,6>{4}} \& \alert<2>{3} \& 3 \& \alert<2>{3} \& \unknw \& \unknw \& \alert<6>{1} \& \alert<6>{5} \& \only<1-2>{\unknw} \only<3>{\alert{?}} \only<4->{\alert<4>{5}} \& \unknw \\
	};
	\node[above left=0.6 of bQ-1-1,rotate=90,xshift=-0.15cm] {user};
	\end{tikzpicture}\\
	\item<2-4> Basic algorithm:
	\begin{enumerate}
		\item \uncover<2>{Find similar user (e.g. 2 ratings same)}
		\item \uncover<3-4>{Infer unknown preferences}
	\end{enumerate}
	\item<5-6> Why does this fail? \only<6>{\alert{Conflicting preferences!}}
	\end{itemize}
\end{frame}

\begin{frame} {Matrix factorization}
	\begin{itemize}
		\item Better model for addressing incompatible preferences
		\item \textbf{Assumption}: Latent (unobserved) customer type, e.g.
		\begin{enumerate}
			\item Adventure seeking
			\item Luxury oriented
		\end{enumerate}

		\item Low-rank matrix decomposition: \\
		\begin{tikzpicture}
		\matrix(bQ) [matrix of math nodes,left delimiter=(, right delimiter=),below delimiter=\},ampersand replacement=\&]
		{
			\alert<2>{2} \& 3 \& 3 \& 1 \& 1 \\
			2 \& 3 \& 3 \& 1 \& 1 \\
			4 \& 3 \& 1 \& 1 \& 3 \\
			4 \& 3 \& 1 \& 1 \& 3 \\
			4 \& 3 \& 1 \& 1 \& 3 \\
		};
		\node[left=0.6 of bQ-2-1,rotate=90] {user};
		\node[above=0.0 of bQ-1-3] {item};
		\node[right=0.2 of bQ]  (equal) {$=$};
		\matrix(U) [right=0.7 of equal,matrix of math nodes,left delimiter=(, right delimiter=),below delimiter=\},ampersand replacement=\&]
		{
			\alert<2>{0} \& \alert<2>{1} \\
			0 \& 1 \\
			1 \& 0 \\
			1 \& 0 \\
			1 \& 0 \\
		};
		\node[left=0.7cm of U-2-1,rotate=90] {user};
		\node[above=0.0cm of U-1-1,xshift=0.21cm] {type};
		\matrix(Q) [right=1.1 of U,matrix of math nodes,left delimiter=(, right delimiter=),below delimiter=\},ampersand replacement=\&]
		{
			\alert<2>{4} \& 3 \& 1 \& 1 \& 3 \\
			\alert<2>{2} \& 3 \& 3 \& 1 \& 1 \\
		};
		\node[right=0.8cm of Q-1-5,rotate=-90] {type};
		\node[above=0.0cm of Q-1-3] {item};

		\node[below=0.4 of bQ] {$D$};
		\node[below=0.4 of U] {$U$};
		\node[below=1.15 of Q] {$Q\tr$};
		\node[below=1.7 of equal]{$=$};
		\end{tikzpicture}
	\item<2> $D_{11} = U_1 Q_1\tr$
	\item<2> Not integral in general!
	\end{itemize}
\end{frame}

\newcommand{\rank}{\operatorname{rank}}

\begin{frame} {Computing matrix factorization}
\begin{itemize}
	\item Given a \textbf{small} constant $k$
    \vfill
	\item How to compute the best factorization $U,Q$?
	\[  D \stackrel{?}{=} U Q\tr \text{ s.t. } \rank(U) = \rank(Q) = k \]
    \vfill
	\item Solve optimization problem:
	\[ 	\min_{U,Q} \| D - U Q\tr\|_F^2  \text{ s.t. } \rank(U) = \rank(Q) = k \]
    \vfill
	\item Frobenius norm: $\|A\|_F^2 = \sum_{i,j} A_{ij}^2 = \sum_i \sigma_i^2$
    \vfill
	\item Solve optimally by SVD when $D$ is complete
    \vfill
	\item NP-hard otherwise
\end{itemize}
\end{frame}

\begin{frame}{Simple matrix factorization}

    \begin{enumerate}
        \item Fix $U$ and solve: $\min_{Q} \| D - U Q\tr\|_F^2 \text{ s.t. } \rank(Q) = k$
        \item Fix $Q$ and solve: $\min_{U} \| D - U Q\tr\|_F^2 \text{ s.t. } \rank(U) = k$
        \item Repeat (go to 1)
    \end{enumerate}

\end{frame}

\begin{frame} {Beyond simple models: Dynamic user behavior}
	\begin{itemize}
		\item Hidden Markov Model
		\begin{center}
			\includegraphics[width=\linewidth]{../figs/class20_7/dynamic_behavior.png}
		\end{center}
		\item Strategic recommendations: optimal in long run (conversion/satisfaction)
	\end{itemize}
\end{frame}



\end{document}

%%% Local Variables:
%%% mode: latex
%%% TeX-master: t
%%% End:
