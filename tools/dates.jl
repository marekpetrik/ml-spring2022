using Dates

sem_start = Date("2022-01-25")
sem_end = Date("2022-05-9")

cdate = sem_start
class_days = (4,) # Mon = 1, Sun = 7

i = 1
while(cdate <= sem_end)
    global cdate
    global i
    if(dayofweek(cdate) ∈ class_days)
        println(i, " ", monthabbr(cdate), " ", day(cdate), " ", dayabbr(cdate))
        i = i + 1
    end
    cdate = cdate + Day(1)
end
